package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataACA;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by PATRA on 10-Apr-17.
 */

public class AdapterACA extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataACA> items_aca;

    public AdapterACA(Activity activity, List<DataACA> items) {
        this.activity = activity;
        this.items_aca = items;
    }

    @Override
    public int getCount() {
        return items_aca.size();
    }

    @Override
    public Object getItem(int position) {
        return items_aca.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_aca,null);
        }

        final TextView txtACADist = (TextView) convertView.findViewById(R.id.txtACADist);
        final TextView txtACADistName = (TextView) convertView.findViewById(R.id.txtACADistName);
        final TextView txtACACity = (TextView) convertView.findViewById(R.id.txtACACity);
        final TextView txtACACP = (TextView) convertView.findViewById(R.id.txtACACP);
        final TextView txtACAAcPC = (TextView) convertView.findViewById(R.id.txtACAAcPC);
        final TextView txtACAAc = (TextView) convertView.findViewById(R.id.txtACAAc);
        final TextView txtACAPc = (TextView) convertView.findViewById(R.id.txtACAPc);

        DataACA datax = items_aca.get(position);

        Log.d("TehMe", datax.getAr_dist()+" ~ "+datax.getAr_dist_name()+" ~ "+datax.getAr_city()+" ~ "+datax.getAr_cp()+" ~ "+datax.getAr_acpc()+" ~ "+datax.getAr_ac()+" ~ "+datax.getAr_pc());

        if (datax.getAr_dist().length()>0){
            txtACADist.setText(datax.getAr_dist());
        }else {
            txtACADist.setText("-");
        }
        if (datax.getAr_dist_name().length()>0) {
            txtACADistName.setText(datax.getAr_dist_name());
        }else {
            txtACADistName.setText("-");
        }
        if (datax.getAr_city().length()>0) {
            txtACACity.setText(datax.getAr_city());
        }else {
            txtACACity.setText("-");
        }
        if (datax.getAr_cp().length()>0) {
            txtACACP.setText(datax.getAr_cp());
        }else {
            txtACACP.setText("-");
        }
        setColornya(datax.getAr_acpc(), txtACAAcPC);
        if(!datax.getAr_acpc().equals("0.0") || !datax.getAr_acpc().equals("0.00") && datax.getAr_acpc().length()>3){
            String a = datax.getAr_acpc();
            Double b = Double.valueOf(a);
            DecimalFormat df = new DecimalFormat("#.##");
            txtACAAcPC.setText(String.valueOf(df.format(b))+"%");
        }else{
            txtACAAcPC.setText("0.00%");
        }
        if (datax.getAr_ac().length()>0){
            txtACAAc.setText(NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(datax.getAr_ac()))));
            if (datax.getAr_ac().length()>7){
                txtACAAc.setTextSize(9);
            }
        }else{
            txtACAAc.setText("0");
        }
        if (datax.getAr_pc().length()>0){
            txtACAPc.setText(NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(datax.getAr_pc()))));
            if (datax.getAr_pc().length()>7){
                txtACAPc.setTextSize(9);
            }
        }else {
            txtACAPc.setText("0");
        }

        return convertView;
    }

    private void setColornya(String val, TextView txt) {
        txt.setTextColor(ContextCompat.getColor(activity,R.color.colorPutih));
        if(Double.valueOf(val)<=40){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if(Double.valueOf(val)>40 && Double.valueOf(val)<80){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if(Double.valueOf(val)>=80){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }
}
