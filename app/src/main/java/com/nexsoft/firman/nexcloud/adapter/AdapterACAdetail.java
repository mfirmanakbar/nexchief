package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataACAdetail;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by PATRA on 10-Apr-17.
 */

public class AdapterACAdetail extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataACAdetail> items_aca_det;

    public AdapterACAdetail(Activity activity, List<DataACAdetail> items_aca_det) {
        this.activity = activity;
        this.items_aca_det = items_aca_det;
    }

    @Override
    public int getCount() {
        return items_aca_det.size();
    }

    @Override
    public Object getItem(int position) {
        return items_aca_det.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_aca_detail,null);
        }

        final TextView txtAcaDetName = (TextView) convertView.findViewById(R.id.txtAcaDetName);
        final TextView txtAcaDetType = (TextView) convertView.findViewById(R.id.txtAcaDetType);
        final TextView txtAcaDetCp = (TextView) convertView.findViewById(R.id.txtAcaDetCp);
        final TextView txtAcaAcPc = (TextView) convertView.findViewById(R.id.txtAcaAcPc);
        final TextView txtAcaAc = (TextView) convertView.findViewById(R.id.txtAcaAc);
        final TextView txtAcaPc = (TextView) convertView.findViewById(R.id.txtAcaPc);

        //DataACA datax = items_aca.get(position);
        DataACAdetail datax = items_aca_det.get(position);
        if (datax.getAr_dist_name().length()>0){
            txtAcaDetName.setText(datax.getAr_dist_name());
        }else {
            txtAcaDetName.setText("-");
        }
        if (datax.getAr_city().length()>0){
            txtAcaDetType.setText(datax.getAr_city());
        }else {
            txtAcaDetType.setText("-");
        }
        if (datax.getAr_cp().length()>0){
            txtAcaDetCp.setText(datax.getAr_cp());
        }else {
            txtAcaDetCp.setText("-");
        }
        setColornya(datax.getAr_acpc(), txtAcaAcPc);
        if(!datax.getAr_acpc().equals("0.0") || !datax.getAr_acpc().equals("0.00") && datax.getAr_acpc().length()>3){
            String a = datax.getAr_acpc();
            Double b = Double.valueOf(a);
            DecimalFormat df = new DecimalFormat("#.##");
            txtAcaAcPc.setText(String.valueOf(df.format(b))+"%");
        }else{
            txtAcaAcPc.setText("0.00%");
        }
        if (datax.getAr_ac().length()>0){
            txtAcaAc.setText(NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(datax.getAr_ac()))));
        }else {
            txtAcaAc.setText("0");
        }
        if (datax.getAr_pc().length()>0){
            txtAcaPc.setText(NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(datax.getAr_pc()))));
        }else {
            txtAcaPc.setText("0");
        }

        return convertView;
    }

    private void setColornya(String val, TextView txt) {
        txt.setTextColor(ContextCompat.getColor(activity,R.color.colorPutih));
        if(Double.valueOf(val)<=40){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if(Double.valueOf(val)>40 && Double.valueOf(val)<80){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if(Double.valueOf(val)>=80){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }

}
