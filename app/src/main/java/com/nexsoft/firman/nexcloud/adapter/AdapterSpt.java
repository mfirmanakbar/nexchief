package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.SPTActivity;
import com.nexsoft.firman.nexcloud.data.DataPvsm;
import com.nexsoft.firman.nexcloud.data.DataSpt;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PATRA on 17-May-17.
 */

public class AdapterSpt extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataSpt> items_spt;

    private ArrayList<String> ar_date = new ArrayList<String>();
    private ArrayList<String> ar_entri1 = new ArrayList<String>();

    public AdapterSpt(Activity activity, List<DataSpt> items) {
        this.activity = activity;
        this.items_spt = items;
    }

    @Override
    public int getCount() {
        return items_spt.size();
    }

    @Override
    public Object getItem(int position) {
        return items_spt.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_spt,null);
        }

        final TextView txtItemSptAr1 = (TextView)convertView.findViewById(R.id.txtItemSptAr1);
        final LineChart mChart = (LineChart) convertView.findViewById(R.id.chartBarSpt);

        DataSpt datax = items_spt.get(position);
        txtItemSptAr1.setText(datax.getAr_no()+" "+datax.getAr_1());

        ar_date.clear();
        ar_entri1.clear();

        for (int a = 0; a< SPTActivity.spinnerB1B2Spt.size(); a++) {
            Log.d("AdapterSptB1B2", SPTActivity.spinnerB1B2Spt.get(a));
            ar_date.add(SPTActivity.spinnerB1B2Spt.get(a));
        }

        String[] itemAnak = datax.getAr_2().toString().trim().split("\\~");
        Log.d("ar_entri1_getAr_2", String.valueOf(datax.getAr_2().toString()));
        for (int b = 0; b < itemAnak.length-2; b++){
            ar_entri1.add(itemAnak[b]+"~"+itemAnak[b+1]+"~"+itemAnak[b+2]+"~"+itemAnak[b+3]+"~"+itemAnak[b+4]+"~");
            b+=4;
        }

        Log.d("ar_entri1_size", String.valueOf(ar_entri1.size()));
        for (int b = 0; b < ar_entri1.size(); b++){ Log.d("ar_entri1", String.valueOf(ar_entri1.get(b))); }

        /*chart begin*/
        mChart.highlightValues(null);
        mChart.invalidate();
        mChart.clear();

        Legend l = mChart.getLegend();
        l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setTextColor(Color.parseColor("#000000"));

        mChart.getDescription().setEnabled(false);
        mChart.setDrawGridBackground(false);
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setEnabled(false);
        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextColor(Color.parseColor("#000000"));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(ar_date.size());
        xAxis.setValueFormatter(null);
        xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());

        if (SPTActivity.typeChartSpt.equals("ratio")){
            chartratio(mChart);
        }else if (SPTActivity.typeChartSpt.equals("dmssfa")){
            chartdmssfa(mChart);
        }else if (SPTActivity.typeChartSpt.equals("value")){
            chartvalue(mChart);
        }

        /*chart ending*/

        return convertView;
    }

    private void chartvalue(LineChart mChart) {
        ArrayList<Entry> entries1 = new ArrayList<Entry>();
        ArrayList<Entry> entries2 = new ArrayList<Entry>();
        ArrayList<Entry> entries3 = new ArrayList<Entry>();
        ArrayList<Entry> entries4 = new ArrayList<Entry>();

        for (int index = 0; index < ar_date.size(); index++){
            String[] itemAnak = ar_entri1.get(index).toString().trim().split("\\~");
            float ar0 = Float.parseFloat(itemAnak[0]);
            float ar1 = Float.parseFloat(itemAnak[1]);
            float ar2 = Float.parseFloat(itemAnak[2]);
            float ar4 = Float.parseFloat(itemAnak[4]);
            entries1.add(new Entry(index, ar0));
            entries2.add(new Entry(index, ar1));
            entries3.add(new Entry(index, ar2));
            entries4.add(new Entry(index, ar4));
        }

        LineDataSet set1 = new LineDataSet(entries1, "Plan Call");
        set1.setDrawIcons(false);
        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(ContextCompat.getColor(activity,R.color.chartGreen));
        set1.setCircleColor(ContextCompat.getColor(activity,R.color.chartGreen));
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(false);
        set1.setFormLineWidth(1f);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);
        set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

        LineDataSet set2 = new LineDataSet(entries2, "Actual Call");
        set2.setDrawIcons(false);
        set2.enableDashedHighlightLine(10f, 5f, 0f);
        set2.setColor(ContextCompat.getColor(activity,R.color.chartBlue));
        set2.setCircleColor(ContextCompat.getColor(activity,R.color.chartBlue));
        set2.setLineWidth(1f);
        set2.setCircleRadius(3f);
        set2.setDrawCircleHole(false);
        set2.setValueTextSize(9f);
        set2.setDrawFilled(false);
        set2.setFormLineWidth(1f);
        set2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set2.setFormSize(15.f);
        set2.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

        LineDataSet set3 = new LineDataSet(entries3, "Effective Call SFA");
        set3.setDrawIcons(false);
        set3.enableDashedHighlightLine(10f, 5f, 0f);
        set3.setColor(ContextCompat.getColor(activity,R.color.chartRed));
        set3.setCircleColor(ContextCompat.getColor(activity,R.color.chartRed));
        set3.setLineWidth(1f);
        set3.setCircleRadius(3f);
        set3.setDrawCircleHole(false);
        set3.setValueTextSize(9f);
        set3.setDrawFilled(false);
        set3.setFormLineWidth(1f);
        set3.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set3.setFormSize(15.f);
        set3.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

        LineDataSet set4 = new LineDataSet(entries4, "Effective Call DMS");
        set4.setDrawIcons(false);
        set4.enableDashedHighlightLine(10f, 5f, 0f);
        set4.setColor(ContextCompat.getColor(activity,R.color.chartPink));
        set4.setCircleColor(ContextCompat.getColor(activity,R.color.chartPink));
        set4.setLineWidth(1f);
        set4.setCircleRadius(3f);
        set4.setDrawCircleHole(false);
        set4.setValueTextSize(9f);
        set4.setDrawFilled(false);
        set4.setFormLineWidth(1f);
        set4.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set4.setFormSize(15.f);
        set4.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);
        dataSets.add(set2);
        dataSets.add(set3);
        dataSets.add(set4);
        LineData data = new LineData(dataSets);
        mChart.setData(data);

    }

    private void chartdmssfa(LineChart mChart) {
        ArrayList<Entry> entries1 = new ArrayList<Entry>();

        for (int index = 0; index < ar_date.size(); index++){
            float cumulativeEntries1 = 0;
            String[] itemAnak = ar_entri1.get(index).toString().trim().split("\\~");
            float ar2 = Float.parseFloat(itemAnak[2]);
            float ar4 = Float.parseFloat(itemAnak[4]);
            if (ar2<=0 || ar4<=0){
                cumulativeEntries1=0;
            }else {
                cumulativeEntries1=(ar4*100)/ar2;
            }
            entries1.add(new Entry(index, cumulativeEntries1));
        }

        LineDataSet set1 = new LineDataSet(entries1, "Eff Call DMS vs Eff Call SFA");
        set1.setDrawIcons(false);
        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(ContextCompat.getColor(activity,R.color.chartPink));
        set1.setCircleColor(ContextCompat.getColor(activity,R.color.chartPink));
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(false);
        set1.setFormLineWidth(1f);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);
        set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);
        LineData data = new LineData(dataSets);
        mChart.setData(data);

    }

    private void chartratio(LineChart mChart) {
        ArrayList<Entry> entries1 = new ArrayList<Entry>();
        ArrayList<Entry> entries2 = new ArrayList<Entry>();

        for (int index = 0; index < ar_date.size(); index++){
            float cumulativeEntries1 = 0, cumulativeEntries2 = 0;
            String[] itemAnak = ar_entri1.get(index).toString().trim().split("\\~");
            float ar0 = Float.parseFloat(itemAnak[0]);
            float ar1 = Float.parseFloat(itemAnak[1]);
            float ar2 = Float.parseFloat(itemAnak[2]);
            if (ar0<=0){
                cumulativeEntries1=0;
                cumulativeEntries2=0;
            }else{
                cumulativeEntries1 = (ar1*100)/ar0;
                cumulativeEntries2 = (ar2*100)/ar0;
            }
            entries1.add(new Entry(index, cumulativeEntries1));
            entries2.add(new Entry(index, cumulativeEntries2));
        }

        LineDataSet set1 = new LineDataSet(entries1, "Route Coverage (AC to Plan Call)");
        set1.setDrawIcons(false);
        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(ContextCompat.getColor(activity,R.color.chartBlue));
        set1.setCircleColor(ContextCompat.getColor(activity,R.color.chartBlue));
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(false);
        set1.setFormLineWidth(1f);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);
        set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

        LineDataSet set2 = new LineDataSet(entries2, "Effective Call rate (EC to Plan Call)");
        set2.setDrawIcons(false);
        set2.enableDashedHighlightLine(10f, 5f, 0f);
        set2.setColor(ContextCompat.getColor(activity,R.color.chartRed));
        set2.setCircleColor(ContextCompat.getColor(activity,R.color.chartRed));
        set2.setLineWidth(1f);
        set2.setCircleRadius(3f);
        set2.setDrawCircleHole(false);
        set2.setValueTextSize(9f);
        set2.setDrawFilled(false);
        set2.setFormLineWidth(1f);
        set2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set2.setFormSize(15.f);
        set2.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);
        dataSets.add(set2);
        LineData data = new LineData(dataSets);
        mChart.setData(data);
    }

    private class MyCustomXAxisValueFormatter implements IAxisValueFormatter {
        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            //labels update
            if(ar_date.size() > (int) value) {
                return ar_date.get((int) value);
            } else return null;
        }
    }

}
