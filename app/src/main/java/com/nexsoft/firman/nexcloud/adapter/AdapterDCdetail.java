package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDC;
import com.nexsoft.firman.nexcloud.data.DataDCdetail;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by firmanmac on 5/2/17.
 */

public class AdapterDCdetail extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDCdetail> items_dc_det;

    public AdapterDCdetail(Activity activity, List<DataDCdetail> items) {
        this.activity = activity;
        this.items_dc_det = items;
    }

    @Override
    public int getCount() {
        return items_dc_det.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dc_det.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dc_detail,null);
        }

        final DecimalFormat df = new DecimalFormat("#.##");

        final ImageView iconformaps = (ImageView)convertView.findViewById(R.id.iconformaps);

        final TextView txtDCDetnoNama = (TextView)convertView.findViewById(R.id.txtDCDetnoNama);
        final TextView txtDCDetType = (TextView)convertView.findViewById(R.id.txtDCDetType);

        final TextView txtDcDetChartRc = (TextView)convertView.findViewById(R.id.txtDcDetChartRc);
        final ProgressBar pgDcDetChartRc = (ProgressBar)convertView.findViewById(R.id.pgDcDetChartRc);
        final TextView txtDcDetRcAc = (TextView)convertView.findViewById(R.id.txtDcDetRcAc);
        final TextView txtDcDetRcPl = (TextView)convertView.findViewById(R.id.txtDcDetRcPl);

        final TextView txtDcDetChartEcr = (TextView)convertView.findViewById(R.id.txtDcDetChartEcr);
        final ProgressBar pgDcDetChartEcr = (ProgressBar)convertView.findViewById(R.id.pgDcDetChartEcr);
        final TextView txtDcDetEcrEc = (TextView)convertView.findViewById(R.id.txtDcDetEcrEc);
        final TextView txtDcDetEcrPc = (TextView)convertView.findViewById(R.id.txtDcDetEcrPc);

        final TextView txtDcDetSkuDC = (TextView)convertView.findViewById(R.id.txtDcDetSkuDC);

        final TextView txtDcDetChartSod = (TextView)convertView.findViewById(R.id.txtDcDetChartSod);
        final ProgressBar pgDcDetChartSod = (ProgressBar)convertView.findViewById(R.id.pgDcDetChartSod);
        final TextView txtDcDetSodSales = (TextView)convertView.findViewById(R.id.txtDcDetSodSales);
        final TextView txtDcDetSodTarget = (TextView)convertView.findViewById(R.id.txtDcDetSodTarget);

        DataDCdetail datax = items_dc_det.get(position);

        String[] itemAnakSub = datax.getAr_0().toString().trim().split("\\~");
        if (itemAnakSub.length==3){
            txtDCDetnoNama.setText(itemAnakSub[1]);
            txtDCDetType.setText(itemAnakSub[2]);
        }else if (itemAnakSub.length==1){
            txtDCDetnoNama.setText(datax.getAr_0());
            txtDCDetType.setText("");
        }

        Double ar1 = Double.valueOf(datax.getAr_1());
        Double ar2 = Double.valueOf(datax.getAr_2());
        Double ar3 = Double.valueOf(datax.getAr_3());
        Double ar4 = Double.valueOf(datax.getAr_4());
        Double ar5 = Double.valueOf(datax.getAr_5());
        Double ar6 = Double.valueOf(datax.getAr_6());
        Double ar7 = Double.valueOf(datax.getAr_7());
        Double ar8 = Double.valueOf(datax.getAr_8());
        Double ar9 = Double.valueOf(datax.getAr_9());
        Double ar10 = Double.valueOf(datax.getAr_10());

        double c1 = 0;
        if (ar2>0 && ar1>0){
            c1 = (ar2*100.00)/ar1;
        }
        if ((c1 == Math.floor(c1)) && !Double.isInfinite(c1)) {
            txtDcDetChartRc.setText(df.format(c1) + ".00%");
        }else {
            txtDcDetChartRc.setText(df.format(c1) + "%");
        }
        setColornya(pgDcDetChartRc,c1,txtDcDetChartRc);
        pgDcDetChartRc.setProgress(Integer.parseInt(String.valueOf(c1).split("\\.")[0]));
        txtDcDetRcAc.setText(String.valueOf(Math.round(ar2)));
        txtDcDetRcPl.setText(String.valueOf(Math.round(ar1)));

        double c2 = 0;
        if (ar3>0 && ar1>0){
            c2 = (ar3*100.00)/ar1;
        }
        if ((c2 == Math.floor(c2)) && !Double.isInfinite(c2)) {
            txtDcDetChartEcr.setText(df.format(c2) + ".00%");
        }else {
            txtDcDetChartEcr.setText(df.format(c2) + "%");
        }
        setColornya(pgDcDetChartEcr,c2,txtDcDetChartEcr);
        pgDcDetChartEcr.setProgress(Integer.parseInt(String.valueOf(c2).split("\\.")[0]));
        txtDcDetEcrEc.setText(String.valueOf(Math.round(ar3)));
        txtDcDetEcrPc.setText(String.valueOf(Math.round(ar1)));

        if(ar4>0) {
            txtDcDetSkuDC.setText(String.valueOf(ar4));
        }else {
            txtDcDetSkuDC.setText("0");
        }
        if (ar4>=4){
            txtDcDetSkuDC.setTextColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else{
            txtDcDetSkuDC.setTextColor(ContextCompat.getColor(activity,R.color.colorGrey4));
        }

        double c3 = 0;
        if (ar9>0 && ar10>0){
            c3 = (ar9*100.00)/ar10;
        }
        if ((c3 == Math.floor(c3)) && !Double.isInfinite(c3)) {
            txtDcDetChartSod.setText(df.format(c3) + ".00%");
        }else {
            txtDcDetChartSod.setText(df.format(c3) + "%");
        }
        setColornya(pgDcDetChartSod,c3,txtDcDetChartSod);
        pgDcDetChartSod.setProgress(Integer.parseInt(String.valueOf(c3).split("\\.")[0]));
        if (ar9>1000.00){
            ar9/=1000.00;
        }
        if (ar10>1000.00){
            ar10/=1000.00;
        }
        txtDcDetSodSales.setText(NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(ar9)))));
        txtDcDetSodTarget.setText(NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(ar10)))));

        setIconMaps(datax.getAr_11(), iconformaps);

        return convertView;
    }

    private void setIconMaps(String ar_11, ImageView iconformaps) {
        Log.d("dc_ar_11", ar_11);
        if(!ar_11.equals("null")) {
            iconformaps.setColorFilter(ContextCompat.getColor(activity,R.color.colorRed1));
        }else{
            iconformaps.setColorFilter(ContextCompat.getColor(activity,R.color.colorGrey1));
        }
    }

    private void setColornya(ProgressBar progressBar, double val, TextView txt) {
        if((int)Math.ceil(val)<40){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorRed1), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if((int)Math.ceil(val)>=40 && (int)Math.ceil(val)<=80){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorYellow), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if((int)Math.ceil(val)>80){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorGreen), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }

}
