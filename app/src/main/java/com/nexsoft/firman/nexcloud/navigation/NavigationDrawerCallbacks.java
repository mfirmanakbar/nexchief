package com.nexsoft.firman.nexcloud.navigation;

/**
 * Created by poliveira on 27/10/2014.
 */
public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
