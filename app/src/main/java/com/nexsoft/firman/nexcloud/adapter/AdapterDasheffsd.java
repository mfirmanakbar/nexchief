package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDasheffsd;
import com.nexsoft.firman.nexcloud.util.MyRainbow;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDasheffsd extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDasheffsd> items_dasheffsd;
    private final MyRainbow mrb = new MyRainbow();

    public AdapterDasheffsd(Activity activity, List<DataDasheffsd> items) {
        this.activity = activity;
        this.items_dasheffsd = items;
    }

    @Override
    public int getCount() {
        return items_dasheffsd.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dasheffsd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dasheffsd, null);
        }

        final TextView txtDasheffsdAr1 = (TextView) convertView.findViewById(R.id.txtDasheffsdAr1);
        final TextView txtDasheffsdAr2 = (TextView) convertView.findViewById(R.id.txtDasheffsdAr2);
        final TextView txtDasheffsdAr3 = (TextView) convertView.findViewById(R.id.txtDasheffsdAr3);
        final TextView txtDasheffsdAr4 = (TextView) convertView.findViewById(R.id.txtDasheffsdAr4);
        final TextView txtDasheffsdAr5 = (TextView) convertView.findViewById(R.id.txtDasheffsdAr5);
        final TextView txtDasheffsdAr6 = (TextView) convertView.findViewById(R.id.txtDasheffsdAr6);
        final TextView txtDasheffsdAr7 = (TextView) convertView.findViewById(R.id.txtDasheffsdAr7);
        final LinearLayout layout_dasheffsd = (LinearLayout) convertView.findViewById(R.id.layout_dasheffsd);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dasheffsd);

        DataDasheffsd datax = items_dasheffsd.get(position);
        txtDasheffsdAr1.setText(datax.getAr_1());
        txtDasheffsdAr2.setText(datax.getAr_2());
        txtDasheffsdAr3.setText(datax.getAr_3());
        txtDasheffsdAr4.setText(datax.getAr_4());
        txtDasheffsdAr5.setText(datax.getAr_5());
        txtDasheffsdAr6.setText(datax.getAr_6());
        mrb.setColor(activity,Double.parseDouble(datax.getAr_7()),txtDasheffsdAr7);
        txtDasheffsdAr7.setText(datax.getAr_7()+"%");

        return convertView;
    }
}
