package com.nexsoft.firman.nexcloud;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.google.android.gms.maps.model.Dash;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudTgl;
import com.nexsoft.firman.nexcloud.navigation.NavigationCustom;
import com.nexsoft.firman.nexcloud.navigation.NavigationDrawerAdapter;
import com.nexsoft.firman.nexcloud.navigation.NavigationDrawerCallbacks;
import com.nexsoft.firman.nexcloud.navigation.NavigationDrawerFragment;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MySession;
import com.nexsoft.firman.nexcloud.util.MyTextFormat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static java.lang.Double.parseDouble;

public class DashboardPvmActivity extends AppCompatActivity implements NavigationDrawerCallbacks {

    private Toolbar mToolbar;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private int mYear, mMonth, mDay;
    private TextView txtDashboardDate, txtDashboardPesan, txtDsbWorkingDays, txtDsbPrimarySales1, txtDsbPrimarySales2,
            txtDsbSecondarySales1, txtDsbSecondarySales2, txtDsbDistributors, txtDsbSalesmanPvTotal, txtDsbSalesmanSfa,
            txtDsbSalesmanNonSfa, txtDsbSalesmanCanvas, txtDsbSalesmanTO, txtDsbSalesmanTotal,
            txtDsbOutlets1, txtDsbOutlets2, txtDsbDrop1Xml, txtDsbDrop2Xml, txtDsbRC1, txtDsbRC2, txtDsbECRsfa1, txtDsbECRsfa2,
            txtDsbECRdms1, txtDsbECRdms2, txtDsbedctes1, txtDsbedctes2, txtDashDateInfo, txtDashDateFrom, txtDashDateTo,
            txtDsbSalesmanSfaXmlx, txtDsbSalesmanNonSfaXmlx;
    private String KategoriDashboard = "MTD";
    private RadioButton btnMtd, btnYtd;
    //private ProgressDialog dialogprogress = null;
    private String TAG = "firman_DASH", sesiLoginUsername="",sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="", sesiLoginListDistLength="";
    private final MySession sesi = new MySession();;
    private String finalIPnya = "";
    private String rotasinya = "";
    private final MyTextFormat mtf = new MyTextFormat();
    private final int id_local_pk=1;
    int poss;
    private Handler handler = new Handler();
    private int hitTime=0;

    public static Activity DashboardPvmActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_pvm);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        DashboardPvmActivity = DashboardPvmActivity.this;

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesiLoginUsername = sesi.getUsername(getApplicationContext());
        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginListDistPetik = sesi.getListDistPetik(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());
        sesiLoginListDistLength = sesi.getListDistLength(getApplicationContext());

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        /*dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);*/

        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        mNavigationDrawerFragment.closeDrawer();//biar ga muncul langsung menunya

        txtDashboardDate = (TextView) findViewById(R.id.txtDashboardDateXML);
        txtDashboardPesan = (TextView) findViewById(R.id.txtDashboardPesanXML);
        txtDashboardPesan.setText("");
        btnMtd = (RadioButton) findViewById(R.id.btnDashboardMTDx);
        btnYtd = (RadioButton) findViewById(R.id.btnDashboardYTDx);
        txtDsbWorkingDays = (TextView) findViewById(R.id.txtDsbWorkingDaysXml);
        txtDsbPrimarySales1 = (TextView) findViewById(R.id.txtDsbPrimarySalesXml1);
        txtDsbPrimarySales2 = (TextView) findViewById(R.id.txtDsbPrimarySalesXml2);
        txtDsbSecondarySales1 = (TextView) findViewById(R.id.txtDsbSecondarySalesXml1);
        txtDsbSecondarySales2 = (TextView) findViewById(R.id.txtDsbSecondarySalesXml2);
        txtDsbDistributors = (TextView) findViewById(R.id.txtDsbDistributorsXml);
        txtDsbSalesmanPvTotal = (TextView) findViewById(R.id.txtDsbSalesmanPvTotalXml);
        txtDsbSalesmanSfa = (TextView) findViewById(R.id.txtDsbSalesmanSfaXml);
        txtDsbSalesmanNonSfa = (TextView) findViewById(R.id.txtDsbSalesmanNonSfaXml);
        txtDsbSalesmanTotal = (TextView) findViewById(R.id.txtDsbSalesmanTotalXml);
        txtDsbSalesmanTO = (TextView) findViewById(R.id.txtDsbSalesmanTOXml);
        txtDsbSalesmanCanvas = (TextView) findViewById(R.id.txtDsbSalesmanCanvasXml);
        txtDsbOutlets1 = (TextView) findViewById(R.id.txtDsbOutletsXml1);
        txtDsbOutlets2 = (TextView) findViewById(R.id.txtDsbOutletsXml2);
        txtDsbRC1 = (TextView) findViewById(R.id.txtDsbRCXml1);
        txtDsbRC2 = (TextView) findViewById(R.id.txtDsbRCXml2);
        txtDsbECRsfa1 = (TextView) findViewById(R.id.txtDsbECRsfaXml1);
        txtDsbECRsfa2 = (TextView) findViewById(R.id.txtDsbECRsfaXml2);
        txtDsbECRdms1 = (TextView) findViewById(R.id.txtDsbECRdmsXml1);
        txtDsbECRdms2 = (TextView) findViewById(R.id.txtDsbECRdmsXml2);
        txtDsbedctes1 = (TextView)findViewById(R.id.txtDsbedctesXml1);
        txtDsbedctes2 = (TextView)findViewById(R.id.txtDsbedctesXml2);
        txtDsbDrop1Xml = (TextView) findViewById(R.id.txtDsbDrop1Xml);
        txtDsbDrop2Xml = (TextView) findViewById(R.id.txtDsbDrop2Xml);
        txtDashDateInfo = (TextView) findViewById(R.id.txtDashDateInfo);
        txtDashDateFrom = (TextView) findViewById(R.id.txtDashDateFrom);
        txtDashDateTo = (TextView) findViewById(R.id.txtDashDateTo);
        txtDsbSalesmanSfaXmlx = (TextView) findViewById(R.id.txtDsbSalesmanSfaXmlx);
        txtDsbSalesmanNonSfaXmlx = (TextView) findViewById(R.id.txtDsbSalesmanNonSfaXmlx);

        clearUI();
        setDateNow();
        btnMtd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KategoriDashboard = "MTD";
                LoadDataFromServer();
            }
        });
        btnYtd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KategoriDashboard = "YTD";
                LoadDataFromServer();
            }
        });

        btnMtd.setChecked(true);


        //clearUI();
        LoadDataFromServer();
        /*hitTime=0;
        handler.postDelayed(runnable, MyConfig.delayToLoad);*/

    }

    private void cekRotation() {
        if (android.provider.Settings.System.getInt(getContentResolver(),Settings.System.ACCELEROMETER_ROTATION, 0) < 1){
            Toast.makeText(getApplicationContext(), "Mohon hidupkan Auto Screen Rotation!", Toast.LENGTH_LONG).show();
        }
    }

    /*private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hitTime+=1;
            if (hitTime<2) {
                handler.postDelayed(this, MyConfig.delayToLoad);
            }else {
                handler.removeCallbacks(runnable);
                LoadDataFromServer();
            }
            Log.e("ikan"+TAG,String.valueOf(hitTime));
        }
    };*/

    private void loadingStart() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.VISIBLE);
    }

    private void loadingStop() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.GONE);
    }

    public void klikDashnod(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DashnodActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDashou(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DashouActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDashprim(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DashprimActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDashsec(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DashsecActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDashsfa(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DashsfaActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDashact(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DashactActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDasheff(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DasheffActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDasheffd(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DasheffdActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDasheffsd(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DasheffsdActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    public void klikDashsales(View view){
        if (!txtDashDateFrom.getText().equals("0000-00-00") && !txtDashDateTo.getText().equals("0000-00-00") && !txtDashDateFrom.getText().equals("") && !txtDashDateTo.getText().equals("")){
            Intent p = new Intent(DashboardPvmActivity.this, DashsalesActivity.class);
            p.putExtra("txtDashDateFrom", txtDashDateFrom.getText().toString());
            p.putExtra("txtDashDateTo", txtDashDateTo.getText().toString());
            startActivity(p);
        }else {
            Toast.makeText(getApplicationContext(),"Date null !",Toast.LENGTH_SHORT).show();
        }
    }

    private void clearUI() {
        if (KategoriDashboard.equals("MTD")){
            txtDashDateInfo.setText("MONTH-TO-DATE");
        }else{
            txtDashDateInfo.setText("YEAR-TO-DATE");
        }
        txtDashDateFrom.setText("0000-00-00");
        txtDashDateTo.setText("0000-00-00");
        txtDsbWorkingDays.setText("0 of 0");
        txtDsbDistributors.setText("0");
        txtDsbSalesmanTotal.setText("0");
        txtDsbSalesmanTO.setText("0");
        txtDsbSalesmanCanvas.setText("0");
        txtDsbSalesmanPvTotal.setText("0");
        txtDsbSalesmanSfa.setText("0");
        txtDsbSalesmanNonSfa.setText("0");

        txtDsbOutlets1.setText("0");
        txtDsbOutlets2.setText("0");
        txtDsbRC1.setText("0");
        txtDsbRC2.setText("0");
        txtDsbECRsfa1.setText("0");
        txtDsbECRsfa2.setText("0");
        txtDsbECRdms1.setText("0");
        txtDsbECRdms2.setText("0");
        txtDsbedctes1.setText("0");
        txtDsbedctes2.setText("0");
        txtDsbSecondarySales1.setText("0");
        txtDsbSecondarySales2.setText("0");
        txtDsbPrimarySales1.setText("0");
        txtDsbPrimarySales2.setText("0");
        txtDsbSalesmanSfaXmlx.setText("~");
        txtDsbSalesmanNonSfaXmlx.setText("~");
    }

    private void setDateNow() {
        TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
        txtDashboardDate.setText(pos.getNowTglA());
    }

    private void LoadDataFromServer() {
        //cekRotation();
        Log.d("xxxTSTS","A1");
        //dialogprogress.show();
        loadingStart();
        if (!IsConnected.CheckInternet()==true){
            Log.d("xxxTSTS","A2");
            LoadDataFromLocal(id_local_pk);
            Toast.makeText(getApplicationContext(), getString(R.string.koneksi_internet_gagal), Toast.LENGTH_SHORT).show();
            //dialogprogress.dismiss();
            loadingStop();
            /*if (txtDashboardPesan.getVisibility() == View.GONE) {
                txtDashboardPesan.setVisibility(View.VISIBLE);
            }
            txtDashboardPesan.setText(getString(R.string.koneksi_internet_gagal));*/
        }else {

            TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
            pos.updateTbNexcloudTgl(txtDashboardDate.getText().toString(),"",1);

            if (txtDashboardPesan.getVisibility()==View.VISIBLE) {
                txtDashboardPesan.setVisibility(View.GONE);
            }

            String postDateTo = txtDashboardDate.getText().toString();
            final String urlnya = "http://"+finalIPnya+"/login/NexCloudAPI?MenuApp=MenuDash&txtdatethru="+postDateTo+
                    "&mode="+KategoriDashboard+
                    "&ldist="+sesiLoginListDistPetik+
                    "&getPrincipalID="+sesiLoginPrincipalID+
                    "&getListDistributorID="+sesiLoginDistributorID;

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                String content = res.getString("content");
                                String message = res.getString("message");
                                boolean success = res.getBoolean("success");
                                if(success==true) {
                                    if (content.equals("")) {
                                        //dialogprogress.dismiss();
                                        loadingStop();
                                        Toast.makeText(getApplicationContext(), "Tidak ada Data!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        updateDataLocal(txtDashboardDate.getText().toString(), content, id_local_pk);
                                        LoadDataFromLocal(id_local_pk);
                                    }
                                }else {
                                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {}
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        //dialogprogress.dismiss();
                        loadingStop();
                        LoadDataFromLocal(id_local_pk);
                        if (txtDashboardPesan.getVisibility()==View.GONE) {
                            txtDashboardPesan.setVisibility(View.VISIBLE);
                        }
                        txtDashboardPesan.setText(getString(R.string.koneksi_server_gagal));
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }


    private void updateDataLocal(String tgl, String all_result, int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        db.updateTbNexcloud(tgl, all_result, idlocal);
        db.close();
    }

    private void LoadDataFromLocal(int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        Cursor cur = db.getTbNexcloudByID(idlocal);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            Log.d("firman_test_local", cur.getString(0) +"_"+ cur.getString(1)+"_"+cur.getString(2)+"_"+cur.getString(3));
                            LoadToSetData(cur.getString(3),cur.getString(1));
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        db.close();
    }

    private void LoadToSetData(String valData, String tgl) {
        clearUI();

        if (!tgl.equals("")) {
            txtDashboardDate.setText(tgl);
            txtDashDateTo.setText(tgl);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date convertedDate = null;
            try {
                convertedDate = dateFormat.parse(tgl);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
            String hasil = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
            txtDashDateFrom.setText(hasil);
        }

        if (!valData.equals("")) {

            Log.d("firman_test_set_data", valData);
            String[] itemInduk = valData.toString().trim().split("\\|");

            Double i0 = Double.valueOf(itemInduk[0]); //[0]
            Double i1 = Double.valueOf(itemInduk[1]);
            Double t1;
            PieChart mChartWD = (PieChart) findViewById(R.id.chart_wd);
            if (i1>0) {
                t1 = (i0*100.00) / i1;
                setChart(t1, mChartWD);
            }else {
                setChart(0.00, mChartWD);
            }

            Double i3 = Double.valueOf(itemInduk[3]);
            Double i2 = Double.valueOf(itemInduk[2]);
            double t2;
            PieChart mChartPS = (PieChart) findViewById(R.id.chart_ps);
            if (i2>0) {
                t2 = (i3*100.00) / i2;
                setChart(t2, mChartPS);
            }else {
                setChart(0.00, mChartPS);
            }

            Double i5 = Double.valueOf(itemInduk[5]);
            double  t3;
            PieChart mChartSS = (PieChart) findViewById(R.id.chart_ss);
            if (i2>0) {
                t3 = (i5*100.00) / i2;
                setChart(t3, mChartSS);
            }else{
                setChart(0.00, mChartSS);
            }

            Double i10 = Double.valueOf(itemInduk[10]);
            Double i9 = Double.valueOf(itemInduk[9]);
            double t4;
            PieChart mChartOU = (PieChart) findViewById(R.id.chart_ou);
            if (i9>0) {
                t4 = ( i10 * 100.00) / i9;
                setChart(t4, mChartOU);
            }else{
                setChart(0.00, mChartOU);
            }

            Double i11 = Double.valueOf(itemInduk[11]);
            double drop=0;
            if(i11>0) {
                drop = i5 / i11;
            }


            Double i6 = Double.valueOf(itemInduk[6]); //Double i6 = Double.valueOf(itemInduk[6]);
            Double i7 = Double.valueOf(itemInduk[7]);
            Double i8 = Double.valueOf(itemInduk[8]);
            Double i12 = Double.valueOf(itemInduk[12]);
            Double i13 = Double.valueOf(itemInduk[13]);

            Double i15 = Double.valueOf(itemInduk[15]);
            Double i14 = Double.valueOf(itemInduk[14]);
            double t5;
            PieChart mChartRC = (PieChart) findViewById(R.id.chart_rc);
            if (i14>0) {
                t5 = ( i15 * 100.00) / i14;
                setChart(t5, mChartRC);
            }else{
                setChart(0.00, mChartRC);
            }

            Double i16 = Double.valueOf(itemInduk[16]);
            double t6;
            PieChart mChartEcrSfa = (PieChart) findViewById(R.id.chart_ecrsfa);
            if (i14>0) {
                t6 = ( i16 * 100.00) / i14;
                setChart(t6, mChartEcrSfa);
            }else{
                setChart(0.00, mChartEcrSfa);
            }

            Double i17 = Double.valueOf(itemInduk[17]);
            double t7;
            PieChart mChartEcrDms = (PieChart) findViewById(R.id.chart_ecrdms);
            if (i14>0) {
                t7 = ( i17 * 100.00) / i14;
                setChart(t7, mChartEcrDms);
            }else{
                setChart(0.00, mChartEcrDms);
            }

            double t8;
            PieChart mChartedctes = (PieChart) findViewById(R.id.chart_edctes);
            if (i16>0) {
                t8 = ( i17 * 100.00) / i16;
                setChart(t8, mChartedctes);
            }else{
                setChart(0.00, mChartedctes);
            }

            Double sfaP = (i12/(i12+i13)) * 100;
            Double nonSfaP = (i13/(i12+i13)) * 100;

            txtDsbWorkingDays.setText(mtf.format_angka(String.valueOf(Math.round(i0))) + " of " + mtf.format_angka(String.valueOf(Math.round(i1))));
            txtDsbPrimarySales1.setText(mtf.format_angka(String.valueOf(Math.round(i3))));
            txtDsbPrimarySales2.setText(mtf.format_angka(String.valueOf(Math.round(i2))));
            txtDsbSecondarySales1.setText(mtf.format_angka(String.valueOf(Math.round(i5))));
            txtDsbSecondarySales2.setText(mtf.format_angka(String.valueOf(Math.round(i2))));
            txtDsbDistributors.setText(mtf.format_angka(String.valueOf(Math.round(i6))));
            txtDsbSalesmanTO.setText(mtf.format_angka(String.valueOf(Math.round(i7))));
            txtDsbSalesmanCanvas.setText(mtf.format_angka(String.valueOf(Math.round(i8))));
            txtDsbSalesmanTotal.setText(mtf.format_angka(String.valueOf(Math.round(i7) + Math.round(i8))));
            txtDsbSalesmanSfa.setText(String.valueOf(mtf.format_desimal_1(i12)));
            txtDsbSalesmanNonSfa.setText(String.valueOf(mtf.format_desimal_1(i13)));
            txtDsbSalesmanSfaXmlx.setText(mtf.format_desimal_2(sfaP)+"%");
            txtDsbSalesmanNonSfaXmlx.setText(mtf.format_desimal_2(nonSfaP)+"%");
            txtDsbSalesmanPvTotal.setText(mtf.format_angka(String.valueOf(Math.round(i12) + Math.round(i13))));
            txtDsbOutlets1.setText(mtf.format_angka(String.valueOf(Math.round(i10))));
            txtDsbOutlets2.setText(mtf.format_angka(String.valueOf(Math.round(i9))));
            if (drop==0) {
                txtDsbDrop1Xml.setText(mtf.format_angka(String.valueOf(Math.round(i11))));
                txtDsbDrop2Xml.setText(mtf.format_angka(String.valueOf(0)));
            }else{
                txtDsbDrop1Xml.setText(mtf.format_angka(String.valueOf(Math.round(i11))));
                txtDsbDrop2Xml.setText(mtf.format_angka(String.valueOf(Math.round(drop))));
            }

            txtDsbRC1.setText(mtf.format_angka(String.valueOf(Math.round(i15))));
            txtDsbRC2.setText(mtf.format_angka(String.valueOf(Math.round(i14))));

            txtDsbECRsfa1.setText(mtf.format_angka(String.valueOf(Math.round(i16))));
            txtDsbECRsfa2.setText(mtf.format_angka(String.valueOf(Math.round(i14))));

            txtDsbECRdms1.setText(mtf.format_angka(String.valueOf(Math.round(i17))));
            txtDsbECRdms2.setText(mtf.format_angka(String.valueOf(Math.round(i14))));

            txtDsbedctes1.setText(mtf.format_angka(String.valueOf(Math.round(i17))));
            txtDsbedctes2.setText(mtf.format_angka(String.valueOf(Math.round(i16))));

        }/*else{
            Toast.makeText(getApplicationContext(),"Tidak ada data!",Toast.LENGTH_SHORT).show();
        }*/
        //dialogprogress.dismiss();
        loadingStop();
    }


    private void setChart(double valuenya, PieChart mChart) {
        mChart.setCenterTextSize(13f);
        mChart.setCenterTextColor(Color.BLACK);
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.TRANSPARENT);
        mChart.setTransparentCircleColor(Color.TRANSPARENT);
        mChart.setTransparentCircleAlpha(0);
        mChart.getDescription().setEnabled(false);
        mChart.setMaxAngle(180f); //HalfChart
        mChart.setRotationAngle(180f); //HalfChart
        mChart.setCenterTextOffset(0, -13); //HalfChart
        mChart.setRotationEnabled(false);

        double percentage = valuenya;
        double akhir = 100.00 - percentage;
        mChart.setCenterText("");
        mChart.setCenterText(mChart.getCenterText()+"\n"+String.valueOf(mtf.format_desimal_2(percentage))+"%");
        ArrayList<PieEntry> values = new ArrayList<PieEntry>();
        values.add(new PieEntry((int) Math.ceil(percentage),0));
        if((int) Math.ceil(percentage)<100) {
            values.add(new PieEntry((int) Math.ceil(akhir), 0));
        }

        PieDataSet dataSet = new PieDataSet(values, "Data");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(5f);
        if((int)Math.ceil(percentage)<=40){
            dataSet.setColors(ContextCompat.getColor(getApplicationContext(), R.color.colorRed1), ContextCompat.getColor(getApplicationContext(), R.color.colorGrey1));
        }else if((int)Math.ceil(percentage)>40 && (int)Math.ceil(percentage)<=80){
            dataSet.setColors(ContextCompat.getColor(getApplicationContext(), R.color.colorYellow), ContextCompat.getColor(getApplicationContext(), R.color.colorGrey1));
        }else if((int)Math.ceil(percentage)>80){
            dataSet.setColors(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen), ContextCompat.getColor(getApplicationContext(), R.color.colorGrey1));
        }
        dataSet.setSelectionShift(0f);
        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.TRANSPARENT);
        mChart.setData(data);
        mChart.invalidate();
        mChart.animateY(1000, Easing.EasingOption.EaseInOutQuad);
        Legend l = mChart.getLegend();
        l.setEnabled(false);
    }

    public void onClickDate(View view){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear+1;
                        int tgl = dayOfMonth;
                        String xBulan = "";
                        String xTgl = "";
                        if (bulan<10){
                            xBulan = "0"+String.valueOf(bulan);
                        }else{
                            xBulan = String.valueOf(bulan);
                        }
                        if (tgl<10){
                            xTgl = "0"+String.valueOf(tgl);
                        }else{
                            xTgl = String.valueOf(tgl);
                        }
                        txtDashboardDate.setText(year+"-"+xBulan+"-"+xTgl);
                    }
                }, mYear, mMonth, mDay);
        //dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //untuk disable past date from today
        dpd.show();

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        poss=position;
        NavigationCustom ok = new NavigationCustom(DashboardPvmActivity.this);
        ok.menu_apps(position);
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen()) {
            mNavigationDrawerFragment.closeDrawer();
        }
        else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main4, menu);
        MenuItem itemx = (MenuItem) menu.findItem(R.id.action_sr);
        if (poss==0){
            itemx.setVisible(true);
        }else{
            itemx.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_sr:
                int orientation=DashboardPvmActivity.this.getResources().getConfiguration().orientation;
                if(orientation==Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

}
