package com.nexsoft.firman.nexcloud.data;

/**
 * Created by PATRA on 20-Mar-17.
 */

public class DataSPA {
    int ar_pk, ar_no, ar_lokasi;
    String ar_nama, ar_pc, ar_ac, ar_ec, ar_sku, ar_ete, ar_ds;

    public DataSPA() {
    }

    public DataSPA(int ar_pk, int ar_no, int ar_lokasi, String ar_nama, String ar_pc, String ar_ac, String ar_ec, String ar_sku, String ar_ete, String ar_ds) {
        this.ar_pk = ar_pk;
        this.ar_no = ar_no;
        this.ar_lokasi = ar_lokasi;
        this.ar_nama = ar_nama;
        this.ar_pc = ar_pc;
        this.ar_ac = ar_ac;
        this.ar_ec = ar_ec;
        this.ar_sku = ar_sku;
        this.ar_ete = ar_ete;
        this.ar_ds = ar_ds;
    }


    public int getAr_pk() {
        return ar_pk;
    }

    public void setAr_pk(int ar_pk) {
        this.ar_pk = ar_pk;
    }

    public int getAr_no() {
        return ar_no;
    }

    public void setAr_no(int ar_no) {
        this.ar_no = ar_no;
    }

    public int getAr_lokasi() {
        return ar_lokasi;
    }

    public void setAr_lokasi(int ar_lokasi) {
        this.ar_lokasi = ar_lokasi;
    }

    public String getAr_pc() {
        return ar_pc;
    }

    public void setAr_pc(String ar_pc) {
        this.ar_pc = ar_pc;
    }

    public String getAr_ac() {
        return ar_ac;
    }

    public void setAr_ac(String ar_ac) {
        this.ar_ac = ar_ac;
    }

    public String getAr_ec() {
        return ar_ec;
    }

    public void setAr_ec(String ar_ec) {
        this.ar_ec = ar_ec;
    }

    public String getAr_sku() {
        return ar_sku;
    }

    public void setAr_sku(String ar_sku) {
        this.ar_sku = ar_sku;
    }

    public String getAr_nama() {
        return ar_nama;
    }

    public void setAr_nama(String ar_nama) {
        this.ar_nama = ar_nama;
    }

    public String getAr_ete() {
        return ar_ete;
    }

    public void setAr_ete(String ar_ete) {
        this.ar_ete = ar_ete;
    }

    public String getAr_ds() {
        return ar_ds;
    }

    public void setAr_ds(String ar_ds) {
        this.ar_ds = ar_ds;
    }

}
