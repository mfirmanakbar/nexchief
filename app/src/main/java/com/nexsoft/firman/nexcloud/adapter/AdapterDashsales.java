package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDashsales;
import com.nexsoft.firman.nexcloud.util.MyRainbow;
import com.nexsoft.firman.nexcloud.util.MyTextFormat;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDashsales extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDashsales> items_dashsales;
    private final MyTextFormat mtf = new MyTextFormat();

    public AdapterDashsales(Activity activity, List<DataDashsales> items) {
        this.activity = activity;
        this.items_dashsales = items;
    }

    @Override
    public int getCount() {
        return items_dashsales.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dashsales.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dashsales, null);
        }

        final TextView txtDashsalesAr1 = (TextView) convertView.findViewById(R.id.txtDashsalesAr1);
        final TextView txtDashsalesAr2 = (TextView) convertView.findViewById(R.id.txtDashsalesAr2);
        final TextView txtDashsalesAr3 = (TextView) convertView.findViewById(R.id.txtDashsalesAr3);
        final TextView txtDashsalesAr4 = (TextView) convertView.findViewById(R.id.txtDashsalesAr4);
        final TextView txtDashsalesAr5 = (TextView) convertView.findViewById(R.id.txtDashsalesAr5);
        final TextView txtDashsalesAr6 = (TextView) convertView.findViewById(R.id.txtDashsalesAr6);
        final TextView txtDashsalesAr7 = (TextView) convertView.findViewById(R.id.txtDashsalesAr7);
        final TextView txtDashsalesAr8 = (TextView) convertView.findViewById(R.id.txtDashsalesAr8);
        final LinearLayout layout_dashsales = (LinearLayout) convertView.findViewById(R.id.layout_dashsales);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dashsales);

        DataDashsales datax = items_dashsales.get(position);
        txtDashsalesAr1.setText(datax.getAr_1());
        if (datax.getAr_2().equals("")){
            txtDashsalesAr2.setText("");
        }else {
            if ((convertView.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
                txtDashsalesAr2.setText(mtf.format_html("<b>Distributor Code:</b> "+datax.getAr_2()));
            }else{
                txtDashsalesAr2.setText(datax.getAr_2());
            }
        }
        if (datax.getAr_3().equals("")){
            txtDashsalesAr3.setText("");
        }else {
            if ((convertView.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
                txtDashsalesAr3.setText(mtf.format_html("<b>Distributor Name:</b><br>"+datax.getAr_3()));
            }else{
                txtDashsalesAr3.setText(datax.getAr_3());
            }
        }
        if (datax.getAr_4().equals("")){
            txtDashsalesAr4.setText("");
        }else {
            if ((convertView.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
                txtDashsalesAr4.setText(mtf.format_html("<b>City:</b> "+datax.getAr_4()));
            }else{
                txtDashsalesAr4.setText(datax.getAr_4());
            }
        }
        txtDashsalesAr5.setText(datax.getAr_5());
        txtDashsalesAr6.setText(datax.getAr_6());
        txtDashsalesAr7.setText(datax.getAr_7());
        txtDashsalesAr8.setText(datax.getAr_8());

        return convertView;
    }
}
