package com.nexsoft.firman.nexcloud;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nexsoft.firman.nexcloud.adapter.AdapterACAdetail;
import com.nexsoft.firman.nexcloud.adapter.AdapterDCdetail;
import com.nexsoft.firman.nexcloud.data.DataACAdetail;
import com.nexsoft.firman.nexcloud.data.DataDCdetail;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;

import java.util.ArrayList;
import java.util.List;

public class DcDetActivity extends AppCompatActivity {

    private ArrayList<String> ar_0 = new ArrayList<String>();
    private ArrayList<String> ar_1 = new ArrayList<String>();
    private ArrayList<String> ar_2 = new ArrayList<String>();
    private ArrayList<String> ar_3 = new ArrayList<String>();
    private ArrayList<String> ar_4 = new ArrayList<String>();
    private ArrayList<String> ar_5 = new ArrayList<String>();
    private ArrayList<String> ar_6 = new ArrayList<String>();
    private ArrayList<String> ar_7 = new ArrayList<String>();
    private ArrayList<String> ar_8 = new ArrayList<String>();
    private ArrayList<String> ar_9 = new ArrayList<String>();
    private ArrayList<String> ar_10 = new ArrayList<String>();
    private ArrayList<String> ar_11 = new ArrayList<String>();
    private String TAG = "firman_DC_detail";

    private ListView list;
    private AdapterDCdetail adapter;
    private List<DataDCdetail> itemList = new ArrayList<DataDCdetail>();
    private CoordinatorLayout dc_det_CoordinatorLayout;
    private View.OnClickListener mOnClickListener;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dc_det);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        dc_det_CoordinatorLayout = (CoordinatorLayout) findViewById(R.id.dc_det_CoordinatorLayout);

        list    = (ListView) findViewById(R.id.listViewDCDet);
        list.setDivider(null);
        adapter = new AdapterDCdetail(DcDetActivity.this, itemList);
        list.setAdapter(adapter);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String lemparDetail = extras.getString("lemparDetailDc");
            Log.d("DataDC", lemparDetail);
            saveDetail(lemparDetail);
        }

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(ar_0.size()>1) {
                    String maps_toko = itemList.get(position).getAr_11();
                    Log.d("maps_toko", maps_toko);
                    if(!maps_toko.equals("null")) {
                        Intent maps = new Intent(DcDetActivity.this, DcMapsActivity.class);
                        maps.putExtra("lemparMaps", maps_toko);
                        startActivity(maps);
                        //overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
                    }else{
                        String pesan="Maaf, Data Maps tidak tersedia.";
                        String aksi = "Done";

                        snackbar = Snackbar.make(dc_det_CoordinatorLayout, pesan, Snackbar.LENGTH_LONG)
                                .setAction(aksi, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        snackbar.dismiss();
                                    }
                                });
                        snackbar.setActionTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary));
                        snackbar.show();

                    }
                }
            }
        });

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main4, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
            }
            /*case R.id.action_sr:{
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation== Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveDetail(String lemparDetail) {

        //kosongin array
        ar_0.clear();
        ar_1.clear();
        ar_2.clear();
        ar_3.clear();
        ar_4.clear();
        ar_5.clear();
        ar_6.clear();
        ar_7.clear();
        ar_8.clear();
        ar_9.clear();
        ar_10.clear();
        ar_11.clear();

        String[] itemInduk = lemparDetail.toString().trim().split("\\^");

        for (int a=1; a<itemInduk.length; a++){
            Log.d("DCDCDET", itemInduk[a]);
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_0.add(itemAnak[0]);
            ar_1.add(itemAnak[1]);
            ar_2.add(itemAnak[2]);
            ar_3.add(itemAnak[3]);
            ar_4.add(itemAnak[4]);
            ar_5.add(itemAnak[5]);
            ar_6.add(itemAnak[6]);
            ar_7.add(itemAnak[7]);
            ar_8.add(itemAnak[8]);
            ar_9.add(itemAnak[9]);
            ar_10.add(itemAnak[10]);
            Log.d("Coba_10", itemAnak[10] + " ~ Length:" + itemAnak.length);
            if (itemAnak.length>=12){
                ar_11.add(itemAnak[11]);
            }else if(itemAnak.length<=11){
                ar_11.add("null");
            }
        }

        lihatDataArray();
        setListview();
    }

    private void setListview() {
        for (int a=0; a<ar_0.size(); a++){
            DataDCdetail ita = new DataDCdetail();
            ita.setAr_0(ar_0.get(a));
            ita.setAr_1(ar_1.get(a));
            ita.setAr_2(ar_2.get(a));
            ita.setAr_3(ar_3.get(a));
            ita.setAr_4(ar_4.get(a));
            ita.setAr_5(ar_5.get(a));
            ita.setAr_6(ar_6.get(a));
            ita.setAr_7(ar_7.get(a));
            ita.setAr_8(ar_8.get(a));
            ita.setAr_9(ar_9.get(a));
            ita.setAr_10(ar_10.get(a));
            ita.setAr_11(ar_11.get(a));
            itemList.add(ita);
        }
    }

    private void lihatDataArray() {
        for (int i=0; i<ar_0.size(); i++){
            Log.d(TAG, "Array DC Det " +
                    String.valueOf(ar_0.get(i)) + "_"+
                    String.valueOf(ar_1.get(i)) + "_"+
                    String.valueOf(ar_2.get(i)) + "_"+
                    String.valueOf(ar_3.get(i)) + "_"+
                    String.valueOf(ar_4.get(i)) + "_"+
                    String.valueOf(ar_5.get(i)) + "_"+
                    String.valueOf(ar_6.get(i)) + "_"+
                    String.valueOf(ar_7.get(i)) + "_"+
                    String.valueOf(ar_8.get(i)) + "_"+
                    String.valueOf(ar_9.get(i)) + "_"+
                    String.valueOf(ar_10.get(i)) + "_"+
                    String.valueOf(ar_11.get(i))
            );
            Log.d("A11",  String.valueOf(ar_11.get(i)));
        }
    }

}
