package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDashsec;
import com.nexsoft.firman.nexcloud.util.MyRainbow;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDashsec extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDashsec> items_dashsec;
    private final MyRainbow mrb = new MyRainbow();

    public AdapterDashsec(Activity activity, List<DataDashsec> items) {
        this.activity = activity;
        this.items_dashsec = items;
    }

    @Override
    public int getCount() {
        return items_dashsec.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dashsec.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dashsec, null);
        }

        final TextView txtDashsecAr1 = (TextView) convertView.findViewById(R.id.txtDashsecAr1);
        final TextView txtDashsecAr2 = (TextView) convertView.findViewById(R.id.txtDashsecAr2);
        final TextView txtDashsecAr3 = (TextView) convertView.findViewById(R.id.txtDashsecAr3);
        final TextView txtDashsecAr4 = (TextView) convertView.findViewById(R.id.txtDashsecAr4);
        final TextView txtDashsecAr5 = (TextView) convertView.findViewById(R.id.txtDashsecAr5);
        final TextView txtDashsecAr6 = (TextView) convertView.findViewById(R.id.txtDashsecAr6);
        final TextView txtDashsecAr7 = (TextView) convertView.findViewById(R.id.txtDashsecAr7);
        final LinearLayout layout_dashsec = (LinearLayout) convertView.findViewById(R.id.layout_dashsec);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dashsec);

        DataDashsec datax = items_dashsec.get(position);
        txtDashsecAr1.setText(datax.getAr_1());
        txtDashsecAr2.setText(datax.getAr_2());
        txtDashsecAr3.setText(datax.getAr_3());
        txtDashsecAr4.setText(datax.getAr_4());
        txtDashsecAr5.setText(datax.getAr_5());
        txtDashsecAr6.setText(datax.getAr_6());
        mrb.setColor(activity,Double.parseDouble(datax.getAr_7()),txtDashsecAr7);
        txtDashsecAr7.setText(datax.getAr_7()+"%");

        return convertView;
    }
}
