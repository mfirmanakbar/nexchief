package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDashnod;
import com.nexsoft.firman.nexcloud.util.MyRainbow;
import com.nexsoft.firman.nexcloud.util.MyTextFormat;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDashnod extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDashnod> items_dashnod;

    public AdapterDashnod(Activity activity, List<DataDashnod> items) {
        this.activity = activity;
        this.items_dashnod = items;
    }

    @Override
    public int getCount() {
        return items_dashnod.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dashnod.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dashnod, null);
        }

        final TextView txtDashnodAr1 = (TextView) convertView.findViewById(R.id.txtDashnodAr1);
        final TextView txtDashnodAr2 = (TextView) convertView.findViewById(R.id.txtDashnodAr2);
        final TextView txtDashnodAr3 = (TextView) convertView.findViewById(R.id.txtDashnodAr3);
        final TextView txtDashnodAr4 = (TextView) convertView.findViewById(R.id.txtDashnodAr4);
        final TextView txtDashnodAr5_6 = (TextView) convertView.findViewById(R.id.txtDashnodAr5_6);
        final LinearLayout layout_dashnod = (LinearLayout) convertView.findViewById(R.id.layout_dashnod);

        final MyTextFormat mtf = new MyTextFormat();

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dashnod);

        DataDashnod datax = items_dashnod.get(position);
        if (!datax.getAr_1().equals("0")) {
            txtDashnodAr1.setText(datax.getAr_1());
        }else{
            txtDashnodAr1.setText("");
        }
        txtDashnodAr2.setText(datax.getAr_2());
        txtDashnodAr3.setText(datax.getAr_3());
        txtDashnodAr4.setText(datax.getAr_4());
        txtDashnodAr5_6.setText(mtf.format_html("<b>City :</b> "+datax.getAr_6()+". <br><b>Address:</b> "+datax.getAr_5()));


        return convertView;
    }
}
