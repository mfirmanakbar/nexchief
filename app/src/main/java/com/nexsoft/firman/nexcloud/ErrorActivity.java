package com.nexsoft.firman.nexcloud;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudTgl;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MySession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ErrorActivity extends AppCompatActivity {

    private String getMyError="", getMyError2="", sesiLoginUsername="";
    private MySession sesi;
    private ProgressDialog dialogprogress = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        sesi = new MySession();
        sesiLoginUsername = sesi.getUsername(getApplicationContext());

        dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            getMyError = extras.getString("error_nya");
            getMyError2 = extras.getString("error_nya2");
        }

    }

    public void klik_send_error(View view){
        sendTele("Nexcloud (Username:"+sesiLoginUsername+")", getMyError2);
        //sendGmail("Nexcloud (Username:"+sesiLoginUsername+")", getMyError);
    }

    private void sendTele(final String userid, final String getMyError2) {
        Log.d("oop_firman_oop", getMyError2);
        dialogprogress.show();
        if (!IsConnected.CheckInternet()==true){
            Toast.makeText(getApplicationContext(),getString(R.string.koneksi_internet_gagal),Toast.LENGTH_SHORT).show();
            dialogprogress.dismiss();
        }else {

            String urlnya = "https://api.telegram.org/bot310270877:AAH_Q_EBi1-U3FmayaXSjSz4-v_iN06sjbQ/sendMessage?chat_id=157627066&text="+getMyError2+"&parse_mode=markdown";

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(urlnya)
                    .get()
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override public void onFailure(Call call, IOException e) {
                    try {
                        dialogprogress.dismiss();
                        Toast.makeText(getApplicationContext(),"Terimakasih. [F1]",Toast.LENGTH_SHORT).show();
                    }catch (Exception a){}
                }
                @Override public void onResponse(Call call, Response response) throws IOException {
                    final String textResponse = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override public void run() {
                            try {
                                dialogprogress.dismiss();
                                JSONObject res = new JSONObject(textResponse);
                                boolean success = res.getBoolean("ok");
                                if(success==true) {
                                    Toast.makeText(getApplicationContext(),"Terimakasih. [T]",Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(getApplicationContext(),"Terimakasih. [F2]",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }
    }


    public void sendGmail(String subject, String text) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:firman.programmer@nexsoft.co.id"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(emailIntent, "Pilih Email"));
    }

}
