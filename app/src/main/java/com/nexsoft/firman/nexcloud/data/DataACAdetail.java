package com.nexsoft.firman.nexcloud.data;

/**
 * Created by PATRA on 10-Apr-17.
 */

public class DataACAdetail {

    String ar_dist, ar_dist_name, ar_city, ar_cp, ar_acpc, ar_ac, ar_pc;

    public DataACAdetail() {
    }

    public DataACAdetail(String ar_dist, String ar_dist_name, String ar_city, String ar_cp, String ar_acpc, String ar_ac, String ar_pc) {
        this.ar_dist = ar_dist;
        this.ar_dist_name = ar_dist_name;
        this.ar_city = ar_city;
        this.ar_cp = ar_cp;
        this.ar_acpc = ar_acpc;
        this.ar_ac = ar_ac;
        this.ar_pc = ar_pc;
    }

    public String getAr_dist() {
        return ar_dist;
    }

    public void setAr_dist(String ar_dist) {
        this.ar_dist = ar_dist;
    }

    public String getAr_dist_name() {
        return ar_dist_name;
    }

    public void setAr_dist_name(String ar_dist_name) {
        this.ar_dist_name = ar_dist_name;
    }

    public String getAr_city() {
        return ar_city;
    }

    public void setAr_city(String ar_city) {
        this.ar_city = ar_city;
    }

    public String getAr_cp() {
        return ar_cp;
    }

    public void setAr_cp(String ar_cp) {
        this.ar_cp = ar_cp;
    }

    public String getAr_acpc() {
        return ar_acpc;
    }

    public void setAr_acpc(String ar_acpc) {
        this.ar_acpc = ar_acpc;
    }

    public String getAr_ac() {
        return ar_ac;
    }

    public void setAr_ac(String ar_ac) {
        this.ar_ac = ar_ac;
    }

    public String getAr_pc() {
        return ar_pc;
    }

    public void setAr_pc(String ar_pc) {
        this.ar_pc = ar_pc;
    }
}
