package com.nexsoft.firman.nexcloud;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.nexsoft.firman.nexcloud.adapter.AdapterACAdetail;
import com.nexsoft.firman.nexcloud.adapter.AdapterSSAdetail;
import com.nexsoft.firman.nexcloud.data.DataACA;
import com.nexsoft.firman.nexcloud.data.DataACAdetail;
import com.nexsoft.firman.nexcloud.data.DataSSAdetail;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.ModeCoding;

import java.util.ArrayList;
import java.util.List;

public class ActualcallDetailActivity extends AppCompatActivity {

    private ListView list;
    private AdapterACAdetail adapter;
    private List<DataACAdetail> itemList = new ArrayList<DataACAdetail>();

    private ArrayList<String> ar_dist = new ArrayList<String>();
    private ArrayList<String> ar_dist_name = new ArrayList<String>();
    private ArrayList<String> ar_city = new ArrayList<String>();
    private ArrayList<String> ar_cp = new ArrayList<String>();
    private ArrayList<String> ar_acpc = new ArrayList<String>();
    private ArrayList<String> ar_ac = new ArrayList<String>();
    private ArrayList<String> ar_pc = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualcall_detail);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String lemparDetail = extras.getString("lemparDetailAca");
            Log.d("DataACA", lemparDetail);
            saveDetail(lemparDetail);
        }

        list = (ListView)findViewById(R.id.listViewACAdetail);

        adapter = new AdapterACAdetail(ActualcallDetailActivity.this, itemList);
        list.setAdapter(adapter);

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main4, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
            }
            /*case R.id.action_sr:{
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation== Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveDetail(String lemparDetail){
        //kosongin array
        ar_dist.clear();
        ar_dist_name.clear();
        ar_city.clear();
        ar_cp.clear();
        ar_acpc.clear();
        ar_ac.clear();
        ar_pc.clear();

        //Simpan PK
        String[] itemInduk = lemparDetail.toString().trim().split("\\~");
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\^");
            ar_dist.add(itemAnak[0]);
            ar_dist_name.add(itemAnak[1]);
            ar_city.add(itemAnak[2]);
            ar_cp.add(itemAnak[3]);
            ar_acpc.add(itemAnak[4]);
            ar_ac.add(itemAnak[5]);
            ar_pc.add(itemAnak[6]);
        }

        //lihat array
        for (int i=0; i<ar_dist.size(); i++){
            Log.d("ACA DETAIL", "Array ACA " +
                    String.valueOf(ar_dist.get(i)) + "_"+
                    String.valueOf(ar_dist_name.get(i)) + "_"+
                    String.valueOf(ar_city.get(i)) + "_" +
                    String.valueOf(ar_cp.get(i)) + "_"+
                    String.valueOf(ar_acpc.get(i)) + "_"+
                    String.valueOf(ar_ac.get(i)) + "_"+
                    String.valueOf(ar_pc.get(i))
            );
        }

        setListview();

    }

    private void setListview() {
        for (int a=0; a<ar_dist.size(); a++){
            DataACAdetail ita = new DataACAdetail();
            ita.setAr_dist(ar_dist.get(a));
            ita.setAr_dist_name(ar_dist_name.get(a));
            ita.setAr_city(ar_city.get(a));
            ita.setAr_cp(ar_cp.get(a));
            ita.setAr_acpc(ar_acpc.get(a));
            ita.setAr_ac(ar_ac.get(a));
            ita.setAr_pc(ar_pc.get(a));
            itemList.add(ita);
        }
    }

}
