package com.nexsoft.firman.nexcloud.util;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;

import com.nexsoft.firman.nexcloud.ErrorActivity;
import com.nexsoft.firman.nexcloud.R;

/**
 * Created by PATRA on 18-May-17.
 */

public class ExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {

    private final Activity myContext;
    private final String LINE_SEPARATOR = "\n";
    private MySession sesi;
    private String  sesiLoginUsername="",sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="", sesiLoginListDistLength="", sesiLoginUserLevel="", sesiLoginLastDate="";

    public ExceptionHandler(Activity context) {
        myContext = context;
    }

    public void uncaughtException(Thread thread, Throwable exception) {

        sesi = new MySession();

        sesiLoginUsername = sesi.getUsername(myContext);
        sesiLoginPrincipalID = sesi.getPrincipalID(myContext);
        sesiLoginListDistPetik = sesi.getListDistPetik(myContext);
        sesiLoginDistributorID = sesi.getDistributorID(myContext);
        sesiLoginUserLevel = sesi.getUserLevel(myContext);
        sesiLoginLastDate = sesi.getLastDate(myContext);
        sesiLoginListDistLength = sesi.getListDistLength(myContext);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        StringBuilder errorReport = new StringBuilder();
        errorReport.append("************ CAUSE OF ERROR ************\n\n");
        errorReport.append(stackTrace.toString());

        errorReport.append("\n************ DATE & TIME ************\n");
        errorReport.append("Datetime: "+formattedDate);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Versi: "+ myContext.getString(R.string.versi_app));
        errorReport.append(LINE_SEPARATOR);

        errorReport.append("\n************ DEVICE INFORMATION ***********\n");
        errorReport.append("Brand: ");
        errorReport.append(Build.BRAND);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Device: ");
        errorReport.append(Build.DEVICE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Model: ");
        errorReport.append(Build.MODEL);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Id: ");
        errorReport.append(Build.ID);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Product: ");
        errorReport.append(Build.PRODUCT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n************ FIRMWARE ************\n");
        errorReport.append("SDK: ");
        errorReport.append(Build.VERSION.SDK);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Release: ");
        errorReport.append(Build.VERSION.RELEASE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Incremental: ");
        errorReport.append(Build.VERSION.INCREMENTAL);
        errorReport.append(LINE_SEPARATOR);

        Intent intent = new Intent(myContext, ErrorActivity.class);
        intent.putExtra("error_nya", errorReport.toString());
        //ERROR~DateTime~Vapp~BRAND~DEVICE~MODEL~ID~PRODUCT~SDK
        intent.putExtra("error_nya2","``` "+
                stackTrace.toString()+"~"
                +formattedDate+"~"
                +myContext.getString(R.string.versi_app)+"~"
                +Build.BRAND+"~"
                +Build.DEVICE+"~"
                +Build.MODEL+"~"
                +Build.ID+"~"
                +Build.PRODUCT+"~"
                +Build.VERSION.SDK+"~"
                +Build.VERSION.RELEASE+"~"
                +sesiLoginUsername+"~"
                +sesiLoginPrincipalID+"~"
                +sesiLoginDistributorID+"~"
                + sesiLoginListDistPetik+"~"
                + sesiLoginListDistLength+"~"
                + sesiLoginUserLevel+"~"
                + sesiLoginLastDate
                +" ```"
        );
        myContext.startActivity(intent);

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

}
