package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataSPA;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by PATRA on 20-Mar-17.
 */

public class AdapterSPA extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataSPA> items;

    public AdapterSPA(Activity activity, List<DataSPA> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_salesmanperformance,null);
        }


        final DecimalFormat df = new DecimalFormat("#.##");
        final TextView noNama = (TextView) convertView.findViewById(R.id.txtSPnoNamaXml);
        final TextView txtSpAcPc1 = (TextView) convertView.findViewById(R.id.txtSpAcPc1);
        final TextView txtSpAcPc2 = (TextView) convertView.findViewById(R.id.txtSpAcPc2);
        final TextView txtSpEcPc1 = (TextView) convertView.findViewById(R.id.txtSpEcPc1);
        final TextView txtSpEcPc2 = (TextView) convertView.findViewById(R.id.txtSpEcPc2);
        final TextView txtCpSPA = (TextView)convertView.findViewById(R.id.txtCpSPA);
        final ProgressBar pgCpSPA = (ProgressBar)convertView.findViewById(R.id.pgCpSPA);
        final TextView txtCpSPEc = (TextView)convertView.findViewById(R.id.txtCpSPEc);
        final ProgressBar pgCpSPEc = (ProgressBar)convertView.findViewById(R.id.pgCpSPEc);
        final TextView sku = (TextView) convertView.findViewById(R.id.txtSpSkuXML);
        final ProgressBar pgCpSPete= (ProgressBar)convertView.findViewById(R.id.pgCpSPete);
        final TextView txtCpSPete = (TextView) convertView.findViewById(R.id.txtCpSPete);
        final TextView txtSpEte1 = (TextView)convertView.findViewById(R.id.txtSpEte1);
        final TextView txtSpEte2 = (TextView)convertView.findViewById(R.id.txtSpEte2);
        final TextView txtSpDropSize = (TextView)convertView.findViewById(R.id.txtSpDropSize);

        /*pgCpSPA.getProgressDrawable().setColorFilter(Color.YELLOW, android.graphics.PorterDuff.Mode.SRC_IN);
        pgCpSPEc.getProgressDrawable().setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
        pgCpSPete.getProgressDrawable().setColorFilter(Color.GREEN, android.graphics.PorterDuff.Mode.SRC_IN);*/

        DataSPA datax = items.get(position);

        noNama.setText(String.valueOf(datax.getAr_no())+" ~ "+String.valueOf(datax.getAr_nama()));

        Double ac = Double.valueOf(datax.getAr_ac());
        Double pc = Double.valueOf(datax.getAr_pc());
        Double ec = Double.valueOf(datax.getAr_ec());
        Double ete = Double.valueOf(datax.getAr_ete());

        Log.d("rcp1", String.valueOf(ac));
        Log.d("rcp2", String.valueOf(pc));
        Log.d("rcp3", String.valueOf(ec));
        Log.d("rcp4", String.valueOf(ete));

        double rcp_ac = 0;
        if (ac!=0 && pc!=0){
            rcp_ac = (ac*100.0)/pc;
            Log.d("rcp_ac", String.valueOf(rcp_ac));
        }
        setColornya(pgCpSPA,rcp_ac,txtCpSPA);
        pgCpSPA.setProgress(Integer.parseInt(String.valueOf(rcp_ac).split("\\.")[0]));
        if ((rcp_ac == Math.floor(rcp_ac)) && !Double.isInfinite(rcp_ac)) {
            txtCpSPA.setText(df.format(rcp_ac) + ".00%");
        }else {
            txtCpSPA.setText(df.format(rcp_ac) + "%");
        }
        txtSpAcPc1.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(ac)))));
        txtSpAcPc2.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(pc)))));

        double rcp_ec = 0;
        if (ec!=0 && pc!=0){
            rcp_ec = (ec*100.0)/pc;
            Log.d("rcp_ec", String.valueOf(rcp_ec));
        }
        setColornya(pgCpSPEc,rcp_ec,txtCpSPEc);
        pgCpSPEc.setProgress(Integer.parseInt(String.valueOf(rcp_ec).split("\\.")[0]));
        if ((rcp_ec == Math.floor(rcp_ec)) && !Double.isInfinite(rcp_ec)) {
            txtCpSPEc.setText(df.format(rcp_ec) + ".00%");
        }else {
            txtCpSPEc.setText(df.format(rcp_ec) + "%");
        }
        txtSpEcPc1.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(ec)))));
        txtSpEcPc2.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(pc)))));

        double rcp_ete = 0;
        if (ete!=0 && ec!=0){
            rcp_ete = (ete*100.0)/ec;
            Log.d("rcp_ete", String.valueOf(rcp_ete));
        }
        setColornya(pgCpSPete,rcp_ete,txtCpSPete);
        pgCpSPete.setProgress(Integer.parseInt(String.valueOf(rcp_ete).split("\\.")[0]));
        if ((rcp_ete == Math.floor(rcp_ete)) && !Double.isInfinite(rcp_ete)) {
            txtCpSPete.setText(df.format(rcp_ete) + ".00%");
        }else {
            txtCpSPete.setText(df.format(rcp_ete) + "%");
        }
        txtSpEte1.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(Math.round(ete)));
        txtSpEte2.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(Math.round(ec)));

        sku.setText(datax.getAr_sku());

        txtSpDropSize.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(Math.round(Double.parseDouble(datax.getAr_ds()))));

        return convertView;
    }

    private void setColornya(ProgressBar progressBar, double val, TextView txt) {
        if((int)Math.ceil(val)<40){
            //progressBar.getProgressDrawable().setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorRed1), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if((int)Math.ceil(val)>=40 && (int)Math.ceil(val)<=80){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorYellow), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if((int)Math.ceil(val)>80){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorGreen), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }
}
