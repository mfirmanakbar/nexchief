package com.nexsoft.firman.nexcloud;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nexsoft.firman.nexcloud.adapter.AdapterNSA;
import com.nexsoft.firman.nexcloud.data.DataNSA;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudTgl;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MySession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static java.lang.Double.parseDouble;

public class NoSyncActivity extends AppCompatActivity {

    private TextView txtNSADateFrom, txtNSADateTo, txtNSAPesan, txtNSARows;
    private EditText txtRangeANsa, txtRangeBNsa;
    private int mYear, mMonth, mDay, pk_yg_diklik;
    private ProgressDialog dialogprogress = null;
    private String TAG = "firman_NSA", dataTxt="", sesiLoginUsername="",sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="";
    MySession sesi;

    private ListView list;
    private AdapterNSA adapter;
    private List<DataNSA> itemList = new ArrayList<DataNSA>();

    private ArrayList<String> ar_dist_code = new ArrayList<String>();
    private ArrayList<String> ar_dist_name = new ArrayList<String>();
    private ArrayList<String> ar_city = new ArrayList<String>();
    private ArrayList<String> ar_asm_name = new ArrayList<String>();
    private ArrayList<String> ar_salesman_name = new ArrayList<String>();
    private ArrayList<String> ar_date = new ArrayList<String>();

    String sortDataBy = "";
    private String finalIPnya = "";

    private final int id_local_pk=6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_sync);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesi = new MySession();

        sesiLoginUsername = sesi.getUsername(getApplicationContext());
        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginListDistPetik = sesi.getListDistPetik(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());

        Log.d("SESINYA", sesiLoginUsername+" # "+sesiLoginPrincipalID+" # "+sesiLoginDistributorID +" # "+ sesiLoginListDistPetik);

        list    = (ListView) findViewById(R.id.listViewNSA);

        dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);

        txtNSAPesan = (TextView)findViewById(R.id.txtNSAPesanXML);
        txtNSADateFrom = (TextView)findViewById(R.id.txtNSADateFromXML);
        txtNSADateTo = (TextView)findViewById(R.id.txtNSADateToXML);
        txtNSARows = (TextView)findViewById(R.id.txtNSARows);

        adapter = new AdapterNSA(NoSyncActivity.this, itemList);
        list.setAdapter(adapter);

        setDateNow();
        LoadDataFromServer();

    }

    private void setDateNow() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = sdf.format(date);
        TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
        txtNSADateFrom.setText(pos.getNowTglB());
        txtNSADateTo.setText(dateString);
    }

    public void onClickDateFromNSA(View view){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear+1;
                        int tgl = dayOfMonth;
                        String xBulan = "";
                        String xTgl = "";
                        if (bulan<10){
                            xBulan = "0"+String.valueOf(bulan);
                        }else{
                            xBulan = String.valueOf(bulan);
                        }
                        if (tgl<10){
                            xTgl = "0"+String.valueOf(tgl);
                        }else{
                            xTgl = String.valueOf(tgl);
                        }
                        txtNSADateFrom.setText(year+"-"+xBulan+"-"+xTgl);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    public void onClickDateToNSA(View view){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear+1;
                        int tgl = dayOfMonth;
                        String xBulan = "";
                        String xTgl = "";
                        if (bulan<10){
                            xBulan = "0"+String.valueOf(bulan);
                        }else{
                            xBulan = String.valueOf(bulan);
                        }
                        if (tgl<10){
                            xTgl = "0"+String.valueOf(tgl);
                        }else{
                            xTgl = String.valueOf(tgl);
                        }
                        txtNSADateTo.setText(year+"-"+xBulan+"-"+xTgl);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    private void LoadDataFromServer() {
        dialogprogress.show();
        if (!IsConnected.CheckInternet()==true){
            dialogprogress.dismiss();
            if (txtNSAPesan.getVisibility()==View.GONE) {
                txtNSAPesan.setVisibility(View.VISIBLE);
            }
            txtNSAPesan.setText(getString(R.string.koneksi_internet_gagal));
            LoadDataFromLocal(id_local_pk);
        }else {

            TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
            pos.updateTbNexcloudTgl(txtNSADateFrom.getText().toString(),"",2);

            if (txtNSAPesan.getVisibility()==View.VISIBLE) {
                txtNSAPesan.setVisibility(View.GONE);
            }
            String postDateFrom = txtNSADateFrom.getText().toString();
            String postDateTo = txtNSADateTo.getText().toString();

            String urlnya = "http://"+finalIPnya+"/login/NexCloudAPI?MenuApp=MenuNSA&txtdatefrom="+postDateFrom+
                    "&txtdatethru="+postDateTo+
                    "&getPrincipalID="+sesiLoginPrincipalID+
                    "&getListDistributorID="+sesiLoginDistributorID;

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                String content = res.getString("content");
                                Log.d(TAG, content);
                                Log.d("POS_SSA", content);
                                if (content.equals("")){
                                    txtNSARows.setText("Sorry, No Data Available!");
                                    dialogprogress.dismiss()    ;
                                }else {
                                    updateDataLocal(txtNSADateFrom.getText().toString() + "^" + txtNSADateTo.getText().toString(), content, id_local_pk);
                                    LoadDataFromLocal(id_local_pk);
                                }
                            } catch (JSONException e) {}
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        dialogprogress.dismiss();
                        LoadDataFromLocal(id_local_pk);
                        if (txtNSAPesan.getVisibility()==View.GONE) {
                            txtNSAPesan.setVisibility(View.VISIBLE);
                        }
                        txtNSAPesan.setText(getString(R.string.koneksi_server_gagal));
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }

    private void updateDataLocal(String tgl, String all_result, int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        db.updateTbNexcloud(tgl, all_result, idlocal);
        db.close();
    }


    private void LoadDataFromLocal(int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        Cursor cur = db.getTbNexcloudByID(idlocal);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            Log.d(TAG, "DB_Local_"+ cur.getString(0) +"_"+ cur.getString(1)+"_"+cur.getString(2)+"_"+cur.getString(3));
                            LoadToSetData(cur.getString(3),cur.getString(1));
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        db.close();
    }

    private void LoadToSetData(String valData, String tgl) {
        if (!tgl.equals("")) {
            String[] valDataInduk = tgl.toString().trim().split("\\^");
            txtNSADateFrom.setText(valDataInduk[0]);
            txtNSADateTo.setText(valDataInduk[1]);
        }
        if (!valData.equals("")) {
            dataTxt = valData;
            simpanKeArray();
            lihatDataArray();
            setKeListView();
        }
    }

    private void simpanKeArray() {
        //kosongin array
        ar_dist_code.clear();
        ar_dist_name.clear();
        ar_city.clear();
        ar_asm_name.clear();
        ar_salesman_name.clear();
        ar_date.clear();

        //Simpan PK
        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        txtNSARows.setText("Showing 1 to "+ NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(itemInduk.length))) +" of "+ NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(itemInduk.length))) +" entries");
        Log.d("dataTxtNSA", dataTxt);
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_dist_code.add(itemAnak[0]);
            ar_dist_name.add(itemAnak[1]);
            ar_city.add(itemAnak[2]);
            ar_asm_name.add(itemAnak[3]);
            ar_salesman_name.add(itemAnak[4]);
            ar_date.add(itemAnak[5]);
        }

        //lihat
        lihatDataArray();
    }

    private void lihatDataArray() {
        for (int i=0; i<ar_dist_code.size(); i++){
            Log.d(TAG, "Lihat NSA " +
                    String.valueOf(ar_dist_code.get(i)) + "_"+
                    String.valueOf(ar_dist_name.get(i)) + "_"+
                    String.valueOf(ar_city.get(i)) + "_"+
                    String.valueOf(ar_asm_name.get(i)) + "_"+
                    String.valueOf(ar_salesman_name.get(i)) + "_"+
                    String.valueOf(ar_date.get(i))
            );
        }
    }

    private void setKeListView() {
        adapter.items_nsa.clear();
        for (int a=0; a<ar_dist_code.size(); a++){
            DataNSA ita = new DataNSA();
            ita.setAr_dist_code(ar_dist_code.get(a));
            ita.setAr_dist_name(ar_dist_name.get(a));
            ita.setAr_city(ar_city.get(a));
            ita.setAr_asm_name(ar_asm_name.get(a));
            ita.setAr_salesman_name(ar_salesman_name.get(a));
            ita.setAr_date(ar_date.get(a));
            itemList.add(ita);
            adapter.notifyDataSetChanged();
        }
        dialogprogress.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main2, menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_sync:
                LoadDataFromServer();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortData() {
        //sortDataBy
        CharSequence colors[] = new CharSequence[] {"Distributor Code", "Distributor Name", "City", "ASM Name", "Salesman Name", "Date"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sort By Asc");
        builder.setItems(colors, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                Toast.makeText(getApplicationContext(),String.valueOf(which),Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

}
