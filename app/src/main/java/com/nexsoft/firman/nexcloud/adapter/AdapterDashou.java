package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDashnod;
import com.nexsoft.firman.nexcloud.data.DataDashou;
import com.nexsoft.firman.nexcloud.util.MyRainbow;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDashou extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDashou> items_dashou;
    private final MyRainbow mrb = new MyRainbow();

    public AdapterDashou(Activity activity, List<DataDashou> items) {
        this.activity = activity;
        this.items_dashou = items;
    }

    @Override
    public int getCount() {
        return items_dashou.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dashou.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dashou, null);
        }

        final TextView txtDashouAr1 = (TextView) convertView.findViewById(R.id.txtDashouAr1);
        final TextView txtDashouAr2 = (TextView) convertView.findViewById(R.id.txtDashouAr2);
        final TextView txtDashouAr3 = (TextView) convertView.findViewById(R.id.txtDashouAr3);
        final TextView txtDashouAr4 = (TextView) convertView.findViewById(R.id.txtDashouAr4);
        final TextView txtDashouAr5 = (TextView) convertView.findViewById(R.id.txtDashouAr5);
        final TextView txtDashouAr6 = (TextView) convertView.findViewById(R.id.txtDashouAr6);
        final TextView txtDashouAr7 = (TextView) convertView.findViewById(R.id.txtDashouAr7);
        final LinearLayout layout_dashou = (LinearLayout) convertView.findViewById(R.id.layout_dashou);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dashou);

        DataDashou datax = items_dashou.get(position);
        txtDashouAr1.setText(datax.getAr_1());
        txtDashouAr2.setText(datax.getAr_2());
        txtDashouAr3.setText(datax.getAr_3());
        txtDashouAr4.setText(datax.getAr_4());
        txtDashouAr5.setText(datax.getAr_5());
        txtDashouAr6.setText(datax.getAr_6());
        mrb.setColor(activity,Double.parseDouble(datax.getAr_7()),txtDashouAr7);
        txtDashouAr7.setText(datax.getAr_7()+"%");

        return convertView;
    }
}
