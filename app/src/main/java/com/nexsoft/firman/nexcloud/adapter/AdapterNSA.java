package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataNSA;

import java.util.List;

/**
 * Created by PATRA on 10-Apr-17.
 */

public class AdapterNSA extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    public List<DataNSA> items_nsa;

    public AdapterNSA(Activity activity, List<DataNSA> items_aca) {
        this.activity = activity;
        this.items_nsa = items_aca;
    }

    @Override
    public int getCount() {
        return items_nsa.size();
    }

    @Override
    public Object getItem(int position) {
        return items_nsa.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_nsa,null);
        }

        final TextView txtNsaDistCode = (TextView) convertView.findViewById(R.id.txtNsaDistCode);
        final TextView txtNsaTgl = (TextView) convertView.findViewById(R.id.txtNsaTgl);
        final TextView txtNsaDistName = (TextView) convertView.findViewById(R.id.txtNsaDistName);
        final TextView txtNsaAsmName = (TextView) convertView.findViewById(R.id.txtNsaAsmName);
        final TextView txtNsaCity = (TextView) convertView.findViewById(R.id.txtNsaCity);
        final TextView txtNsaSalesName = (TextView) convertView.findViewById(R.id.txtNsaSalesName);

        DataNSA datax = items_nsa.get(position);

        if (datax.getAr_dist_code().length()>0){
            txtNsaDistCode.setText(datax.getAr_dist_code());
        }else {
            txtNsaDistCode.setText("-");
        }

        if (datax.getAr_date().length()>0){
            txtNsaTgl.setText(datax.getAr_date());
        }else {
            txtNsaTgl.setText("-");
        }

        if (datax.getAr_dist_name().length()>0){
            txtNsaDistName.setText(datax.getAr_dist_name());
        }else {
            txtNsaDistName.setText("-");
        }

        if (datax.getAr_asm_name().length()>0){
            txtNsaAsmName.setText(datax.getAr_asm_name());
        }else {
            txtNsaAsmName.setText("-");
        }

        if (datax.getAr_city().length()>0){
            txtNsaCity.setText(datax.getAr_city());
        }else {
            txtNsaCity.setText("-");
        }

        if (datax.getAr_salesman_name().length()>0){
            txtNsaSalesName.setText(datax.getAr_salesman_name());
        }else {
            txtNsaSalesName.setText("-");
        }

        return convertView;
    }
}
