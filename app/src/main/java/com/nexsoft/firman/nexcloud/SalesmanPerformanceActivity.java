package com.nexsoft.firman.nexcloud;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nexsoft.firman.nexcloud.adapter.AdapterSPA;
import com.nexsoft.firman.nexcloud.data.DataSPA;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudTgl;
import com.nexsoft.firman.nexcloud.navigation.NavigationCustom;
import com.nexsoft.firman.nexcloud.navigation.NavigationDrawerFragment;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MySession;
import com.nexsoft.firman.nexcloud.util.MyTextFormat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by PATRA on 05-May-17.
 */

public class SalesmanPerformanceActivity extends AppCompatActivity {

    private int mYear, mMonth, mDay, pk_yg_diklik, lokasi_yg_diklik = 0;
    private TextView txtSPDateFrom, txtSPDateThru, txtSPPesan, txtInfoHistoriesF;
    //private ProgressDialog dialogprogress = null;
    private String TAG = "firman_SPA", dataTxt = "", txtInfoLokasi = "", posisi_namaNow = "Home", sesiLoginUsername = "", sesiLoginPrincipalID = "", sesiLoginDistributorID = "", sesiLoginListDistPetik = "", sesiLoginUserLevel = "";
    private MySession sesi;
    private ArrayList<Integer> ar_pk = new ArrayList<Integer>();
    private ArrayList<Integer> ar_no = new ArrayList<Integer>();
    private ArrayList<Integer> ar_lokasi = new ArrayList<Integer>();
    private ArrayList<String> ar_nama = new ArrayList<String>();
    private ArrayList<String> ar_pc = new ArrayList<String>();
    private ArrayList<String> ar_ac = new ArrayList<String>();
    private ArrayList<String> ar_ec = new ArrayList<String>();
    private ArrayList<String> ar_sku = new ArrayList<String>();
    private ArrayList<String> ar_ete = new ArrayList<String>();
    private ArrayList<String> ar_ds = new ArrayList<String>();
    private ListView list;
    private AdapterSPA adapter;
    private List<DataSPA> itemList = new ArrayList<DataSPA>();
    private Boolean status_awal = true;
    private String finalIPnya = "";
    private MaterialSpinner spinnerSpLevel;
    private ArrayList<String> ar_spinnerSpLevel = new ArrayList<String>();
    private int levelListnya = 0;
    private final int id_local_pk=2;
    private Handler handler = new Handler();
    private int hitTime=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesman_performance);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesi = new MySession();

        sesiLoginUsername = sesi.getUsername(getApplicationContext());
        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginListDistPetik = sesi.getListDistPetik(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());
        sesiLoginUserLevel = sesi.getUserLevel(getApplicationContext());

        spinnerSpLevel = (MaterialSpinner) findViewById(R.id.spinnerSPLevel);
        txtSPDateFrom = (TextView) findViewById(R.id.txtSPFromXML);
        txtSPDateThru = (TextView) findViewById(R.id.txtSPToXML);
        txtSPPesan = (TextView) findViewById(R.id.txtSPPesanXML);
        txtSPPesan.setText("");
        list = (ListView) findViewById(R.id.listViewSPA);
        txtInfoHistoriesF = (TextView) findViewById(R.id.infoHistoriesSPXML);

        /*dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);*/

        adapter = new AdapterSPA(SalesmanPerformanceActivity.this, itemList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pk_yg_diklik = itemList.get(position).getAr_pk();
                Log.d("pk_yg_diklik", String.valueOf(pk_yg_diklik));
                lokasi_yg_diklik = itemList.get(position).getAr_lokasi();
                setHistoryUp(position);
                loadClickData();
            }
        });

        setSpinner();
        spinnerSpLevel.setItems(ar_spinnerSpLevel);
        setDateNow();

        spinnerSpLevel.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                //Snackbar.make(view, item, Snackbar.LENGTH_LONG).show();
                String[] split1 = item.toString().trim().split("\\:");
                String[] split2 = split1[1].toString().trim().split("\\-");
                levelListnya = Integer.parseInt(split2[0].trim());
                //Toast.makeText(getApplicationContext(),String.valueOf(levelListnya)+"_"+split2[0],Toast.LENGTH_SHORT).show();
            }
        });

        txtInfoLokasi = lokasi_yg_diklik + "";

        /*hitTime=0;
        handler.postDelayed(runnable, MyConfig.delayToLoad);*/
        LoadDataFromServer();
    }

    /*private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hitTime+=1;
            if (hitTime<2) {
                handler.postDelayed(this, MyConfig.delayToLoad);
            }else {
                handler.removeCallbacks(runnable);
                LoadDataFromServer();
            }
            Log.e("ikan"+TAG,String.valueOf(hitTime));
        }
    };*/

    private void loadingStart() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.VISIBLE);
    }

    private void loadingStop() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.GONE);
    }

    private void setSpinner() {
        int usrLvl = Integer.parseInt(sesiLoginUserLevel);
        for (int k = usrLvl; k <= 6; k++) {
            ar_spinnerSpLevel.add("Lvl: " + String.valueOf(k) + "-6");
        }
    }

    private Boolean cekDataPkLokasi() {
        Boolean bol = false;
        for (int a = 0; a < ar_pk.size(); a++) {
            if (ar_lokasi.get(a) == pk_yg_diklik) {
                bol = true;
                break;
            }
        }
        return bol;
    }

    private void loadClickData() {
        setKeListView();
    }

    private void setKeListView() {

        if (status_awal == true) {
            //muncul pertama
            adapter.items.clear();
            for (int a = 0; a < ar_pk.size(); a++) {
                if (ar_lokasi.get(a) == 0) { //if(ar_no.get(a)==levelListnya){
                    DataSPA ita = new DataSPA();
                    ita.setAr_pk(ar_pk.get(a));
                    ita.setAr_lokasi(ar_lokasi.get(a));
                    ita.setAr_no(ar_no.get(a));
                    ita.setAr_nama(ar_nama.get(a));
                    ita.setAr_pc(ar_pc.get(a));
                    ita.setAr_ac(ar_ac.get(a));
                    ita.setAr_ec(ar_ec.get(a));
                    ita.setAr_ete(ar_ete.get(a));
                    ita.setAr_sku(ar_sku.get(a));
                    ita.setAr_ds(ar_ds.get(a));
                    itemList.add(ita);
                    adapter.notifyDataSetChanged();
                }
            }
            status_awal = false;
        } else {
            if (cekDataPkLokasi() == true) {
                //muncul selanjutnya
                txtInfoLokasi = txtInfoLokasi + "^" + pk_yg_diklik;
                adapter.items.clear();
                for (int a = 0; a < ar_pk.size(); a++) {
                    Log.d("TES", a + "");
                    if (ar_lokasi.get(a) == pk_yg_diklik) {
                        Log.d("TES_TRUE_IF", ar_lokasi.get(a) + "");
                        DataSPA ita = new DataSPA();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_nama(ar_nama.get(a));
                        ita.setAr_pc(ar_pc.get(a));
                        ita.setAr_ac(ar_ac.get(a));
                        ita.setAr_ec(ar_ec.get(a));
                        ita.setAr_ete(ar_ete.get(a));
                        ita.setAr_sku(ar_sku.get(a));
                        ita.setAr_ds(ar_ds.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal = false;
            } else {
                if (txtSPPesan.getVisibility() != View.VISIBLE) {
                    txtSPPesan.setVisibility(View.VISIBLE);
                }
                txtSPPesan.setText("Maaf, tidak ada data lagi !");
            }
        }
        //dialogprogress.dismiss();
        loadingStop();
    }

    private void setHistoryUp(int position) {
        if (cekDataPkLokasi() == true) {
            txtInfoHistoriesF.setVisibility(View.VISIBLE);
            posisi_namaNow = posisi_namaNow + " > " + itemList.get(position).getAr_nama();
            txtInfoHistoriesF.setText(posisi_namaNow);
        }
    }

    private void setDateNow() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = sdf.format(date);
        TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
        txtSPDateFrom.setText(pos.getNowTglB());
        txtSPDateThru.setText(dateString);
    }

    public void onClickDateSPFrom(View view) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear + 1;
                        int tgl = dayOfMonth;
                        String xBulan = "";
                        String xTgl = "";
                        if (bulan < 10) {
                            xBulan = "0" + String.valueOf(bulan);
                        } else {
                            xBulan = String.valueOf(bulan);
                        }
                        if (tgl < 10) {
                            xTgl = "0" + String.valueOf(tgl);
                        } else {
                            xTgl = String.valueOf(tgl);
                        }
                        txtSPDateFrom.setText(year + "-" + xBulan + "-" + xTgl);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();

    }

    public void onClickDateSPThru(View view) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear + 1;
                        int tgl = dayOfMonth;
                        String xBulan = "";
                        String xTgl = "";
                        if (bulan < 10) {
                            xBulan = "0" + String.valueOf(bulan);
                        } else {
                            xBulan = String.valueOf(bulan);
                        }
                        if (tgl < 10) {
                            xTgl = "0" + String.valueOf(tgl);
                        } else {
                            xTgl = String.valueOf(tgl);
                        }
                        txtSPDateThru.setText(year + "-" + xBulan + "-" + xTgl);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    private String getLevel() {
        String[] split1 = spinnerSpLevel.getText().toString().trim().split("\\:");
        String[] split2 = split1[1].toString().trim().split("\\-");
        return split2[0].trim();
    }

    private void LoadDataFromServer() {
        loadingStart();
        levelListnya = Integer.parseInt(getLevel());
        if (txtInfoHistoriesF.getVisibility() == View.VISIBLE) {
            txtInfoHistoriesF.setVisibility(View.GONE);
        }
        //dialogprogress.show();
        if (!IsConnected.CheckInternet() == true) {
            LoadDataFromLocal(id_local_pk);
            //dialogprogress.dismiss();
            loadingStop();
            Toast.makeText(getApplicationContext(), getString(R.string.koneksi_internet_gagal), Toast.LENGTH_SHORT).show();
            /*if (txtSPPesan.getVisibility() != View.VISIBLE) {
                txtSPPesan.setVisibility(View.VISIBLE);
            }
            txtSPPesan.setText(getString(R.string.koneksi_internet_gagal));*/
        } else {

            TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
            pos.updateTbNexcloudTgl(txtSPDateFrom.getText().toString(), "", 2);

            if (txtSPPesan.getVisibility() == View.VISIBLE) {
                txtSPPesan.setVisibility(View.GONE);
            }
            String postDateFrom = txtSPDateFrom.getText().toString();
            String postDateTo = txtSPDateThru.getText().toString();
            String urlnya = "http://" + finalIPnya + "/login/NexCloudAPI?MenuApp=MenuSPA&txtdatefrom=" + postDateFrom +
                    "&txtdatethru=" + postDateTo +
                    "&getPrincipalID=" + sesiLoginPrincipalID +
                    "&getListDistributorID=" + sesiLoginDistributorID +
                    "&userLevel=" + sesiLoginUserLevel +
                    "&txttoplevel=" + levelListnya;

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                String content = res.getString("content");
                                String message = res.getString("message");
                                boolean success = res.getBoolean("success");
                                if (success == true) {
                                    if (content.equals("")) {
                                        //dialogprogress.dismiss();
                                        loadingStop();
                                        //finish();
                                        Toast.makeText(getApplicationContext(), "Tidak ada Data!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Log.d("SPAcontent", content);
                                        updateDataLocal(txtSPDateFrom.getText().toString() + "^" + txtSPDateThru.getText().toString(), content, id_local_pk);
                                        LoadDataFromLocal(id_local_pk);
                                    }
                                } else {
                                    finish();
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {}
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        //dialogprogress.dismiss();
                        loadingStop();
                        LoadDataFromLocal(id_local_pk);
                        if (txtSPPesan.getVisibility() != View.VISIBLE) {
                            txtSPPesan.setVisibility(View.VISIBLE);
                        }
                        txtSPPesan.setText(getString(R.string.koneksi_server_gagal));
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }

    private void updateDataLocal(String tgl, String all_result, int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        db.updateTbNexcloud(tgl, all_result, idlocal);
        db.close();
    }

    private void LoadDataFromLocal(int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        Cursor cur = db.getTbNexcloudByID(idlocal);
        if (cur.getCount() != 0) {
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            Log.d(TAG, "DB_Local_" + cur.getString(0) + "_" + cur.getString(1) + "_" + cur.getString(2) + "_" + cur.getString(3));
                            LoadToSetData(cur.getString(3), cur.getString(1));
                        } while (cur.moveToNext());
                    }
                }
            }
        } else {
            Log.d("firman_test", "SQLite Data -> NULL");
        }
        db.close();
    }

    private void LoadToSetData(String valData, String tgl) {
        if (!tgl.equals("")) {
            String[] valDataInduk = tgl.toString().trim().split("\\^");
            txtSPDateFrom.setText(valDataInduk[0]);
            txtSPDateThru.setText(valDataInduk[1]);
        }
        if (!valData.equals("")) {
            Log.d(TAG, valData + " & " + tgl);
            dataTxt = valData;
            simpanKeArray();
            lihatDataArray();
            loadClickData();
        } else {
            Toast.makeText(getApplicationContext(), "Tidak ada data!", Toast.LENGTH_SHORT).show();
        }
        //dialogprogress.dismiss();
        loadingStop();
    }

    private void simpanKeArray() {
        setArrayNull();

        MyTextFormat mtf = new MyTextFormat();
        String hasilDs="0";
        //Simpan PK
        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        for (int a = 0; a < itemInduk.length; a++) {
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_pk.add(a + 1);//+1
            ar_no.add(Integer.valueOf(itemAnak[0]));
            ar_lokasi.add(99999);
            ar_nama.add(itemAnak[1]);
            ar_pc.add(itemAnak[2]);
            ar_ac.add(itemAnak[3]);
            ar_ec.add(itemAnak[4]);
            ar_ete.add(itemAnak[6]);
            ar_sku.add(itemAnak[5]);
            if (Double.parseDouble(itemAnak[6])>0) {
                hasilDs = String.valueOf(Double.parseDouble(itemAnak[8])/Double.parseDouble(itemAnak[6]));
            }else{
                hasilDs="0";
            }
            ar_ds.add(hasilDs);
        }
        //update Lokasi
        if (levelListnya == 0) {
            for (int i = 0; i < ar_pk.size(); i++) {
                if (ar_no.get(i) == 0) {
                    ar_lokasi.set(i, 0);
                } else if (ar_no.get(i) == 1) {
                    ar_lokasi.set(i, 1);
                } else if (ar_no.get(i) > 1) {
                    for (int b = i; b < ar_pk.size(); b++) {
                        if (ar_no.get(b) == ar_no.get(i) && ar_no.get(b) > ar_no.get(b - 1)) {
                            ar_lokasi.set(b, ar_pk.get(i - 1));
                        } else if (ar_no.get(b) > ar_no.get(i)) {
                            ar_lokasi.set(b, ar_pk.get(i));
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < ar_pk.size(); i++) {
                if (i == 0 || ar_no.get(i) == levelListnya) {
                    ar_lokasi.set(i, 0);
                } else {
                    if (ar_no.get(i) > ar_no.get(i - 1)) {
                        ar_lokasi.set(i, ar_pk.get(i - 1));
                    } else if (ar_no.get(i) == ar_no.get(i - 1)) {
                        ar_lokasi.set(i, ar_lokasi.get(i - 1));
                    } else if (ar_no.get(i) < ar_no.get(i - 1)) { //no skrg < no sebelumnya
                        int popo = cekLokasiOFnumber(ar_no.get(i), i);
                        ar_lokasi.set(i, popo);
                    }
                }
            }
        }

    }

    private int cekLokasiOFnumber(int val, int i) {
        int hasil = 0;
        for (int x = i - 1; x >= 0; x--) {
            if (ar_no.get(x) == val) {
                hasil = ar_lokasi.get(x);
                break;
            }
        }
        return hasil;
    }

    private void lihatDataArray() {
        for (int i = 0; i < ar_no.size(); i++) {
            Log.d(TAG, "Lihat Array " +
                    String.valueOf(ar_pk.get(i)) + "_" +
                    String.valueOf(ar_lokasi.get(i)) + "_" +
                    String.valueOf(ar_no.get(i)) + "_" +
                    String.valueOf(ar_nama.get(i)) + "_" +
                    String.valueOf(ar_pc.get(i)) + "_" +
                    String.valueOf(ar_ac.get(i)) + "_" +
                    String.valueOf(ar_ec.get(i)) + "_" +
                    String.valueOf(ar_ete.get(i)) + "_" +
                    String.valueOf(ar_sku.get(i)) + "_" +
                    String.valueOf(ar_ds.get(i))

            );
        }
    }

    private void kembaliKeHeader() {
        String lokasiUriLast = "NULL";
        String lokasiUri = txtInfoLokasi;
        String lastLocationToTxt = "";
        Log.d("KKH_lokasiUri", lokasiUri);
        String[] lokasiUriCut = lokasiUri.toString().trim().split("\\^");
        for (int a = 0; a < lokasiUriCut.length; a++) {
            if (a == lokasiUriCut.length - 2) {
                lokasiUriLast = String.valueOf(lokasiUriCut[a]);
            }
            if (a < lokasiUriCut.length - 1) {
                if (a == lokasiUriCut.length - 2) {
                    lastLocationToTxt += lokasiUriCut[a];
                } else {
                    lastLocationToTxt += lokasiUriCut[a] + "^";
                }
            }
        }
        Log.d("KKH_lastLocationToTxt", lastLocationToTxt);
        txtInfoLokasi = lastLocationToTxt;//.setText(lastLocationToTxt);
        Log.d("KKH_lokasiUriLast", lokasiUriLast);
        if (!lokasiUriLast.equals("NULL")) {
            Log.d("KKH_lokasiUriLast", "OKEH_SHOW_LOKASI_" + lokasiUriLast);
            if (lokasiUriLast.equals("")) {
                lokasiUriLast = "0";
            }
            if (Integer.parseInt(lokasiUriLast) > 0) {
                //munculin data WHERE lokasi_di_array == PK_nya
                Log.d("KKH_JADI", "munculin data WHERE lokasi_di_array == PK_nya " + lokasiUriLast);
                adapter.items.clear();
                for (int a = 0; a < ar_pk.size(); a++) {
                    Log.d("KKH_JADI_FOR", "" + ar_lokasi.get(a));
                    if (String.valueOf(ar_lokasi.get(a)).equals(lokasiUriLast)) {
                        Log.d("KKH_JADI_FOR_", "" + ar_lokasi.get(a));
                        DataSPA ita = new DataSPA();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_nama(ar_nama.get(a));
                        ita.setAr_pc(ar_pc.get(a));
                        ita.setAr_ac(ar_ac.get(a));
                        ita.setAr_ec(ar_ec.get(a));
                        ita.setAr_ete(ar_ete.get(a));
                        ita.setAr_sku(ar_sku.get(a));
                        ita.setAr_ds(ar_ds.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal = false;
                //SET HISTORY BACK
                setHistoryDown();
                //update_text
            } else {
                //munculin data WHERE no_di_array == 0
                Log.d("KKH_JADI", "munculin data WHERE no_di_array == 0");
                /*if (levelListnya>0){
                    txtInfoLokasi = String.valueOf(levelListnya);
                }else{
                    txtInfoLokasi = "0";
                }*/
                txtInfoLokasi = "0";
                posisi_namaNow = "Home";
                txtInfoHistoriesF.setVisibility(View.GONE);
                //muncul pertama
                adapter.items.clear();
                for (int a = 0; a < ar_pk.size(); a++) {
                    if (ar_no.get(a) == levelListnya) {
                        DataSPA ita = new DataSPA();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_nama(ar_nama.get(a));
                        ita.setAr_pc(ar_pc.get(a));
                        ita.setAr_ac(ar_ac.get(a));
                        ita.setAr_ec(ar_ec.get(a));
                        ita.setAr_ete(ar_ete.get(a));
                        ita.setAr_sku(ar_sku.get(a));
                        ita.setAr_ds(ar_ds.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal = false;
            }
        }
    }

    private void setHistoryDown() {
        String posisi_endNama = "";
        String[] hd = posisi_namaNow.toString().trim().split("\\ > ");
        for (int a = 1; a < hd.length - 1; a++) {
            Log.d("HDHD1", hd[a]);
            posisi_endNama += " > " + hd[a];
        }
        Log.d("HDHD2", posisi_endNama);
        posisi_namaNow = "Home " + posisi_endNama;
        txtInfoHistoriesF.setText(posisi_namaNow);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5 && keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            //Log.d("CDA", "onKeyDown Called");
            if (txtSPPesan.getVisibility() == View.VISIBLE) {
                txtSPPesan.setVisibility(View.GONE);
            }
            if (!txtInfoLokasi.equals("0") && txtInfoHistoriesF.getVisibility() == View.VISIBLE) {
                kembaliKeHeader();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sync:
                status_awal = true;
                txtInfoHistoriesF.setText("");
                txtInfoLokasi = "";
                posisi_namaNow = "Home";
                setArrayNull();
                LoadDataFromServer();
                return true;
            case android.R.id.home:
                finish();
                return true;
            /*case R.id.action_sr:
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation== Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

    private void setArrayNull() {
        ar_pk.clear();
        ar_lokasi.clear();
        ar_no.clear();
        ar_nama.clear();
        ar_pc.clear();
        ar_ac.clear();
        ar_ec.clear();
        ar_ete.clear();
        ar_sku.clear();
        ar_ds.clear();
    }
}
