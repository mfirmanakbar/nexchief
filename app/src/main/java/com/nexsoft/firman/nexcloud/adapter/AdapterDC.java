package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDC;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by firmanmac on 5/2/17.
 */

public class AdapterDC extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDC> items_dc;

    public AdapterDC(Activity activity, List<DataDC> items) {
        this.activity = activity;
        this.items_dc = items;
    }

    @Override
    public int getCount() {
        return items_dc.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dc.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dc,null);
        }

        final DecimalFormat df = new DecimalFormat("#.##");

        final TextView txtDCnoNama = (TextView)convertView.findViewById(R.id.txtDCnoNama);

        final TextView txtChartRc = (TextView)convertView.findViewById(R.id.txtChartRc);
        final ProgressBar pgChartRc = (ProgressBar)convertView.findViewById(R.id.pgChartRc);
        final TextView txtRcAc = (TextView)convertView.findViewById(R.id.txtRcAc);
        final TextView txtRcPl = (TextView)convertView.findViewById(R.id.txtRcPl);

        final TextView txtChartEcr = (TextView)convertView.findViewById(R.id.txtChartEcr);
        final ProgressBar pgChartEcr = (ProgressBar)convertView.findViewById(R.id.pgChartEcr);
        final TextView txtEcrEc = (TextView)convertView.findViewById(R.id.txtEcrEc);
        final TextView txtEcrPc = (TextView)convertView.findViewById(R.id.txtEcrPc);

        final TextView txtSkuDC = (TextView)convertView.findViewById(R.id.txtSkuDC);

        final TextView txtChartSod = (TextView)convertView.findViewById(R.id.txtChartSod);
        final ProgressBar pgChartSod = (ProgressBar)convertView.findViewById(R.id.pgChartSod);
        final TextView txtSodSales = (TextView)convertView.findViewById(R.id.txtSodSales);
        final TextView txtSodTarget = (TextView)convertView.findViewById(R.id.txtSodTarget);

        final TextView txtDcTO1 = (TextView)convertView.findViewById(R.id.txtDcTO1);
        final TextView txtDcTO2 = (TextView)convertView.findViewById(R.id.txtDcTO2);

        final TextView txtDcMO1 = (TextView)convertView.findViewById(R.id.txtDcMO1);
        final TextView txtDcMO2 = (TextView)convertView.findViewById(R.id.txtDcMO2);

        DataDC datax = items_dc.get(position);

        txtDCnoNama.setText(datax.getAr_1());

        Double ar2 = Double.valueOf(datax.getAr_2());
        Double ar3 = Double.valueOf(datax.getAr_3());
        Double ar4 = Double.valueOf(datax.getAr_4());
        Double ar5 = Double.valueOf(datax.getAr_5());
        Double ar6 = Double.valueOf(datax.getAr_6());
        Double ar7 = Double.valueOf(datax.getAr_7());
        Double ar8 = Double.valueOf(datax.getAr_8());
        Double ar9 = Double.valueOf(datax.getAr_9());
        Double ar10 = Double.valueOf(datax.getAr_10());
        Double ar11 = Double.valueOf(datax.getAr_11());

        double c1 = 0;
        if (ar3>0 && ar2>0){
            c1 = (ar3*100.00)/ar2;
        }
        if ((c1 == Math.floor(c1)) && !Double.isInfinite(c1)) {
            txtChartRc.setText(df.format(c1) + ".00%");
        }else {
            txtChartRc.setText(df.format(c1) + "%");
        }
        setColornya(pgChartRc,c1,txtChartRc);
        pgChartRc.setProgress(Integer.parseInt(String.valueOf(c1).split("\\.")[0]));
        txtRcAc.setText(String.valueOf(Math.round(ar3)));
        txtRcPl.setText(String.valueOf(Math.round(ar2)));

        double c2 = 0;
        if (ar4>0 && ar2>0){
            c2 = (ar4*100.00)/ar2;
        }
        if ((c2 == Math.floor(c2)) && !Double.isInfinite(c2)) {
            txtChartEcr.setText(df.format(c2) + ".00%");
        }else {
            txtChartEcr.setText(df.format(c2) + "%");
        }
        setColornya(pgChartEcr,c2,txtChartEcr);
        pgChartEcr.setProgress(Integer.parseInt(String.valueOf(c2).split("\\.")[0]));
        txtEcrEc.setText(String.valueOf(Math.round(ar4)));
        txtEcrPc.setText(String.valueOf(Math.round(ar2)));

        if(ar5>0) {
            txtSkuDC.setText(String.valueOf(ar5));
        }else {
            txtSkuDC.setText("0");
        }
        if (ar5>=4){
            txtSkuDC.setTextColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else{
            txtSkuDC.setTextColor(ContextCompat.getColor(activity,R.color.colorGrey4));
        }

        double c3 = 0;
        if (ar10>0 && ar11>0){
            c3 = (ar10*100.00)/ar11;
        }
        if ((c3 == Math.floor(c3)) && !Double.isInfinite(c3)) {
            txtChartSod.setText(df.format(c3) + ".00%");
        }else {
            txtChartSod.setText(df.format(c3) + "%");
        }
        setColornya(pgChartSod,c3,txtChartSod);
        pgChartSod.setProgress(Integer.parseInt(String.valueOf(c3).split("\\.")[0]));
        if (ar10>1000.00){
            ar10/=1000.00;
        }
        if (ar11>1000.00){
            ar11/=1000.00;
        }
        txtSodSales.setText(NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(ar10)))));
        txtSodTarget.setText(NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(ar11)))));

        txtDcTO1.setText("#Pv-Pro: "+String.valueOf(Math.round(ar6)));
        txtDcTO2.setText("#Registered: "+String.valueOf(Math.round(ar7)));

        txtDcMO1.setText("#Pv-Pro: "+String.valueOf(Math.round(ar8)));
        txtDcMO2.setText("#Registered: "+String.valueOf(Math.round(ar9)));

        return convertView;
    }

    private void setColornya(ProgressBar progressBar, double val, TextView txt) {
        if((int)Math.ceil(val)<40){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorRed1), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if((int)Math.ceil(val)>=40 && (int)Math.ceil(val)<=80){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorYellow), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if((int)Math.ceil(val)>80){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorGreen), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }
}
