package com.nexsoft.firman.nexcloud;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MySession;

import java.util.regex.Pattern;

public class LastlogActivity extends AppCompatActivity {

    private TextView txtLogOutPlease, txtLogDate, txtLogUserId, txtLogPrincipleId, txtLogDistributorId, txtLogDistributorTotal, txtLogUserLevel, txtVn;
    private MySession sesi;
    private String sesiLoginUsername="",sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="", sesiLoginLastDate="", sesiLoginUserLevel="";
    private String finalIPnya = "";

    //komit

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lastlog);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesi = new MySession();

        sesiLoginUsername = sesi.getUsername(getApplicationContext());
        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginListDistPetik = sesi.getListDistPetik(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());
        sesiLoginLastDate = sesi.getLastDate(getApplicationContext());
        sesiLoginUserLevel = sesi.getUserLevel(getApplicationContext());

        txtLogOutPlease = (TextView)findViewById(R.id.txtLogOutPlease);
        txtLogDate = (TextView)findViewById(R.id.txtLogDate);
        txtLogUserId = (TextView)findViewById(R.id.txtLogUserId);
        txtLogPrincipleId = (TextView)findViewById(R.id.txtLogPrincipleId);
        txtLogDistributorId = (TextView)findViewById(R.id.txtLogDistributorId);
        txtLogDistributorTotal = (TextView)findViewById(R.id.txtLogDistributorTotal);
        txtLogUserLevel = (TextView)findViewById(R.id.txtLogUserLevel);
        txtVn = (TextView)findViewById(R.id.txtVn);

        String[] xDistributor = sesiLoginDistributorID.split(Pattern.quote(","));

        txtLogDate.setText(sesiLoginLastDate);
        txtLogUserId.setText(sesiLoginUsername);
        txtLogPrincipleId.setText(sesiLoginPrincipalID);
        txtLogDistributorId.setText(sesiLoginDistributorID);
        txtLogDistributorTotal.setText(String.valueOf(xDistributor.length));
        txtLogUserLevel.setText(sesiLoginUserLevel);
        txtVn.setText(BuildConfig.VERSION_NAME);

        txtLogOutPlease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOutServer();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logOutServer() {}

}
