package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDasheff;
import com.nexsoft.firman.nexcloud.util.MyRainbow;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDasheff extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDasheff> items_dasheff;
    private final MyRainbow mrb = new MyRainbow();

    public AdapterDasheff(Activity activity, List<DataDasheff> items) {
        this.activity = activity;
        this.items_dasheff = items;
    }

    @Override
    public int getCount() {
        return items_dasheff.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dasheff.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dasheff, null);
        }

        final TextView txtDasheffAr1 = (TextView) convertView.findViewById(R.id.txtDasheffAr1);
        final TextView txtDasheffAr2 = (TextView) convertView.findViewById(R.id.txtDasheffAr2);
        final TextView txtDasheffAr3 = (TextView) convertView.findViewById(R.id.txtDasheffAr3);
        final TextView txtDasheffAr4 = (TextView) convertView.findViewById(R.id.txtDasheffAr4);
        final TextView txtDasheffAr5 = (TextView) convertView.findViewById(R.id.txtDasheffAr5);
        final TextView txtDasheffAr6 = (TextView) convertView.findViewById(R.id.txtDasheffAr6);
        final TextView txtDasheffAr7 = (TextView) convertView.findViewById(R.id.txtDasheffAr7);
        final LinearLayout layout_dasheff = (LinearLayout) convertView.findViewById(R.id.layout_dasheff);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dasheff);

        DataDasheff datax = items_dasheff.get(position);
        txtDasheffAr1.setText(datax.getAr_1());
        txtDasheffAr2.setText(datax.getAr_2());
        txtDasheffAr3.setText(datax.getAr_3());
        txtDasheffAr4.setText(datax.getAr_4());
        txtDasheffAr5.setText(datax.getAr_5());
        txtDasheffAr6.setText(datax.getAr_6());
        mrb.setColor(activity,Double.parseDouble(datax.getAr_7()),txtDasheffAr7);
        txtDasheffAr7.setText(datax.getAr_7()+"%");

        return convertView;
    }
}
