package com.nexsoft.firman.nexcloud;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nexsoft.firman.nexcloud.adapter.AdapterPvsd;
import com.nexsoft.firman.nexcloud.data.DataPvsd;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MySession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;

public class PvsdActivity extends AppCompatActivity {

    public static String typeChartPvsd = "RT";
    public static ArrayList<String> spinnerB1B2Pvsd = new ArrayList<String>();
    private LinearLayout atasPvsd;
    private TextView infoHistoriesPvsd, txtPvsdPesan;
    private ProgressDialog dialogprogress = null;
    private MySession sesi;
    private String TAG = "firman_PVSD", dataTxt="", txtInfoLokasi="", posisi_namaNow = "Home", sesiLoginUsername="",sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="", sesiLoginUserLevel="";
    private MaterialSpinner spinnerPvsdB1, spinnerPvsdThn, spinnerPvsdLevel, spinnerPvsdType, spinnerPvsdData;
    private ArrayList<String> ar_spinnerPvsdB1 = new ArrayList<String>();
    private ArrayList<String> ar_spinnerPvsdThn = new ArrayList<String>();
    private ArrayList<String> ar_spinnerPvsdLevel = new ArrayList<String>();
    private ArrayList<String> ar_spinnerPvsdType = new ArrayList<String>();
    private ArrayList<Integer> ar_pk = new ArrayList<Integer>();
    private ArrayList<Integer> ar_no = new ArrayList<Integer>();
    private ArrayList<Integer> ar_lokasi = new ArrayList<Integer>();
    private ArrayList<String> ar_1 = new ArrayList<String>();
    private ArrayList<String> ar_2 = new ArrayList<String>();
    private ListView list;
    private AdapterPvsd adapter;
    private List<DataPvsd> itemList = new ArrayList<DataPvsd>();
    private Boolean status_awal=true;
    private String finalIPnya = "";
    private int levelListnya=0;
    private int pk_yg_diklik, lokasi_yg_diklik=0;
    private final int id_local_pk=9;
    private Handler handler = new Handler();
    private int hitTime=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pvsd);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesi = new MySession();

        //dialogprogress = new ProgressDialog(this);
        //dialogprogress.setMessage("Please wait ...");
        //dialogprogress.setCancelable(false);

        sesiLoginUsername = sesi.getUsername(getApplicationContext());
        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginListDistPetik = sesi.getListDistPetik(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());
        sesiLoginUserLevel = sesi.getUserLevel(getApplicationContext());

        atasPvsd = (LinearLayout)findViewById(R.id.atasPvsd);
        spinnerPvsdB1 = (MaterialSpinner)findViewById(R.id.spinnerPvsdB1);
        //spinnerPvsdB2 = (MaterialSpinner)findViewById(R.id.spinnerPvsdB2);
        spinnerPvsdThn = (MaterialSpinner)findViewById(R.id.spinnerPvsdThn);
        spinnerPvsdLevel = (MaterialSpinner)findViewById(R.id.spinnerPvsdLevel);
        spinnerPvsdType = (MaterialSpinner)findViewById(R.id.spinnerPvsdType);
        infoHistoriesPvsd = (TextView)findViewById(R.id.infoHistoriesPvsd);
        txtPvsdPesan = (TextView)findViewById(R.id.txtPvsdPesan);
        list    = (ListView) findViewById(R.id.listViewPvsd);

        adapter = new AdapterPvsd(PvsdActivity.this, itemList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pk_yg_diklik = itemList.get(position).getAr_pk();
                Log.d("pk_yg_diklik", String.valueOf(pk_yg_diklik));
                lokasi_yg_diklik = itemList.get(position).getAr_lokasi();
                setHistoryUp(position);
                setKeListView();
            }
        });

        spinnerPvsdB1.setItems(setspinnerPvsdB1());
        spinnerPvsdThn.setItems(setspinnerPvsdThn());
        spinnerPvsdLevel.setItems(setspinnerPvsdLevel());
        spinnerPvsdType.setItems(setspinnerPvsdType());

        getTodayx();


        txtInfoLokasi = lokasi_yg_diklik+"";

        /*if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }*/

        /*hitTime=0;
        handler.postDelayed(runnable, MyConfig.delayToLoad);*/
        LoadDataFromServer();

    }

    /*private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hitTime+=1;
            if (hitTime<2) {
                handler.postDelayed(this, MyConfig.delayToLoad);
            }else {
                handler.removeCallbacks(runnable);
                LoadDataFromServer();
            }
            Log.e("ikan"+TAG,String.valueOf(hitTime));
        }
    };*/

    private void loadingStart() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.VISIBLE);
    }

    private void loadingStop() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.GONE);
    }

    private void getTodayx() {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DATE, -1);
        DateFormat sdf = new SimpleDateFormat("MM");
        spinnerPvsdB1.setSelectedIndex(Integer.parseInt(String.valueOf(sdf.format(today)))-1);
    }

    private ArrayList<String> setspinnerPvsdB1() {
        ar_spinnerPvsdB1.clear();
        ar_spinnerPvsdB1.add("Jan");
        ar_spinnerPvsdB1.add("Feb");
        ar_spinnerPvsdB1.add("Mar");
        ar_spinnerPvsdB1.add("Apr");
        ar_spinnerPvsdB1.add("May");
        ar_spinnerPvsdB1.add("Jun");
        ar_spinnerPvsdB1.add("Jul");
        ar_spinnerPvsdB1.add("Aug");
        ar_spinnerPvsdB1.add("Sep");
        ar_spinnerPvsdB1.add("Oct");
        ar_spinnerPvsdB1.add("Nov");
        ar_spinnerPvsdB1.add("Dec");
        return ar_spinnerPvsdB1;
    }

    private ArrayList<String> setspinnerPvsdThn() {
        ar_spinnerPvsdThn.clear();
        ar_spinnerPvsdThn.add("2017");
        ar_spinnerPvsdThn.add("2018");
        return ar_spinnerPvsdThn;
    }

    private ArrayList<String> setspinnerPvsdLevel() {
        ar_spinnerPvsdLevel.clear();
        int usrLvl = Integer.parseInt(sesiLoginUserLevel);
        for (int k=usrLvl; k<=6; k++){
            ar_spinnerPvsdLevel.add("Lvl: "+String.valueOf(k)+" - 6");
        }
        return ar_spinnerPvsdLevel;
    }

    private ArrayList<String> setspinnerPvsdType() {
        ar_spinnerPvsdType.clear();
        ar_spinnerPvsdType.add("Both");
        ar_spinnerPvsdType.add("T/Order");
        ar_spinnerPvsdType.add("Motorist");
        return ar_spinnerPvsdType;
    }

    private String getBulan1() {
        String hasil = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date convertedDate = dateFormat.parse(spinnerPvsdThn.getText().toString().trim() + "-" + String.valueOf(spinnerPvsdB1.getSelectedIndex()+1) + "-" + "01");
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
            hasil = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hasil;
    }

    private String getBulan2() {
        String hasil = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date convertedDate = dateFormat.parse(spinnerPvsdThn.getText().toString().trim() + "-" + String.valueOf(spinnerPvsdB1.getSelectedIndex()+1) + "-" + "01");
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
            hasil = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hasil;
    }

    private int getMaxDay() {
        int hasil = 0;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date convertedDate = dateFormat.parse(spinnerPvsdThn.getText().toString().trim() + "-" + String.valueOf(spinnerPvsdB1.getSelectedIndex()+1) + "-" + "01");
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
            hasil = Integer.parseInt(new SimpleDateFormat("dd").format(c.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hasil;
    }

    private String getLevel(){
        String[] split1 = spinnerPvsdLevel.getText().toString().trim().split("\\:");
        String[] split2 = split1[1].toString().trim().split("\\-");
        return split2[0].trim();
    }

    private String getTypex(){
        String typ = spinnerPvsdType.getText().toString();
        String rx = "";
        if (typ.equals("Both")){
            rx = "";
        }else if(typ.equals("T/Order")){
            rx = "TO";
        }else if(typ.equals("Motorist")){
            rx = "CV";
        }
        return rx;
    }

    private void LoadDataFromServer() {
        //Toast.makeText(getApplicationContext(),getBulan1() +" ~ "+ getBulan2() +" ~ "+ getLevel() +" ~ "+ getTypex(),Toast.LENGTH_SHORT).show();
        levelListnya = Integer.parseInt(getLevel());
        adapter.items_pvsd.clear();
        //dialogprogress.show();
        loadingStart();
        if (!IsConnected.CheckInternet()==true){
            LoadDataFromLocal(id_local_pk);
            if (txtPvsdPesan.getVisibility()!= View.VISIBLE) {
                txtPvsdPesan.setVisibility(View.VISIBLE);
            }
            //dialogprogress.dismiss();
            loadingStop();
            txtPvsdPesan.setText(getString(R.string.koneksi_internet_gagal));
        }else {

            if (txtPvsdPesan.getVisibility()==View.VISIBLE) {
                txtPvsdPesan.setVisibility(View.GONE);
            }

            String urlnya = "http://"+ finalIPnya +"/login/NexCloudAPI?MenuApp=MenuPVSD&txtdatefrom="+getBulan1()+
                    "&txtdatethru="+getBulan2()+
                    "&getPrincipalID="+sesiLoginPrincipalID+
                    "&getListDistributorID="+sesiLoginDistributorID+
                    "&userLevel="+sesiLoginUserLevel+
                    "&txttoplevel="+levelListnya+
                    "&txtTipe="+getTypex()+
                    "&listDist="+sesiLoginListDistPetik;

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                String content = res.getString("content");
                                String message = res.getString("message");
                                boolean success = res.getBoolean("success");
                                if(success==true){
                                    if (content.equals("")){
                                        //dialogprogress.dismiss();
                                        loadingStop();
                                        Toast.makeText(getApplicationContext(),"Tidak ada Data!",Toast.LENGTH_SHORT).show();
                                    }else {
                                        Log.d("PVSDcontent", content);
                                        updateDataLocal( spinnerPvsdB1.getSelectedIndex() + "^" + spinnerPvsdThn.getSelectedIndex() + "^" + spinnerPvsdLevel.getSelectedIndex() + "^" + spinnerPvsdType.getSelectedIndex(), content, id_local_pk);
                                        LoadDataFromLocal(id_local_pk);
                                    }
                                }else{
                                    finish();
                                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {}
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        //dialogprogress.dismiss();
                        loadingStop();
                        LoadDataFromLocal(id_local_pk);
                        if (txtPvsdPesan.getVisibility()!=View.VISIBLE) {
                            txtPvsdPesan.setVisibility(View.VISIBLE);
                        }
                        txtPvsdPesan.setText(getString(R.string.koneksi_server_gagal));
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }

    private void updateDataLocal(String tgl, String all_result, int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        db.updateTbNexcloud(tgl, all_result, idlocal);
        db.close();
    }

    private void LoadDataFromLocal(int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        Cursor cur = db.getTbNexcloudByID(idlocal);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            Log.d(TAG, "DB_Local_"+ cur.getString(0) +"_"+ cur.getString(1)+"_"+cur.getString(2)+"_"+cur.getString(3));
                            LoadToSetData(cur.getString(3),cur.getString(1));
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        db.close();
    }

    private void saveBulanTerpilih() {
        spinnerB1B2Pvsd.clear();
        for(int a=1; a<=getMaxDay(); a++){
            spinnerB1B2Pvsd.add(String.valueOf(a));
        }
        /*for(int a=0; a<spinnerB1B2Pvsd.size(); a++){
            Log.d("spinnerB1B2Pvsd",spinnerB1B2Pvsd.get(a));
        }*/
    }

    private void LoadToSetData(String valData, String tgl) {
        saveBulanTerpilih();
        //spinnerB1B2PvsD.add(String.valueOf(spinnerPvsdB1.getSelectedIndex()));
        if (!tgl.equals("")) {
            String[] valDataInduk = tgl.toString().trim().split("\\^");
            if (valDataInduk.length>3){
                spinnerPvsdB1.setSelectedIndex(Integer.parseInt(valDataInduk[0]));
                spinnerPvsdThn.setSelectedIndex(Integer.parseInt(valDataInduk[1]));
                spinnerPvsdLevel.setSelectedIndex(Integer.parseInt(valDataInduk[2]));
                spinnerPvsdType.setSelectedIndex(Integer.parseInt(valDataInduk[3]));
            }
        }
        if (!valData.equals("")) {
            Log.d(TAG, valData + " & " + tgl);
            dataTxt = valData;
            simpanKeArray();
            lihatDataArray();
            setKeListView();
        }else{
            Toast.makeText(getApplicationContext(),"Tidak ada data!",Toast.LENGTH_SHORT).show();
        }
        //dialogprogress.dismiss();
        loadingStop();
    }


    private void simpanKeArray() {
        setArrayNull();

        //Simpan PK
        String ar2 = "";
        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        for (int a = 0; a < itemInduk.length; a++) {
            ar2 = "";
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_pk.add(a + 1);//+1
            ar_no.add(Integer.valueOf(itemAnak[0]));
            ar_lokasi.add(99999);
            ar_1.add(itemAnak[1]);
            for (int b = 2; b < itemAnak.length; b++){
                ar2+=itemAnak[b]+"~";
            }
            ar_2.add(ar2);
        }
        //update Lokasi
        if (levelListnya==0){
            for (int i=0; i<ar_pk.size(); i++){
                if (ar_no.get(i)==0){
                    ar_lokasi.set(i,0);
                }else if(ar_no.get(i)==1){
                    ar_lokasi.set(i,1);
                }else if(ar_no.get(i)>1) {
                    for (int b=i; b<ar_pk.size(); b++){
                        if(ar_no.get(b)==ar_no.get(i) && ar_no.get(b)>ar_no.get(b-1)){
                            ar_lokasi.set(b,ar_pk.get(i-1));
                        }else if(ar_no.get(b)>ar_no.get(i)){
                            ar_lokasi.set(b,ar_pk.get(i));
                        }
                    }
                }
            }
        }else{
            for (int i=0; i<ar_pk.size(); i++){
                if (i==0 || ar_no.get(i)==levelListnya){
                    ar_lokasi.set(i,0);
                }else{
                    if(ar_no.get(i)>ar_no.get(i-1)){
                        ar_lokasi.set(i,ar_pk.get(i-1));
                    }else if(ar_no.get(i)==ar_no.get(i-1)){
                        ar_lokasi.set(i,ar_lokasi.get(i-1));
                    }else if (ar_no.get(i)<ar_no.get(i-1)){ //no skrg < no sebelumnya
                        int popo = cekLokasiOFnumber(ar_no.get(i),i);
                        ar_lokasi.set(i,popo);
                    }
                }
            }
        }

    }

    private void setArrayNull() {
        ar_pk.clear();
        ar_lokasi.clear();
        ar_no.clear();
        ar_1.clear();
        ar_2.clear();
    }

    private int cekLokasiOFnumber(int val, int i) {
        int hasil=0;
        for (int x=i-1; x>=0; x--){
            if (ar_no.get(x)==val){
                hasil = ar_lokasi.get(x);
                break;
            }
        }
        return hasil;
    }

    private void lihatDataArray() {
        for (int i=0; i<ar_no.size(); i++){
            Log.d(TAG, "Lihat Array " +
                    String.valueOf(ar_pk.get(i)) + "_"+
                    String.valueOf(ar_lokasi.get(i)) + "_"+
                    String.valueOf(ar_no.get(i)) + "_"+
                    String.valueOf(ar_1.get(i)) + "_"+
                    String.valueOf(ar_2.get(i))
            );
        }
    }

    private void setKeListView() {

        if (status_awal==true){
            //muncul pertama
            adapter.items_pvsd.clear();
            for (int a=0; a<ar_pk.size(); a++){
                if(ar_lokasi.get(a)==0){
                    DataPvsd ita = new DataPvsd();
                    ita.setAr_pk(ar_pk.get(a));
                    ita.setAr_lokasi(ar_lokasi.get(a));
                    ita.setAr_no(ar_no.get(a));
                    ita.setAr_1(ar_1.get(a));
                    ita.setAr_2(ar_2.get(a));
                    itemList.add(ita);
                    adapter.notifyDataSetChanged();
                }
            }
            status_awal=false;
        }else {
            if (cekDataPkLokasi()==true){
                //muncul selanjutnya
                txtInfoLokasi = txtInfoLokasi+"^"+pk_yg_diklik;
                adapter.items_pvsd.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    Log.d("TES",a+"");
                    if(ar_lokasi.get(a)==pk_yg_diklik){
                        Log.d("TES_TRUE_IF",ar_lokasi.get(a)+"");
                        DataPvsd ita = new DataPvsd();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_1(ar_1.get(a));
                        ita.setAr_2(ar_2.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
            }else {
                if (txtPvsdPesan.getVisibility()!= View.VISIBLE) {
                    txtPvsdPesan.setVisibility(View.VISIBLE);
                }
                txtPvsdPesan.setText("Maaf, tidak ada data lagi !");
            }
        }
        //dialogprogress.dismiss();
        loadingStop();
    }

    private Boolean cekDataPkLokasi() {
        Boolean bol = false;
        for (int a=0; a<ar_pk.size(); a++){
            if(ar_lokasi.get(a)==pk_yg_diklik){
                bol=true;
                break;
            }
        }
        return bol;
    }

    private void setHistoryUp(int position) {
        if(cekDataPkLokasi()==true){
            infoHistoriesPvsd.setVisibility(View.VISIBLE);
            posisi_namaNow = posisi_namaNow+" > "+itemList.get(position).getAr_1();
            infoHistoriesPvsd.setText(posisi_namaNow);
        }
    }

    private void kembaliKeHeader() {
        String lokasiUriLast="NULL";
        String lokasiUri = txtInfoLokasi;
        String lastLocationToTxt="";
        Log.d("KKH_lokasiUri",lokasiUri);
        String[] lokasiUriCut = lokasiUri.toString().trim().split("\\^");
        for (int a=0; a<lokasiUriCut.length; a++){
            if (a==lokasiUriCut.length-2){
                lokasiUriLast = String.valueOf(lokasiUriCut[a]);
            }
            if (a<lokasiUriCut.length-1){
                if (a==lokasiUriCut.length-2){
                    lastLocationToTxt += lokasiUriCut[a];
                }else {
                    lastLocationToTxt += lokasiUriCut[a] + "^";
                }
            }
        }
        Log.d("KKH_lastLocationToTxt",lastLocationToTxt);
        txtInfoLokasi = lastLocationToTxt;//.setText(lastLocationToTxt);
        Log.d("KKH_lokasiUriLast",lokasiUriLast);
        if (!lokasiUriLast.equals("NULL")){
            Log.d("KKH_lokasiUriLast","OKEH_SHOW_LOKASI_"+lokasiUriLast);
            if (lokasiUriLast.equals("")){
                lokasiUriLast="0";
            }
            if(Integer.parseInt(lokasiUriLast)>0){
                //munculin data WHERE lokasi_di_array == PK_nya
                Log.d("KKH_JADI","munculin data WHERE lokasi_di_array == PK_nya "+lokasiUriLast);
                adapter.items_pvsd.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    Log.d("KKH_JADI_FOR",""+ar_lokasi.get(a));
                    if(String.valueOf(ar_lokasi.get(a)).equals(lokasiUriLast)){
                        Log.d("KKH_JADI_FOR_",""+ar_lokasi.get(a));
                        DataPvsd ita = new DataPvsd();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_1(ar_1.get(a));
                        ita.setAr_2(ar_2.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
                //SET HISTORY BACK
                setHistoryDown();
                //update_text
            }else{
                //munculin data WHERE no_di_array == 0
                Log.d("KKH_JADI","munculin data WHERE no_di_array == 0");
                /*if (levelListnya>0){
                    txtInfoLokasi = String.valueOf(levelListnya);
                }else{
                    txtInfoLokasi = "0";
                }*/
                txtInfoLokasi = "0";
                posisi_namaNow = "Home";
                infoHistoriesPvsd.setVisibility(View.GONE);
                //muncul pertama
                adapter.items_pvsd.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    if(ar_no.get(a)==levelListnya){
                        DataPvsd ita = new DataPvsd();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_1(ar_1.get(a));
                        ita.setAr_2(ar_2.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
            }
        }
    }

    private void setHistoryDown() {
        String posisi_endNama = "";
        String[] hd = posisi_namaNow.toString().trim().split("\\ > ");
        for (int a=1; a<hd.length-1; a++){
            Log.d("HDHD1", hd[a]);
            posisi_endNama+= " > "+hd[a];
        }
        Log.d("HDHD2", posisi_endNama);
        posisi_namaNow = "Home "+posisi_endNama;
        infoHistoriesPvsd.setText(posisi_namaNow);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5 && keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (txtPvsdPesan.getVisibility()==View.VISIBLE) {
                txtPvsdPesan.setVisibility(View.GONE);
            }
            if (!txtInfoLokasi.equals("0") && infoHistoriesPvsd.getVisibility()==View.VISIBLE){
                kembaliKeHeader();
            }else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onBackPressed() {}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main3, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_sync:
                oneMoreLoad();
                LoadDataFromServer();
                return true;
            case R.id.action_tc:
                if (typeChartPvsd.equals("RT")){
                    typeChartPvsd="AC";
                }else if (typeChartPvsd.equals("AC")){
                    typeChartPvsd="RT";
                }
                Log.d("typeChartPvsd",typeChartPvsd);
                oneMoreLoad();
                LoadDataFromLocal(id_local_pk);
                return true;
            case R.id.action_sh:
                if (atasPvsd.getVisibility()==View.VISIBLE) {
                    atasPvsd.setVisibility(View.GONE);
                }else if (atasPvsd.getVisibility()==View.GONE){
                    atasPvsd.setVisibility(View.VISIBLE);
                }
                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_sr:
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation== Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void oneMoreLoad() {
        status_awal=true;
        infoHistoriesPvsd.setText("");
        txtInfoLokasi = "";
        posisi_namaNow = "Home";
        setArrayNull();
        if (infoHistoriesPvsd.getVisibility()== View.VISIBLE){
            infoHistoriesPvsd.setVisibility(View.GONE);
        }
    }


}
