package com.nexsoft.firman.nexcloud.data;

/**
 * Created by PATRA on 21-Mar-17.
 */

public class DataPSS {
    int ar_pk, ar_no, ar_lokasi;
    String ar_nama, ar_1ac, ar_1tg, ar_2ac, ar_2tg, ar_3ac, ar_3tg, ar_4ac, ar_4tg;

    public DataPSS() {
    }

    public DataPSS(int ar_pk, int ar_no, int ar_lokasi, String ar_nama, String ar_1ac, String ar_1tg, String ar_2ac, String ar_2tg, String ar_3ac, String ar_3tg, String ar_4ac, String ar_4tg) {
        this.ar_pk = ar_pk;
        this.ar_no = ar_no;
        this.ar_lokasi = ar_lokasi;
        this.ar_nama = ar_nama;
        this.ar_1ac = ar_1ac;
        this.ar_1tg = ar_1tg;
        this.ar_2ac = ar_2ac;
        this.ar_2tg = ar_2tg;
        this.ar_3ac = ar_3ac;
        this.ar_3tg = ar_3tg;
        this.ar_4ac = ar_4ac;
        this.ar_4tg = ar_4tg;
    }

    public int getAr_pk() {
        return ar_pk;
    }

    public void setAr_pk(int ar_pk) {
        this.ar_pk = ar_pk;
    }

    public int getAr_no() {
        return ar_no;
    }

    public void setAr_no(int ar_no) {
        this.ar_no = ar_no;
    }

    public int getAr_lokasi() {
        return ar_lokasi;
    }

    public void setAr_lokasi(int ar_lokasi) {
        this.ar_lokasi = ar_lokasi;
    }

    public String getAr_nama() {
        return ar_nama;
    }

    public void setAr_nama(String ar_nama) {
        this.ar_nama = ar_nama;
    }

    public String getAr_1ac() {
        return ar_1ac;
    }

    public void setAr_1ac(String ar_1ac) {
        this.ar_1ac = ar_1ac;
    }

    public String getAr_1tg() {
        return ar_1tg;
    }

    public void setAr_1tg(String ar_1tg) {
        this.ar_1tg = ar_1tg;
    }

    public String getAr_2ac() {
        return ar_2ac;
    }

    public void setAr_2ac(String ar_2ac) {
        this.ar_2ac = ar_2ac;
    }

    public String getAr_2tg() {
        return ar_2tg;
    }

    public void setAr_2tg(String ar_2tg) {
        this.ar_2tg = ar_2tg;
    }

    public String getAr_3ac() {
        return ar_3ac;
    }

    public void setAr_3ac(String ar_3ac) {
        this.ar_3ac = ar_3ac;
    }

    public String getAr_3tg() {
        return ar_3tg;
    }

    public void setAr_3tg(String ar_3tg) {
        this.ar_3tg = ar_3tg;
    }

    public String getAr_4ac() {
        return ar_4ac;
    }

    public void setAr_4ac(String ar_4ac) {
        this.ar_4ac = ar_4ac;
    }

    public String getAr_4tg() {
        return ar_4tg;
    }

    public void setAr_4tg(String ar_4tg) {
        this.ar_4tg = ar_4tg;
    }
}
