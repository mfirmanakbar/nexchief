package com.nexsoft.firman.nexcloud.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.nexsoft.firman.nexcloud.DashboardPvmActivity;
import com.nexsoft.firman.nexcloud.LoginActivity;

/**
 * Created by PATRA on 11-Apr-17.
 */

public class MySession {

    SharedPreferences sp;

    public String getLastDate(Context context){
        sp = context.getSharedPreferences("sesiLogin",Context.MODE_PRIVATE);
        return sp.getString("sesiLoginLastDate", null);
    }

    public String getUsername(Context context){
        sp = context.getSharedPreferences("sesiLogin",Context.MODE_PRIVATE);
        return sp.getString("sesiLoginUsername", null);
    }

    public String getPrincipalID(Context context){
        sp = context.getSharedPreferences("sesiLogin",Context.MODE_PRIVATE);
        return sp.getString("sesiLoginPrincipalID", null);
    }

    public String getListDistPetik(Context context){
        sp = context.getSharedPreferences("sesiLogin",Context.MODE_PRIVATE);
        return sp.getString("sesiLoginListDistPetik", null);
    }

    public String getDistributorID(Context context){
        sp = context.getSharedPreferences("sesiLogin",Context.MODE_PRIVATE);
        return sp.getString("sesiLoginDistributorID", null);
    }

    public String getUserLevel(Context context){
        sp = context.getSharedPreferences("sesiLogin",Context.MODE_PRIVATE);
        return sp.getString("sesiLoginUserLevel", null);
    }

    public String getListDistLength(Context context){
        sp = context.getSharedPreferences("sesiLogin",Context.MODE_PRIVATE);
        return sp.getString("sesiLoginListDistLength", null);
    }

    public void setSessionLogin(String usernamex, String principalIDx, String listDistPetikx, String distributorIDx, String userLevelx, String LastDate, String ListDistLength){
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("sesiLoginUsername", usernamex);
        editor.putString("sesiLoginPrincipalID", principalIDx);
        editor.putString("sesiLoginListDistPetik", listDistPetikx);
        editor.putString("sesiLoginDistributorID", distributorIDx);
        editor.putString("sesiLoginUserLevel", userLevelx);
        editor.putString("sesiLoginLastDate", LastDate);
        editor.putString("sesiLoginListDistLength", ListDistLength);
        editor.commit();
    }

    public boolean checkSession(Context context){
        boolean cek = false;
        sp = context.getSharedPreferences("sesiLogin", Context.MODE_PRIVATE);
        String sesiLogin_email = sp.getString("sesiLoginUsername", null);
        if(sesiLogin_email!=null){
            cek = true;
        }
        return cek;
    }

    public void logOut(Context context){
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }

}
