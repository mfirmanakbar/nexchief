package com.nexsoft.firman.nexcloud;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.nexsoft.firman.nexcloud.adapter.AdapterSSA;
import com.nexsoft.firman.nexcloud.adapter.AdapterSSAdetail;
import com.nexsoft.firman.nexcloud.data.DataSSA;
import com.nexsoft.firman.nexcloud.data.DataSSAdetail;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.ModeCoding;

import java.util.ArrayList;
import java.util.List;

public class SkusoldActivityDetailActivity extends AppCompatActivity {

    private ListView list;
    private AdapterSSAdetail adapter;
    private List<DataSSAdetail> itemList = new ArrayList<DataSSAdetail>();

    private ArrayList<String> ar_dist = new ArrayList<String>();
    private ArrayList<String> ar_dist_name = new ArrayList<String>();
    private ArrayList<String> ar_city = new ArrayList<String>();
    private ArrayList<String> ar_cp = new ArrayList<String>();
    private ArrayList<String> ar_sku = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skusold_detail);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String lemparDetail = extras.getString("lemparDetail");
            Log.d("DataX", lemparDetail);
            saveDetail(lemparDetail);
        }

        list    = (ListView) findViewById(R.id.listViewSSAdetail);

        adapter = new AdapterSSAdetail(SkusoldActivityDetailActivity.this, itemList);
        list.setAdapter(adapter);

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main4, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
            }
            /*case R.id.action_sr:{
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation== Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveDetail(String lemparDetail) {

        ar_dist.clear();
        ar_dist_name.clear();
        ar_city.clear();
        ar_cp.clear();
        ar_sku.clear();

        String[] itemInduk = lemparDetail.toString().trim().split("\\~");
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\^");
            ar_dist.add(itemAnak[0]);
            ar_dist_name.add(itemAnak[1]);
            ar_city.add(itemAnak[2]);
            ar_cp.add(itemAnak[3]);
            ar_sku.add(itemAnak[4]);
        }

        for (int i=0; i<ar_dist.size(); i++){
            Log.d("BROH", "Lihat Array " +
                    String.valueOf(ar_dist.get(i)) + "_"+
                    String.valueOf(ar_dist_name.get(i)) + "_"+
                    String.valueOf(ar_city.get(i)) + "_" +
                    String.valueOf(ar_cp.get(i)) + "_"+
                    String.valueOf(ar_sku.get(i))
            );
        }

        setListview();

    }

    private void setListview() {

        for (int a=0; a<ar_dist.size(); a++){
            DataSSAdetail ita = new DataSSAdetail();
            ita.setAr_dist(ar_dist.get(a));
            ita.setAr_dist_name(ar_dist_name.get(a));
            ita.setAr_city(ar_city.get(a));
            ita.setAr_cp(ar_cp.get(a));
            ita.setAr_sku(ar_sku.get(a));
            itemList.add(ita);
            //adapter.notifyDataSetChanged();
        }

    }
}
