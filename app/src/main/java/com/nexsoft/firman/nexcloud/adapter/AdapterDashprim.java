package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDashou;
import com.nexsoft.firman.nexcloud.data.DataDashprim;
import com.nexsoft.firman.nexcloud.util.MyRainbow;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDashprim extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDashprim> items_dashprim;
    private final MyRainbow mrb = new MyRainbow();

    public AdapterDashprim(Activity activity, List<DataDashprim> items) {
        this.activity = activity;
        this.items_dashprim = items;
    }

    @Override
    public int getCount() {
        return items_dashprim.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dashprim.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dashprim, null);
        }

        final TextView txtDashprimAr1 = (TextView) convertView.findViewById(R.id.txtDashprimAr1);
        final TextView txtDashprimAr2 = (TextView) convertView.findViewById(R.id.txtDashprimAr2);
        final TextView txtDashprimAr3 = (TextView) convertView.findViewById(R.id.txtDashprimAr3);
        final TextView txtDashprimAr4 = (TextView) convertView.findViewById(R.id.txtDashprimAr4);
        final TextView txtDashprimAr5 = (TextView) convertView.findViewById(R.id.txtDashprimAr5);
        final TextView txtDashprimAr6 = (TextView) convertView.findViewById(R.id.txtDashprimAr6);
        final TextView txtDashprimAr7 = (TextView) convertView.findViewById(R.id.txtDashprimAr7);
        final LinearLayout layout_dashprim = (LinearLayout) convertView.findViewById(R.id.layout_dashprim);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dashprim);

        DataDashprim datax = items_dashprim.get(position);
        txtDashprimAr1.setText(datax.getAr_1());
        txtDashprimAr2.setText(datax.getAr_2());
        txtDashprimAr3.setText(datax.getAr_3());
        txtDashprimAr4.setText(datax.getAr_4());
        txtDashprimAr5.setText(datax.getAr_5());
        txtDashprimAr6.setText(datax.getAr_6());
        mrb.setColor(activity,Double.parseDouble(datax.getAr_7()),txtDashprimAr7);
        txtDashprimAr7.setText(datax.getAr_7()+"%");

        return convertView;
    }
}
