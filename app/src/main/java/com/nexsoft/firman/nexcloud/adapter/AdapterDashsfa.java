package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDashsfa;
import com.nexsoft.firman.nexcloud.util.MyRainbow;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDashsfa extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDashsfa> items_dashsfa;

    public AdapterDashsfa(Activity activity, List<DataDashsfa> items) {
        this.activity = activity;
        this.items_dashsfa = items;
    }

    @Override
    public int getCount() {
        return items_dashsfa.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dashsfa.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dashsfa, null);
        }

        final TextView txtDashsfaAr1 = (TextView) convertView.findViewById(R.id.txtDashsfaAr1);
        final TextView txtDashsfaAr2 = (TextView) convertView.findViewById(R.id.txtDashsfaAr2);
        final TextView txtDashsfaAr3 = (TextView) convertView.findViewById(R.id.txtDashsfaAr3);
        final TextView txtDashsfaAr4 = (TextView) convertView.findViewById(R.id.txtDashsfaAr4);
        final LinearLayout layout_dashsfa = (LinearLayout) convertView.findViewById(R.id.layout_dashsfa);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dashsfa);

        DataDashsfa datax = items_dashsfa.get(position);
        txtDashsfaAr1.setText(datax.getAr_1());
        txtDashsfaAr2.setText(datax.getAr_2());
        txtDashsfaAr3.setText(datax.getAr_3());
        txtDashsfaAr4.setText(datax.getAr_4());

        return convertView;
    }
}
