package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDashact;
import com.nexsoft.firman.nexcloud.data.DataDashsec;
import com.nexsoft.firman.nexcloud.util.MyRainbow;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDashact extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDashact> items_dashact;
    private final MyRainbow mrb = new MyRainbow();

    public AdapterDashact(Activity activity, List<DataDashact> items) {
        this.activity = activity;
        this.items_dashact = items;
    }

    @Override
    public int getCount() {
        return items_dashact.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dashact.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dashact, null);
        }

        final TextView txtDashactAr1 = (TextView) convertView.findViewById(R.id.txtDashactAr1);
        final TextView txtDashactAr2 = (TextView) convertView.findViewById(R.id.txtDashactAr2);
        final TextView txtDashactAr3 = (TextView) convertView.findViewById(R.id.txtDashactAr3);
        final TextView txtDashactAr4 = (TextView) convertView.findViewById(R.id.txtDashactAr4);
        final TextView txtDashactAr5 = (TextView) convertView.findViewById(R.id.txtDashactAr5);
        final TextView txtDashactAr6 = (TextView) convertView.findViewById(R.id.txtDashactAr6);
        final TextView txtDashactAr7 = (TextView) convertView.findViewById(R.id.txtDashactAr7);
        final LinearLayout layout_dashact = (LinearLayout) convertView.findViewById(R.id.layout_dashact);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dashact);

        DataDashact datax = items_dashact.get(position);
        txtDashactAr1.setText(datax.getAr_1());
        txtDashactAr2.setText(datax.getAr_2());
        txtDashactAr3.setText(datax.getAr_3());
        txtDashactAr4.setText(datax.getAr_4());
        txtDashactAr5.setText(datax.getAr_5());
        txtDashactAr6.setText(datax.getAr_6());
        mrb.setColor(activity,Double.parseDouble(datax.getAr_7()),txtDashactAr7);
        txtDashactAr7.setText(datax.getAr_7()+"%");

        return convertView;
    }
}
