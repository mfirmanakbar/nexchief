package com.nexsoft.firman.nexcloud.data;

/**
 * Created by PATRA on 09-May-17.
 */

public class DataPvsd {

    int ar_pk, ar_no, ar_lokasi;
    String ar_1, ar_2;

    public DataPvsd() {
    }

    public DataPvsd(int ar_pk, int ar_no, int ar_lokasi, String ar_1, String ar_2) {
        this.ar_pk = ar_pk;
        this.ar_no = ar_no;
        this.ar_lokasi = ar_lokasi;
        this.ar_1 = ar_1;
        this.ar_2 = ar_2;
    }

    public int getAr_pk() {
        return ar_pk;
    }

    public void setAr_pk(int ar_pk) {
        this.ar_pk = ar_pk;
    }

    public int getAr_no() {
        return ar_no;
    }

    public void setAr_no(int ar_no) {
        this.ar_no = ar_no;
    }

    public int getAr_lokasi() {
        return ar_lokasi;
    }

    public void setAr_lokasi(int ar_lokasi) {
        this.ar_lokasi = ar_lokasi;
    }

    public String getAr_1() {
        return ar_1;
    }

    public void setAr_1(String ar_1) {
        this.ar_1 = ar_1;
    }

    public String getAr_2() {
        return ar_2;
    }

    public void setAr_2(String ar_2) {
        this.ar_2 = ar_2;
    }
}
