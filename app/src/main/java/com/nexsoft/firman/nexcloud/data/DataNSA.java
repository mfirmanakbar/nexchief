package com.nexsoft.firman.nexcloud.data;

/**
 * Created by PATRA on 10-Apr-17.
 */

public class DataNSA {
    String ar_dist_code, ar_dist_name, ar_city, ar_asm_name, ar_salesman_name, ar_date;

    public DataNSA() {
    }

    public DataNSA(String ar_dist_code, String ar_dist_name, String ar_city, String ar_asm_name, String ar_salesman_name, String ar_date) {
        this.ar_dist_code = ar_dist_code;
        this.ar_dist_name = ar_dist_name;
        this.ar_city = ar_city;
        this.ar_asm_name = ar_asm_name;
        this.ar_salesman_name = ar_salesman_name;
        this.ar_date = ar_date;
    }

    public String getAr_dist_code() {
        return ar_dist_code;
    }

    public void setAr_dist_code(String ar_dist_code) {
        this.ar_dist_code = ar_dist_code;
    }

    public String getAr_dist_name() {
        return ar_dist_name;
    }

    public void setAr_dist_name(String ar_dist_name) {
        this.ar_dist_name = ar_dist_name;
    }

    public String getAr_city() {
        return ar_city;
    }

    public void setAr_city(String ar_city) {
        this.ar_city = ar_city;
    }

    public String getAr_asm_name() {
        return ar_asm_name;
    }

    public void setAr_asm_name(String ar_asm_name) {
        this.ar_asm_name = ar_asm_name;
    }

    public String getAr_salesman_name() {
        return ar_salesman_name;
    }

    public void setAr_salesman_name(String ar_salesman_name) {
        this.ar_salesman_name = ar_salesman_name;
    }

    public String getAr_date() {
        return ar_date;
    }

    public void setAr_date(String ar_date) {
        this.ar_date = ar_date;
    }
}
