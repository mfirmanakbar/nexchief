package com.nexsoft.firman.nexcloud;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nexsoft.firman.nexcloud.adapter.AdapterDC;
import com.nexsoft.firman.nexcloud.data.DataDC;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudTgl;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MySession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DcActivity extends AppCompatActivity {

    TextView txtDcDate, txtDCPesan;
    private int mYear, mMonth, mDay;
    //private ProgressDialog dialogprogress = null;
    private String TAG = "firman_DC", dataTxt="", sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="", sesiLoginUserLevel="";

    MySession sesi;
    private String finalIPnya = "";

    private ArrayList<Integer> ar_pk = new ArrayList<Integer>();
    private ArrayList<Integer> ar_no = new ArrayList<Integer>();
    private ArrayList<Integer> ar_lokasi = new ArrayList<Integer>();
    private ArrayList<String> ar_1 = new ArrayList<String>();
    private ArrayList<String> ar_2 = new ArrayList<String>();
    private ArrayList<String> ar_3 = new ArrayList<String>();
    private ArrayList<String> ar_4 = new ArrayList<String>();
    private ArrayList<String> ar_5 = new ArrayList<String>();
    private ArrayList<String> ar_6 = new ArrayList<String>();
    private ArrayList<String> ar_7 = new ArrayList<String>();
    private ArrayList<String> ar_8 = new ArrayList<String>();
    private ArrayList<String> ar_9 = new ArrayList<String>();
    private ArrayList<String> ar_10 = new ArrayList<String>();
    private ArrayList<String> ar_11 = new ArrayList<String>();
    private ArrayList<String> ar_12 = new ArrayList<String>();

    private ListView list;
    private AdapterDC adapter;
    private List<DataDC> itemList = new ArrayList<DataDC>();

    private String lemparDetail="";
    private int pk_yg_diklik;
    private final int id_local_pk=7;
    private String rotasinya = "";
    private Handler handler = new Handler();
    private int hitTime=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dc);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesi = new MySession();

        txtDcDate = (TextView)findViewById(R.id.txtDcDate);
        txtDCPesan = (TextView)findViewById(R.id.txtDCPesan);

        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());
        sesiLoginUserLevel = sesi.getUserLevel(getApplicationContext());

        /*dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);*/

        list    = (ListView) findViewById(R.id.listViewDC);

        adapter = new AdapterDC(DcActivity.this, itemList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(ar_pk.size()>1) {
                    lemparDetail = "";
                    pk_yg_diklik = 0;
                    pk_yg_diklik = Integer.parseInt(itemList.get(position).getAr_pk());
                    Log.d("aaa_1", String.valueOf(pk_yg_diklik));
                    for (int i = 0; i < ar_pk.size(); i++) {
                        if (ar_lokasi.get(i) == pk_yg_diklik) {
                            lemparDetail += ar_1.get(i) + "|" + ar_2.get(i) + "|" + ar_3.get(i) + "|" + ar_4.get(i) + "|" + ar_5.get(i) + "|" + ar_6.get(i) + "|" + ar_7.get(i) + "|" + ar_8.get(i) + "|" + ar_9.get(i) + "|" + ar_10.get(i) + "|" + ar_11.get(i) + "|" + ar_12.get(i) + "^";
                        }
                    }
                    Log.d("aaa_2", String.valueOf(lemparDetail));
                    Intent aa = new Intent(DcActivity.this,DcDetActivity.class);
                    aa.putExtra("lemparDetailDc", lemparDetail);
                    startActivity(aa);
                    overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
                }else{
                    Toast.makeText(getApplicationContext(),"Maaf, tidak ada data lagi!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        setDateNow();
        LoadDataFromServer();
        /*hitTime=0;
        handler.postDelayed(runnable, MyConfig.delayToLoad);*/

    }

    /*private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hitTime+=1;
            if (hitTime<2) {
                handler.postDelayed(this, MyConfig.delayToLoad);
            }else {
                handler.removeCallbacks(runnable);
                LoadDataFromServer();
            }
            Log.e("ikan"+TAG,String.valueOf(hitTime));
        }
    };*/

    private void loadingStart() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.VISIBLE);
    }

    private void loadingStop() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.GONE);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main4, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
            }
            /*case R.id.action_sr:{
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation==Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setDateNow() {
        TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
        txtDcDate.setText(pos.getNowTglC());
    }

    public void onClickDateDc(View view){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear+1;
                        int tgl = dayOfMonth;
                        String xBulan = "";
                        String xTgl = "";
                        if (bulan<10){
                            xBulan = "0"+String.valueOf(bulan);
                        }else{
                            xBulan = String.valueOf(bulan);
                        }
                        if (tgl<10){
                            xTgl = "0"+String.valueOf(tgl);
                        }else{
                            xTgl = String.valueOf(tgl);
                        }
                        txtDcDate.setText(year+"-"+xBulan+"-"+xTgl);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    public void onClickSubmitDC(View view){
        LoadDataFromServer();
    }

    private void LoadDataFromServer() {
        //dialogprogress.show();
        loadingStart();
        if (!IsConnected.CheckInternet()==true){
            loadingStop();
            if (txtDCPesan.getVisibility()==View.GONE) {
                txtDCPesan.setVisibility(View.VISIBLE);
            }
            txtDCPesan.setText(getString(R.string.koneksi_internet_gagal));
            LoadDataFromLocal(id_local_pk);
        }else {

            TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
            pos.updateTbNexcloudTgl(txtDcDate.getText().toString(),"",3);

            if (txtDCPesan.getVisibility()==View.VISIBLE) {
                txtDCPesan.setVisibility(View.GONE);
            }
            String postDateFromOnly = txtDcDate.getText().toString();

            String urlnya = "http://"+ finalIPnya+"/login/NexCloudAPI?MenuApp=MenuDC&txtdatefrom="+postDateFromOnly+
                    "&txtdatethru="+postDateFromOnly+
                    "&getPrincipalID="+sesiLoginPrincipalID+
                    "&getListDistributorID="+sesiLoginDistributorID+
                    "&userLevel="+sesiLoginUserLevel;

            Log.d("urlnya",urlnya);

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                //Log.e("DCDC", textResponse);
                                String content = res.getString("content");
                                if (content.equals("")){
                                    //dialogprogress.dismiss();
                                    loadingStop();
                                }else {
                                    updateDataLocal(txtDcDate.getText().toString()+"^"+txtDcDate.getText().toString(), content, id_local_pk);
                                    LoadDataFromLocal(id_local_pk);
                                }
                                //dialogprogress.dismiss();
                            } catch (JSONException e) {}
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        //dialogprogress.dismiss();
                        loadingStop();
                        LoadDataFromLocal(id_local_pk);
                        if (txtDCPesan.getVisibility()==View.GONE) {
                            txtDCPesan.setVisibility(View.VISIBLE);
                        }
                        txtDCPesan.setText(getString(R.string.koneksi_server_gagal));
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }

    private void updateDataLocal(String tgl, String all_result, int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        db.updateTbNexcloud(tgl, all_result, idlocal);
        db.close();
    }

    private void LoadDataFromLocal(int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        Cursor cur = db.getTbNexcloudByID(idlocal);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            Log.d(TAG, "DB_Local_"+ cur.getString(0) +"_"+ cur.getString(1)+"_"+cur.getString(2)+"_"+cur.getString(3));
                            LoadToSetData(cur.getString(3),cur.getString(1));
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        db.close();
    }

    private void LoadToSetData(String valData, String tgl) {
        if (!tgl.equals("")) {
            String[] valDataInduk = tgl.toString().trim().split("\\^");
            txtDcDate.setText(valDataInduk[0]);
        }
        if (!valData.equals("")) {
            dataTxt = valData;
            simpanKeArray();
            lihatDataArray();
            setKeListView();
        }else{
            Toast.makeText(getApplicationContext(),"Tidak ada data!",Toast.LENGTH_SHORT).show();
        }
    }

    private void simpanKeArray() {
        //kosongin array
        ar_pk.clear();
        ar_no.clear();
        ar_lokasi.clear();
        ar_1.clear();
        ar_2.clear();
        ar_3.clear();
        ar_4.clear();
        ar_5.clear();
        ar_6.clear();
        ar_7.clear();
        ar_8.clear();
        ar_9.clear();
        ar_10.clear();
        ar_11.clear();
        ar_12.clear();

        //Simpan PK
        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        int pkku = 0;
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            if(itemAnak[0].equals("6") || itemAnak[0].equals("7")){
                ar_pk.add(pkku);
                ar_no.add(Integer.valueOf(itemAnak[0]));
                ar_lokasi.add(0);
                ar_1.add(itemAnak[1]);
                ar_2.add(itemAnak[2]);
                ar_3.add(itemAnak[3]);
                ar_4.add(itemAnak[4]);
                ar_5.add(itemAnak[5]);
                ar_6.add(itemAnak[6]);
                ar_7.add(itemAnak[7]);
                ar_8.add(itemAnak[8]);
                ar_9.add(itemAnak[9]);
                ar_10.add(itemAnak[10]);
                ar_11.add(itemAnak[11]);
                ar_12.add(itemAnak[12]);
                pkku++;
            }
        }

        //update Lokasi type 4
        int angkaA=0, angkaB=0, lokasiNow=0;
        angkaA = ar_no.get(0);
        angkaB = ar_no.get(1);
        for (int i=0; i<ar_pk.size(); i++){
            if (ar_no.get(i)==angkaA){
                lokasiNow = ar_pk.get(i);
                ar_lokasi.set(i,ar_pk.get(i));
            }else if (ar_no.get(i)==angkaB){
                ar_lokasi.set(i,lokasiNow);
            }
        }
    }

    private void lihatDataArray() {
        for (int i=0; i<ar_no.size(); i++){
            Log.d(TAG, "Array DC " +
                    String.valueOf(ar_pk.get(i)) + "_"+
                    String.valueOf(ar_lokasi.get(i)) + "_"+
                    String.valueOf(ar_no.get(i)) + "_"+
                    String.valueOf(ar_1.get(i)) + "_"+
                    String.valueOf(ar_2.get(i)) + "_"+
                    String.valueOf(ar_3.get(i)) + "_"+
                    String.valueOf(ar_4.get(i)) + "_"+
                    String.valueOf(ar_5.get(i)) + "_"+
                    String.valueOf(ar_6.get(i)) + "_"+
                    String.valueOf(ar_7.get(i)) + "_"+
                    String.valueOf(ar_8.get(i)) + "_"+
                    String.valueOf(ar_9.get(i)) + "_"+
                    String.valueOf(ar_10.get(i)) + "_"+
                    String.valueOf(ar_11.get(i)) + "_"+
                    String.valueOf(ar_12.get(i))
            );
            Log.d("A12["+String.valueOf(i)+"]=", String.valueOf(ar_12.get(i)).trim());
        }
    }

    private void setKeListView() {
        adapter.items_dc.clear();
        for (int a=0; a<ar_pk.size(); a++){
            if(ar_no.get(a)==6){
                DataDC ita = new DataDC();
                ita.setAr_pk(String.valueOf(ar_pk.get(a)));
                ita.setAr_lokasi(String.valueOf(ar_lokasi.get(a)));
                ita.setAr_no(String.valueOf(ar_no.get(a)));
                ita.setAr_1(ar_1.get(a));
                ita.setAr_2(ar_2.get(a));
                ita.setAr_3(ar_3.get(a));
                ita.setAr_4(ar_4.get(a));
                ita.setAr_5(ar_5.get(a));
                ita.setAr_6(ar_6.get(a));
                ita.setAr_7(ar_7.get(a));
                ita.setAr_8(ar_8.get(a));
                ita.setAr_9(ar_9.get(a));
                ita.setAr_10(ar_10.get(a));
                ita.setAr_11(ar_11.get(a));
                ita.setAr_12(ar_12.get(a));
                itemList.add(ita);
                adapter.notifyDataSetChanged();
            }
        }
        //dialogprogress.dismiss();
        loadingStop();
    }


}
