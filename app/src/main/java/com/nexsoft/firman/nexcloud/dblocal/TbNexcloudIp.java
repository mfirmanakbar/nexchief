package com.nexsoft.firman.nexcloud.dblocal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by PATRA on 17-Mar-17.
 */

public class TbNexcloudIp {

    private SQLiteDatabase db;
    private final Context con;
    private final DbOpenHelper dbHelper;

    public TbNexcloudIp(Context con) {
        this.con = con;
        dbHelper = new DbOpenHelper(this.con, "", null, 0);
    }

    public void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    public void updateTbNexcloudIpAddressByID(String ipnya) {
        open();
        db.execSQL("UPDATE "+DbOpenHelper.KEY_TB_NEXCLOUD_IP_ADDRESS+" SET "+DbOpenHelper.KEY_IPNYA+"='"+ipnya+"' WHERE id_ip_ads='1' ");
        close();
    }

    public Cursor getTbNexcloudIpAddressByID(int id){
        return db.rawQuery("SELECT * FROM "+DbOpenHelper.KEY_TB_NEXCLOUD_IP_ADDRESS+" WHERE id_ip_ads=?", new String[]{String.valueOf(id)});
    }

    public String getIPAdrs() {
        String value="";
        open();
        Cursor cur = getTbNexcloudIpAddressByID(1);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            value = cur.getString(1);
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        close();
        return value;
    }

}
