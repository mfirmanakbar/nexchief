package com.nexsoft.firman.nexcloud;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MySession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity{

    private TextView txtLoginMessage;
    private EditText txtLoginEmail, txtLoginPassword;
    //private ProgressDialog dialogprogress = null;
    private String TAG = "firman_LOGIN", dataTxt="";
    private String usernamex, principalIDx, distributorIDx, listDistPetikx, userLevelx, loginEmail, loginPassword, ListDistLengthx;
    private MySession sesi;
    private Button btnLoginLogin;

    private Handler handler = new Handler();
    private int hitTime=0;
    private String finalIPnya = "";
    String dateString="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        sesi = new MySession();

        if (sesi.checkSession(getApplicationContext())==true){
            Intent a = new Intent(LoginActivity.this, DashboardPvmActivity.class);
            startActivity(a);
            finish();
        }else{
            TbNexcloud tbNex = new TbNexcloud(LoginActivity.this);
            tbNex.insertAll();
        }

        /*dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);*/

        btnLoginLogin = (Button)findViewById(R.id.btnLoginLogin);
        txtLoginMessage = (TextView)findViewById(R.id.txtLoginMessage);
        txtLoginEmail = (EditText)findViewById(R.id.txtLoginEmail);
        txtLoginPassword = (EditText)findViewById(R.id.txtLoginPassword);

        //txtLoginEmail.setText("albert"); txtLoginPassword.setText("@Brooklyn6");
        //txtLoginEmail.setText("dashboard"); txtLoginPassword.setText("password");
        //txtLoginEmail.setText("firman"); txtLoginPassword.setText("f1rm4n");

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        Log.d("finalIPnya_login", finalIPnya);

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        dateString = sdf.format(date);

    }

    public void ClickForLogin(View view){

        if (txtLoginEmail.length() < 1 && txtLoginPassword.length() < 1) {
            txtLoginMessage.setText("Tidak boleh kosong!");
        } else if (txtLoginEmail.length() < 1) {
            txtLoginMessage.setText("Email tidak boleh kosong!");
        } else if (txtLoginPassword.length() < 1) {
            txtLoginMessage.setText("Password tidak boleh kosong!");
        } else {
            txtLoginMessage.setText("Please Wait ... don't do anything!");
            btnLoginLogin.setText("Wait a few minute ...");
            //dialogprogress.show();
            //LoadLoginServer();
            hitTime=0;
            handler.postDelayed(runnable, MyConfig.delayToLoad);
        }

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hitTime+=1;
            if (hitTime<2) {
                handler.postDelayed(this, MyConfig.delayToLoad);
            }else {
                handler.removeCallbacks(runnable);
                LoadLoginServer();
            }
            Log.e("ikan"+TAG,String.valueOf(hitTime));
        }
    };

    private void LoadLoginServer() {

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        if (!IsConnected.CheckInternet()==true){
            //dialogprogress.dismiss();
            txtLoginMessage.setText(getString(R.string.koneksi_internet_gagal));
            btnLoginLogin.setText("LOGIN");
        }else {

            /*btnLoginLogin.setText("LOGIN");
            txtLoginMessage.setText("");*/
            loginEmail = txtLoginEmail.getText().toString();
            loginPassword = txtLoginPassword.getText().toString();

            String urlnya = "http://"+ finalIPnya+"/login/DoLoginAPI?userID="+loginEmail+"&password="+loginPassword;
            Log.d("urlnya",urlnya);

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                String content = res.getString("content");
                                String message = res.getString("message");
                                boolean success = res.getBoolean("success");
                                Log.d(TAG, "content: "+content);
                                Log.d(TAG, "message: "+message);
                                Log.d(TAG, "success: "+success);
                                if (success==true){
                                    String[] itemInduk = content.toString().trim().split("\\~");
                                    if(itemInduk.length>=6){
                                        usernamex = String.valueOf(itemInduk[0]);
                                        principalIDx = itemInduk[1];
                                        listDistPetikx = itemInduk[2];
                                        distributorIDx = itemInduk[3];
                                        userLevelx = itemInduk[4];
                                        ListDistLengthx = itemInduk[5];

                                        sesi.setSessionLogin(usernamex, principalIDx, listDistPetikx, distributorIDx, userLevelx, dateString, ListDistLengthx);

                                        Intent i = new Intent(LoginActivity.this, DashboardPvmActivity.class);
                                        startActivity(i);
                                        //overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
                                        finish();
                                        btnLoginLogin.setText("LOGIN");
                                        txtLoginMessage.setText("");
                                    }else{
                                        Toast.makeText(getApplicationContext(),"Maaf, terdapat kesalahan kami pada data user.",Toast.LENGTH_SHORT).show();
                                        Intent a = new Intent(LoginActivity.this,OopsActivity.class);
                                        startActivity(a);
                                    }
                                    //dialogprogress.dismiss();
                                }else if (success==false){
                                    btnLoginLogin.setText("LOGIN");
                                    txtLoginMessage.setText("");
                                    txtLoginMessage.setText(message);
                                    //dialogprogress.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        btnLoginLogin.setText("LOGIN");
                        txtLoginMessage.setText("");
                        //dialogprogress.dismiss();
                        txtLoginMessage.setText(getString(R.string.koneksi_server_gagal));
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }

    public void klikPopupIpAddress(View view){
        boolean aktif=false;
        if (aktif==true) {
            String finalIPnya = "";
            TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
            finalIPnya = pos.getIPAdrs();

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.popup_ip_address, null);
            dialogBuilder.setView(dialogView);

            final EditText edt = (EditText) dialogView.findViewById(R.id.editTextDialogUserInput);

            if (!finalIPnya.equals(null) || !finalIPnya.equals("")){
                edt.setText(finalIPnya);
            }

            dialogBuilder.setTitle("Custom IP Address");
            dialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (edt.length()>0) {
                        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
                        pos.updateTbNexcloudIpAddressByID(edt.getText().toString());
                        Toast.makeText(getApplicationContext(), "Successfully!", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(getApplicationContext(), "Empty Ip Address!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //pass
                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }
    }


}
