package com.nexsoft.firman.nexcloud.util;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;

/**
 * Created by PATRA on 26-May-17.
 */

public class MyRainbow {

    public static void setColor(Activity activity, double val, TextView txt) {
        if((int)Math.ceil(val)<40){
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if((int)Math.ceil(val)>=40 && (int)Math.ceil(val)<=80){
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if((int)Math.ceil(val)>80){
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }

    public static void setColorLayoutTb(Activity activity, int position, LinearLayout linearLayout) {
        if (position%2!=0){
            linearLayout.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorGrey5));
        }else{
            linearLayout.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorPutih));
        }
    }
}
