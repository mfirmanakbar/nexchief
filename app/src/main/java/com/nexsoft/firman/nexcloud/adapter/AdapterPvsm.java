package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.nexsoft.firman.nexcloud.PvsmActivity;
import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataPvsm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PATRA on 09-May-17.
 */

public class AdapterPvsm extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataPvsm> items_pvsm;

    private ArrayList<String> ar_entri1 = new ArrayList<String>();
    private ArrayList<String> ar_entri2 = new ArrayList<String>();
    private ArrayList<String> ar_date = new ArrayList<String>();

    public AdapterPvsm(Activity activity, List<DataPvsm> items) {
        this.activity = activity;
        this.items_pvsm = items;
    }

    @Override
    public int getCount() {
        return items_pvsm.size();
    }

    @Override
    public Object getItem(int position) {
        return items_pvsm.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_pvsm,null);
        }

        final TextView txtItemPvsmAr1 = (TextView)convertView.findViewById(R.id.txtItemPvsmAr1);
        final LineChart mChart = (LineChart) convertView.findViewById(R.id.chartBarPvsm);

        DataPvsm datax = items_pvsm.get(position);
        txtItemPvsmAr1.setText(datax.getAr_no()+" "+datax.getAr_1());

        ar_date.clear();
        ar_entri1.clear();
        ar_entri2.clear();

        for (int a=0; a<PvsmActivity.spinnerB1B2Pvsm.size(); a++) {
            Log.d("AdapterPvsmB1B2", PvsmActivity.spinnerB1B2Pvsm.get(a));
            ar_date.add(PvsmActivity.spinnerB1B2Pvsm.get(a));
        }

        String[] itemAnak = datax.getAr_2().toString().trim().split("\\~");
        for (int b = 0; b < itemAnak.length-2; b++){
            if(b==0 || b%2==0) {
                ar_entri1.add(itemAnak[b]);
                ar_entri2.add(itemAnak[b + 1]);
            }
            b++;
        }

        Log.d("xPvsm1", String.valueOf(ar_date.size()));
        Log.d("xPvsm2", String.valueOf(ar_entri1.size()));
        Log.d("xPvsm3", String.valueOf(ar_entri2.size()));

        /*chart begin*/
        mChart.highlightValues(null);
        mChart.invalidate();
        mChart.clear();

        Legend l = mChart.getLegend();
        l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setTextColor(Color.parseColor("#000000"));

        mChart.getDescription().setEnabled(false);
        mChart.setDrawGridBackground(false);
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setEnabled(false);
        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextColor(Color.parseColor("#000000"));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(ar_date.size());
        xAxis.setValueFormatter(null);
        xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());

        if (PvsmActivity.typeChartPvsm.equals("RT")){
            chartRt(mChart);
        }else if (PvsmActivity.typeChartPvsm.equals("AC")){
            chartAc(mChart);
        }

        /*chart ending*/

        return convertView;
    }

    private void chartAc(LineChart mChart) {
        ArrayList<Entry> entries1 = new ArrayList<Entry>();
        ArrayList<Entry> entries2 = new ArrayList<Entry>();
        float cumulativeEntries1 = 0, cumulativeEntries2 = 0;

        for (int index = 0; index < ar_date.size(); index++){
            if(Float.parseFloat(ar_entri1.get(index))<=0 && Float.parseFloat(ar_entri2.get(index))>0){
                cumulativeEntries1 = 0;
                cumulativeEntries2 = Float.parseFloat(ar_entri2.get(index));
            }else if(Float.parseFloat(ar_entri1.get(index))>0 && Float.parseFloat(ar_entri2.get(index))<=0){
                cumulativeEntries1 = Float.parseFloat(ar_entri1.get(index));
                cumulativeEntries2 = 0;
            }else if(Float.parseFloat(ar_entri1.get(index))<=0 && Float.parseFloat(ar_entri2.get(index))<=0){
                cumulativeEntries1 = 0;
                cumulativeEntries2 = 0;
            }else if(Float.parseFloat(ar_entri1.get(index))>0 && Float.parseFloat(ar_entri2.get(index))>0){
                cumulativeEntries1 = Float.parseFloat(ar_entri1.get(index));
                cumulativeEntries2 = Float.parseFloat(ar_entri2.get(index));
            }
            entries1.add(new Entry(index, cumulativeEntries1));
            entries2.add(new Entry(index, cumulativeEntries2));
        }

        LineDataSet set1 = new LineDataSet(entries1, "Salesman");
        set1.setDrawIcons(false);
        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(ContextCompat.getColor(activity,R.color.chartBlue));
        set1.setCircleColor(ContextCompat.getColor(activity,R.color.chartBlue));
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(false);
        set1.setFormLineWidth(1f);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);
        set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);//curved line / melengkung

        LineDataSet set2 = new LineDataSet(entries2, "Pv-Pro Sync");
        set2.setDrawIcons(false);
        set2.enableDashedHighlightLine(10f, 5f, 0f);
        set2.setColor(ContextCompat.getColor(activity,R.color.chartRed));
        set2.setCircleColor(ContextCompat.getColor(activity,R.color.chartRed));
        set2.setLineWidth(1f);
        set2.setCircleRadius(3f);
        set2.setDrawCircleHole(false);
        set2.setValueTextSize(9f);
        set2.setDrawFilled(false);
        set2.setFormLineWidth(1f);
        set2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set2.setFormSize(15.f);
        set2.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);//curved line / melengkung

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);
        dataSets.add(set2);
        LineData data = new LineData(dataSets);
        mChart.setData(data);
    }

    private void chartRt(LineChart mChart) {
        ArrayList<Entry> entries3 = new ArrayList<Entry>();
        float cumulativeEntries3 = 0;
        for (int index = 0; index < ar_date.size(); index++){
            if(Float.parseFloat(ar_entri1.get(index))<=0 || Float.parseFloat(ar_entri2.get(index))<=0){
                cumulativeEntries3 = 0;
            }else{
                cumulativeEntries3 = (Float.parseFloat(ar_entri2.get(index))/Float.parseFloat(ar_entri1.get(index))) * 100;
            }
            entries3.add(new Entry(index, cumulativeEntries3));
        }
        LineDataSet set3 = new LineDataSet(entries3, "Pv-Pro Sync Ratio");
        set3.setDrawIcons(false);
        set3.enableDashedHighlightLine(10f, 5f, 0f);
        set3.setColor(ContextCompat.getColor(activity,R.color.chartRed));
        set3.setCircleColor(ContextCompat.getColor(activity,R.color.chartRed));
        set3.setLineWidth(1f);
        set3.setCircleRadius(3f);
        set3.setDrawCircleHole(false);
        set3.setValueTextSize(9f);
        set3.setDrawFilled(false);
        set3.setFormLineWidth(1f);
        set3.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set3.setFormSize(15.f);
        set3.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);//curved line / melengkung

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set3);

        LineData data = new LineData(dataSets);
        mChart.setData(data);
    }

    private class MyCustomXAxisValueFormatter implements IAxisValueFormatter {
        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            //labels update
            if(ar_date.size() > (int) value) {
                return ar_date.get((int) value);
            } else return null;
        }
    }

}
