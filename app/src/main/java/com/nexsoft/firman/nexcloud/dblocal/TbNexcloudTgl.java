package com.nexsoft.firman.nexcloud.dblocal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by PATRA on 17-Mar-17.
 */

public class TbNexcloudTgl {

    private SQLiteDatabase db;
    private final Context con;
    private final DbOpenHelper dbHelper;

    public TbNexcloudTgl(Context con) {
        this.con = con;
        dbHelper = new DbOpenHelper(this.con, "", null, 0);
    }

    public void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    public Cursor getTbNexcloudTgl(){
        return db.rawQuery("SELECT * FROM "+DbOpenHelper.KEY_TB_NEXCLOUD, null);
    }

    public void updateTbNexcloudTgl(String tgl_a, String tgl_b, int id) {
        open();
        db.execSQL("UPDATE "+DbOpenHelper.KEY_TB_NEXCLOUD_TGL+" SET "+DbOpenHelper.KEY_TGL_A+"='"+tgl_a+"', "+DbOpenHelper.KEY_TGL_B+"='"+tgl_b+"' WHERE "+DbOpenHelper.KEY_ID_TBNTGL+"='"+id+"' ");
        close();
    }

    public Cursor getTbNexcloudTglByID(int id){
        return db.rawQuery("SELECT * FROM "+DbOpenHelper.KEY_TB_NEXCLOUD_TGL+" WHERE "+DbOpenHelper.KEY_ID_TBNTGL+"=?", new String[]{String.valueOf(id)});
    }

    public String getNowTglA() {
        String value="";
        open();
        Cursor cur = getTbNexcloudTglByID(1);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            value = cur.getString(1);
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        close();
        return value;
    }

    public String getNowTglB() {
        String value="";
        open();
        Cursor cur = getTbNexcloudTglByID(2);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            value = cur.getString(1);
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        close();
        return value;
    }

    public String getNowTglC() {
        String value="";
        open();
        Cursor cur = getTbNexcloudTglByID(3);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            value = cur.getString(1);
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        close();
        return value;
    }

    public String getNowTglD() {
        String value="";
        open();
        Cursor cur = getTbNexcloudTglByID(4);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            value = cur.getString(1);
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        close();
        return value;
    }

}
