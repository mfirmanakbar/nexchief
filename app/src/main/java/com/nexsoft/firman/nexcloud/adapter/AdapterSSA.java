package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataSSA;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by PATRA on 07-Apr-17.
 */

public class AdapterSSA extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataSSA> items_saa;

    public AdapterSSA(Activity activity, List<DataSSA> items) {
        this.activity = activity;
        this.items_saa = items;
    }

    @Override
    public int getCount() {
        return items_saa.size();
    }

    @Override
    public Object getItem(int position) {
        return items_saa.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_ssa,null);
        }

        final TextView txtSSADist = (TextView) convertView.findViewById(R.id.txtSSADist);
        final TextView txtSSADistName = (TextView) convertView.findViewById(R.id.txtSSADistName);
        final TextView txtSSACity = (TextView) convertView.findViewById(R.id.txtSSACity);
        final TextView txtSSACP = (TextView) convertView.findViewById(R.id.txtSSACP);
        final TextView txtSSASkuSold = (TextView) convertView.findViewById(R.id.txtSSASkuSold);

        DataSSA datax = items_saa.get(position);

        Log.d("TehMe", datax.getAr_dist()+" ~ "+datax.getAr_dist_name()+" ~ "+datax.getAr_city()+" ~ "+datax.getAr_cp()+" ~ "+datax.getAr_sku());

        if (datax.getAr_dist().length()>0){
            txtSSADist.setText(datax.getAr_dist());
        }else{
            txtSSADist.setText("-");
        }
        if (datax.getAr_dist_name().length()>0) {
            txtSSADistName.setText(datax.getAr_dist_name());
        }else{
            txtSSADistName.setText("-");
        }
        if (datax.getAr_city().length()>0) {
            txtSSACity.setText(datax.getAr_city());
        }else{
            txtSSACity.setText("-");
        }
        if (datax.getAr_cp().length()>0) {
            txtSSACP.setText(datax.getAr_cp());
        }else{
            txtSSACP.setText("-");
        }
        setColornya(datax.getAr_sku(), txtSSASkuSold);
        if(!datax.getAr_sku().equals("0.0") || !datax.getAr_sku().equals("0.00") && datax.getAr_sku().length()>3){
            String skuSold = datax.getAr_sku();
            Double skuSoldx = Double.valueOf(skuSold);
            DecimalFormat df = new DecimalFormat("#.##");
            txtSSASkuSold.setText(String.valueOf(df.format(skuSoldx)));
        }else{
            txtSSASkuSold.setText("0.00");
        }

        return convertView;
    }

    private void setColornya(String val, TextView txt) {
        txt.setTextColor(ContextCompat.getColor(activity,R.color.colorPutih));
        if(Double.valueOf(val)<=0){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if(Double.valueOf(val)>0 && Double.valueOf(val)<=8){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if(Double.valueOf(val)>8){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }
}

