package com.nexsoft.firman.nexcloud.dblocal;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DbOpenHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "nexsoft.db";

	public static final String KEY_TB_NEXCLOUD = "tb_nexcloud";
	public static final String KEY_ID_TBN = "id_tbn";
    public static final String KEY_TGL = "tgl";
	public static final String KEY_KATEGORI = "kategori";
	public static final String KEY_ISI = "isi";
	public static final String KEY_TB_NEXCLOUD_TGL = "tb_nexcloud_tgl";
	public static final String KEY_ID_TBNTGL = "id_tbn_tgl";
	public static final String KEY_TGL_A = "tgl_a";
	public static final String KEY_TGL_B = "tgl_b";
	public static final String KEY_TB_NEXCLOUD_IP_ADDRESS = "tb_nexcloud_ipaddress";
	public static final String KEY_IPNYA = "ipnya";
	public static final String KEY_TB_NEXCLOUD_ROTATION = "tb_nexcloud_rotation";
	public static final String KEY_ROTATION = "rotation";

	private static final String TABLE_NEXSOFT_CREATE =
            "CREATE TABLE "+
            KEY_TB_NEXCLOUD+" ("+
            KEY_ID_TBN+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            KEY_TGL+" TEXT, "+
            KEY_KATEGORI+" TEXT, "+
            KEY_ISI+" TEXT)";

	private static final String TABLE_NEXSOFT_TGL_FIRST_CREATE =
			"CREATE TABLE "+
					KEY_TB_NEXCLOUD_TGL+" ("+
					KEY_ID_TBNTGL+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_TGL_A+" TEXT, "+
					KEY_TGL_B+" TEXT)";

	private static final String TABLE_NEXSOFT_IP_ADDRESS_CREATE =
			"CREATE TABLE "+
					KEY_TB_NEXCLOUD_IP_ADDRESS+" (id_ip_ads INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_IPNYA+" TEXT)";

	private static final String TABLE_NEXSOFT_ROTATION_CREATE =
			"CREATE TABLE "+
					KEY_TB_NEXCLOUD_ROTATION +" (id_rot INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_ROTATION+" TEXT)";

	public DbOpenHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_NEXSOFT_CREATE);
		db.execSQL(TABLE_NEXSOFT_TGL_FIRST_CREATE);
		db.execSQL(TABLE_NEXSOFT_IP_ADDRESS_CREATE);
		db.execSQL(TABLE_NEXSOFT_ROTATION_CREATE);

		//IP ADDRESS
		String[] my_ip_address={
				"10.10.8.248",
				"10.10.8.88",
				"nexcloud.id"
		};
		ContentValues cv3 = new ContentValues();
		cv3.put(KEY_IPNYA, my_ip_address[2]);
		db.insert(KEY_TB_NEXCLOUD_IP_ADDRESS, null, cv3);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
		db.execSQL("DROP TABLE IF EXISTS tb_nexcloud");
		db.execSQL("DROP TABLE IF EXISTS tb_nexcloud_tgl");
		db.execSQL("DROP TABLE IF EXISTS tb_nexcloud_ipaddress");
		db.execSQL("DROP TABLE IF EXISTS tb_nexcloud_rotation");
		onCreate(db);
	}

}
