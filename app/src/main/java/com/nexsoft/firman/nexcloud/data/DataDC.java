package com.nexsoft.firman.nexcloud.data;

/**
 * Created by PATRA on 02-May-17.
 */

public class DataDC {

    String ar_pk, ar_no, ar_lokasi, ar_1, ar_2, ar_3, ar_4, ar_5, ar_6, ar_7, ar_8, ar_9, ar_10, ar_11, ar_12;

    public DataDC() {
    }

    public DataDC(String ar_pk, String ar_no, String ar_lokasi, String ar_1, String ar_2, String ar_3, String ar_4, String ar_5, String ar_6, String ar_7, String ar_8, String ar_9, String ar_10, String ar_11, String ar_12) {
        this.ar_pk = ar_pk;
        this.ar_no = ar_no;
        this.ar_lokasi = ar_lokasi;
        this.ar_1 = ar_1;
        this.ar_2 = ar_2;
        this.ar_3 = ar_3;
        this.ar_4 = ar_4;
        this.ar_5 = ar_5;
        this.ar_6 = ar_6;
        this.ar_7 = ar_7;
        this.ar_8 = ar_8;
        this.ar_9 = ar_9;
        this.ar_10 = ar_10;
        this.ar_11 = ar_11;
        this.ar_12 = ar_12;
    }

    public String getAr_pk() {
        return ar_pk;
    }

    public void setAr_pk(String ar_pk) {
        this.ar_pk = ar_pk;
    }

    public String getAr_no() {
        return ar_no;
    }

    public void setAr_no(String ar_no) {
        this.ar_no = ar_no;
    }

    public String getAr_lokasi() {
        return ar_lokasi;
    }

    public void setAr_lokasi(String ar_lokasi) {
        this.ar_lokasi = ar_lokasi;
    }

    public String getAr_1() {
        return ar_1;
    }

    public void setAr_1(String ar_1) {
        this.ar_1 = ar_1;
    }

    public String getAr_2() {
        return ar_2;
    }

    public void setAr_2(String ar_2) {
        this.ar_2 = ar_2;
    }

    public String getAr_3() {
        return ar_3;
    }

    public void setAr_3(String ar_3) {
        this.ar_3 = ar_3;
    }

    public String getAr_4() {
        return ar_4;
    }

    public void setAr_4(String ar_4) {
        this.ar_4 = ar_4;
    }

    public String getAr_5() {
        return ar_5;
    }

    public void setAr_5(String ar_5) {
        this.ar_5 = ar_5;
    }

    public String getAr_6() {
        return ar_6;
    }

    public void setAr_6(String ar_6) {
        this.ar_6 = ar_6;
    }

    public String getAr_7() {
        return ar_7;
    }

    public void setAr_7(String ar_7) {
        this.ar_7 = ar_7;
    }

    public String getAr_8() {
        return ar_8;
    }

    public void setAr_8(String ar_8) {
        this.ar_8 = ar_8;
    }

    public String getAr_9() {
        return ar_9;
    }

    public void setAr_9(String ar_9) {
        this.ar_9 = ar_9;
    }

    public String getAr_10() {
        return ar_10;
    }

    public void setAr_10(String ar_10) {
        this.ar_10 = ar_10;
    }

    public String getAr_11() {
        return ar_11;
    }

    public void setAr_11(String ar_11) {
        this.ar_11 = ar_11;
    }

    public String getAr_12() {
        return ar_12;
    }

    public void setAr_12(String ar_12) {
        this.ar_12 = ar_12;
    }
}
