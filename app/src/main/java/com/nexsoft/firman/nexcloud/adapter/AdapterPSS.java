package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataPSS;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by PATRA on 21-Mar-17.
 */

public class AdapterPSS extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataPSS> items;

    public AdapterPSS(Activity activity, List<DataPSS> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_ps_sales,null);
        }

        final DecimalFormat df = new DecimalFormat("#.##");
        final TextView noNama = (TextView) convertView.findViewById(R.id.txtPSSnoNama);
        final TextView txtCircle3Psm = (TextView)convertView.findViewById(R.id.txtCircle3Psm);
        final ProgressBar pgCircle3Psm = (ProgressBar)convertView.findViewById(R.id.pgCircle3Psm);
        final TextView txt3psma1 = (TextView)convertView.findViewById(R.id.txt3psma1);
        final TextView txt3psma2 = (TextView)convertView.findViewById(R.id.txt3psma2);
        final TextView txtCircle3ssm = (TextView)convertView.findViewById(R.id.txtCircle3ssm);
        final ProgressBar pgCircle3ssm = (ProgressBar)convertView.findViewById(R.id.pgCircle3ssm);
        final TextView txt3pssm1 = (TextView)convertView.findViewById(R.id.txt3pssm1);
        final TextView txt3pssm2 = (TextView)convertView.findViewById(R.id.txt3pssm2);
        final TextView txtCircle3psy = (TextView)convertView.findViewById(R.id.txtCircle3psy);
        final ProgressBar pgCircle3psy = (ProgressBar)convertView.findViewById(R.id.pgCircle3psy);
        final TextView txt3psy1 = (TextView)convertView.findViewById(R.id.txt3psy1);
        final TextView txt3psy2 = (TextView)convertView.findViewById(R.id.txt3psy2);

        final TextView txtCircle3ssy = (TextView)convertView.findViewById(R.id.txtCircle3ssy);
        final ProgressBar pgCircle3ssy = (ProgressBar)convertView.findViewById(R.id.pgCircle3ssy);
        final TextView txt3ssy1 = (TextView)convertView.findViewById(R.id.txt3ssy1);
        final TextView txt3ssy2 = (TextView)convertView.findViewById(R.id.txt3ssy2);

        /*final CircleProgress Circle3ssy = (CircleProgress) convertView.findViewById(R.id.idCircle3ssy);
        final TextView txt3ssy = (TextView) convertView.findViewById(R.id.txt3ssy);*/

        DataPSS datax = items.get(position);
        noNama.setText(String.valueOf(datax.getAr_no())+" ~ "+datax.getAr_nama());
        Double g1ac = Double.valueOf(datax.getAr_1ac());
        Double g1tg = Double.valueOf(datax.getAr_1tg());
        Double g2ac = Double.valueOf(datax.getAr_2ac());
        Double g2tg = Double.valueOf(datax.getAr_2tg());
        Double g3ac = Double.valueOf(datax.getAr_3ac());
        Double g3tg = Double.valueOf(datax.getAr_3tg());
        Double g4ac = Double.valueOf(datax.getAr_4ac());
        Double g4tg = Double.valueOf(datax.getAr_4tg());

        //CHART1
        if (g1ac!=0){
            g1ac/=1000000.00;
        }if (g1tg!=0){
            g1tg/=1000000.00;
        }
        double c1 = 0;
        if (g1ac!=0 && g1tg!= 0){
            c1 = (g1ac*100.00)/g1tg;
        }
        if ((c1 == Math.floor(c1)) && !Double.isInfinite(c1)) {
            txtCircle3Psm.setText(df.format(c1) + ".00%");
        }else {
            txtCircle3Psm.setText(df.format(c1) + "%");
        }
        setColornya(pgCircle3Psm,c1,txtCircle3Psm);
        pgCircle3Psm.setProgress(Integer.parseInt(String.valueOf(c1).split("\\.")[0]));
        txt3psma1.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(g1ac)))));
        txt3psma2.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(g1tg)))));

        //CHART2
        if (g2ac>0){
            g2ac/=1000000.00;
        }if (g2tg!=0){
            g2tg/=1000000.00;
        }
        double c2 = 0;
        if (g2ac>0 && g1tg>0){
            c2 = (g2ac*100.00)/g1tg;
        }if ((c2 == Math.floor(c2)) && !Double.isInfinite(c2)) {
            txtCircle3ssm.setText(df.format(c2) + ".00%");
        }else {
            txtCircle3ssm.setText(df.format(c2) + "%");
        }
        setColornya(pgCircle3ssm,c2,txtCircle3ssm);
        pgCircle3ssm.setProgress(Integer.parseInt(String.valueOf(c2).split("\\.")[0]));
        txt3pssm1.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(g2ac)))));
        txt3pssm2.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(g1tg)))));

        //CHART3
        if (g3ac!=0){
            g3ac/=1000000.00;
        }if (g3tg!=0){
            g3tg/=1000000.00;
        }
        double c3 = 0;
        if (g3ac!=0 && g3tg!= 0){
            c3 = (g3ac*100.00)/g3tg;
        }if ((c3 == Math.floor(c3)) && !Double.isInfinite(c3)) {
            txtCircle3psy.setText(df.format(c3) + ".00%");
        }else {
            txtCircle3psy.setText(df.format(c3) + "%");
        }
        setColornya(pgCircle3psy,c3,txtCircle3psy);
        pgCircle3psy.setProgress(Integer.parseInt(String.valueOf(c3).split("\\.")[0]));
        txt3psy1.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(g3ac)))));
        txt3psy2.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(g3tg)))));

        //CHART4
        if (g4ac!=0){
            g4ac/=1000000.00;
        }if (g4tg!=0){
            g4tg/=1000000.00;
        }
        double c4 = 0;
        if (g4ac!=0 && g3tg!= 0){
            c4 = (g4ac*100.00)/g3tg;
        }if ((c4 == Math.floor(c4)) && !Double.isInfinite(c4)) {
            txtCircle3ssy.setText(df.format(c4) + ".00%");
        }else {
            txtCircle3ssy.setText(df.format(c4) + "%");
        }
        setColornya(pgCircle3ssy,c4,txtCircle3ssy);
        pgCircle3ssy.setProgress(Integer.parseInt(String.valueOf(c4).split("\\.")[0]));
        txt3ssy1.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(g4ac)))));
        txt3ssy2.setText(java.text.NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(Math.round(g3tg)))));

        return convertView;
    }

    private void setColornya(ProgressBar progressBar, double val, TextView txt) {
        if((int)Math.ceil(val)<40){
            //progressBar.getProgressDrawable().setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorRed1), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if((int)Math.ceil(val)>=40 && (int)Math.ceil(val)<=80){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorYellow), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if((int)Math.ceil(val)>80){
            progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(activity,R.color.colorGreen), android.graphics.PorterDuff.Mode.SRC_IN);
            txt.setTextColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }
}
