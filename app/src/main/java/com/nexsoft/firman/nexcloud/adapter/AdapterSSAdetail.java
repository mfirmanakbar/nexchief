package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataSSAdetail;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by PATRA on 07-Apr-17.
 */

public class AdapterSSAdetail extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataSSAdetail> items_det;

    public AdapterSSAdetail(Activity activity, List<DataSSAdetail> items) {
        this.activity = activity;
        this.items_det = items;
    }

    @Override
    public int getCount() {
        return items_det.size();
    }

    @Override
    public Object getItem(int position) {
        return items_det.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_ssa_detail,null);
        }

        final TextView txtSsaDetName = (TextView) convertView.findViewById(R.id.txtSsaDetName);
        final TextView txtSsaDetType = (TextView) convertView.findViewById(R.id.txtSsaDetType);
        final TextView txtSsaDetCp = (TextView) convertView.findViewById(R.id.txtSsaDetCp);
        final TextView txtSsaDetSku = (TextView) convertView.findViewById(R.id.txtSsaDetSku);

        DataSSAdetail datax = items_det.get(position);
        if (datax.getAr_city().length()>0) {
            txtSsaDetType.setText(datax.getAr_city());
        }else{
            txtSsaDetType.setText("-");
        }
        if (datax.getAr_dist_name().length()>0) {
            txtSsaDetName.setText(datax.getAr_dist_name());
        }else{
            txtSsaDetName.setText("-");
        }
        if (datax.getAr_cp().length()>0) {
            txtSsaDetCp.setText(datax.getAr_cp());
        }else{
            txtSsaDetCp.setText("-");
        }
        setColornya(datax.getAr_sku(), txtSsaDetSku);
        if(!datax.getAr_sku().equals("0.0") || !datax.getAr_sku().equals("0.00") && datax.getAr_sku().length()>3){
            String skuSold = datax.getAr_sku();
            Double skuSoldx = Double.valueOf(skuSold);
            DecimalFormat df = new DecimalFormat("#.##");
            txtSsaDetSku.setText(String.valueOf(df.format(skuSoldx)));
        }else{
            txtSsaDetSku.setText("0.00");
        }
        return convertView;
    }

    private void setColornya(String val, TextView txt) {
        Log.d("valval",val);
        txt.setTextColor(ContextCompat.getColor(activity,R.color.colorPutih));
        if(Double.valueOf(val)<=0){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorRed1));
        }else if(Double.valueOf(val)>0 && Double.valueOf(val)<=8){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorYellow));
        }else if(Double.valueOf(val)>8){
            txt.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorGreen));
        }
    }
}
