package com.nexsoft.firman.nexcloud.navigation;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.nexsoft.firman.nexcloud.ActualCallActivity;
import com.nexsoft.firman.nexcloud.DashboardPvmActivity;
import com.nexsoft.firman.nexcloud.DcActivity;
import com.nexsoft.firman.nexcloud.LastlogActivity;
import com.nexsoft.firman.nexcloud.LoginActivity;
import com.nexsoft.firman.nexcloud.PSSalesActivity;
import com.nexsoft.firman.nexcloud.PSTActivity;
import com.nexsoft.firman.nexcloud.PvsdActivity;
import com.nexsoft.firman.nexcloud.PvsmActivity;
import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.SPTActivity;
import com.nexsoft.firman.nexcloud.SalesmanPerformanceActivity;
import com.nexsoft.firman.nexcloud.SkusoldActivity;
import com.nexsoft.firman.nexcloud.dblocal.DbOpenHelper;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.MySession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by PATRA on 18-May-17.
 */

public class NavigationCustom {

    private final Activity activity;
    private ProgressDialog dialogprogress = null;
    private MySession sesi;
    private String sesiLoginUsername="", finalIPnya="";

    public NavigationCustom(Activity activity) {
        this.activity = activity;
    }

    public void menu_apps(int position){
        Menu menu;

        if (position==0 && !String.valueOf(activity.getClass().getSimpleName()).equals("DashboardPvmActivity")){
            Intent a = new Intent (activity, DashboardPvmActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==1 && !String.valueOf(activity.getClass().getSimpleName()).equals("SalesmanPerformanceActivity")){
            Intent a = new Intent (activity, SalesmanPerformanceActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==2 && !String.valueOf(activity.getClass().getSimpleName()).equals("PSSalesActivity")){
            Intent a = new Intent(activity,PSSalesActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==3 && !String.valueOf(activity.getClass().getSimpleName()).equals("PvsmActivity")){
            Intent a = new Intent(activity,PvsmActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==4 && !String.valueOf(activity.getClass().getSimpleName()).equals("PvsdActivity")){
            Intent a = new Intent(activity,PvsdActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==5 && !String.valueOf(activity.getClass().getSimpleName()).equals("SPTActivity")){
            Intent a = new Intent(activity,SPTActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==6 && !String.valueOf(activity.getClass().getSimpleName()).equals("PSTActivity")){
            Intent a = new Intent(activity,PSTActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==7 && !String.valueOf(activity.getClass().getSimpleName()).equals("SkusoldActivity")){
            Intent a = new Intent(activity,SkusoldActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==8 && !String.valueOf(activity.getClass().getSimpleName()).equals("ActualCallActivity")){
            Intent a = new Intent(activity,ActualCallActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==9 && !String.valueOf(activity.getClass().getSimpleName()).equals("DcActivity")){
            Intent a = new Intent(activity,DcActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==10 && !String.valueOf(activity.getClass().getSimpleName()).equals("LastlogActivity")){
            Intent a = new Intent(activity,LastlogActivity.class);
            activity.startActivity(a);
            //activity.finish();
        }else if (position==11){
            dialogprogress = new ProgressDialog(activity);
            dialogprogress.setMessage("Please wait ...");
            dialogprogress.setCancelable(false);
            sesi = new MySession();
            sesiLoginUsername = sesi.getUsername(activity);
            TbNexcloudIp pos = new TbNexcloudIp(activity);
            finalIPnya = pos.getIPAdrs();
            logOutServer();
        }

    }

    public void logOutServer() {
        dialogprogress.show();
        if (!IsConnected.CheckInternet()==true){
            dialogprogress.dismiss();
            Toast.makeText(activity,activity.getString(R.string.koneksi_internet_gagal),Toast.LENGTH_SHORT).show();
        }else {
            String urlnya = "http://"+finalIPnya+"/logout/DoLogoutAPI?userID="+sesiLoginUsername;
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(urlnya)
                    .get()
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override public void onFailure(Call call, IOException e) {
                    Log.d("firman_test", "FAIL!");
                    try {
                        dialogprogress.dismiss();
                        Toast.makeText(activity,activity.getString(R.string.koneksi_server_gagal),Toast.LENGTH_SHORT).show();
                    }catch (Exception a){}
                }
                @Override public void onResponse(Call call, Response response) throws IOException {
                    final String textResponse = response.body().string();
                    activity.runOnUiThread(new Runnable() {
                        @Override public void run() {
                            try {
                                JSONObject res = new JSONObject(textResponse);
                                String content = res.getString("content");
                                String message = res.getString("message");
                                boolean success = res.getBoolean("success");
                                sesi.logOut(activity);
                                TbNexcloud tbNex = new TbNexcloud(activity);
                                tbNex.deleteAll();
                                Intent a = new Intent(activity,LoginActivity.class);
                                activity.startActivity(a);
                                activity.finish();
                                dialogprogress.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }
    }

}
