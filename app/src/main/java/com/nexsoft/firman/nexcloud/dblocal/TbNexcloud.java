package com.nexsoft.firman.nexcloud.dblocal;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by PATRA on 17-Mar-17.
 */

public class TbNexcloud {

    private SQLiteDatabase db;
    private final Context con;
    private final DbOpenHelper dbHelper;

    public TbNexcloud(Context con) {
        this.con = con;
        dbHelper = new DbOpenHelper(this.con, "", null, 0);
    }

    public void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    public Cursor getTbNexcloud(){
        return db.rawQuery("SELECT * FROM "+DbOpenHelper.KEY_TB_NEXCLOUD, null);
    }

    public void updateTbNexcloud(String tgl, String isi, int id) {
        db.execSQL("UPDATE "+DbOpenHelper.KEY_TB_NEXCLOUD+" SET "+DbOpenHelper.KEY_ISI+"='"+isi+"', "+DbOpenHelper.KEY_TGL+"='"+tgl+"' WHERE "+DbOpenHelper.KEY_ID_TBN+"='"+id+"' ");
    }

    public Cursor getTbNexcloudByID(int id){
        return db.rawQuery("SELECT * FROM "+DbOpenHelper.KEY_TB_NEXCLOUD+" WHERE "+DbOpenHelper.KEY_ID_TBN+"=?", new String[]{String.valueOf(id)});
    }


    public void deleteAll() {
        open();
        db.execSQL("delete from "+ DbOpenHelper.KEY_TB_NEXCLOUD);
        db.execSQL("delete from "+ DbOpenHelper.KEY_TB_NEXCLOUD_TGL);
        close();
    }

    private Date today() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        return cal.getTime();
    }

    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public void insertAll() {
        deleteAll();
        open();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String todayNow = sdf.format(today());

        String hasil = "", hasil2 = "";
        Calendar c = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
        c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));
        hasil = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        hasil2 = new SimpleDateFormat("yyyy-MM-dd").format(c2.getTime());
        String todayMin = hasil;
        String todayMax = hasil2;
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String yesTerDay = sdf2.format(yesterday());

        String[] kategori={
                "dsb","sp","pss","ssa","aca","nsa","dc","pvsm",
                "pvsd","spt","pst","dashnod","dashou","dashprim",
                "dashsec","dashsfa","dashact","dasheff","dasheffd",
                "dasheffsd","dashsales"
        };
        for(int i=0;i<kategori.length;i++){
            String id_pk = String.valueOf(i+1);
            db.execSQL("INSERT INTO tb_nexcloud (id_tbn,tgl,kategori,isi) VALUES('"+id_pk+"','','"+kategori[i]+"','')");
        }
        db.execSQL("INSERT INTO tb_nexcloud_tgl (id_tbn_tgl,tgl_a,tgl_b) VALUES('1','"+todayNow+"','')");
        db.execSQL("INSERT INTO tb_nexcloud_tgl (id_tbn_tgl,tgl_a,tgl_b) VALUES('2','"+todayMin+"','')");
        db.execSQL("INSERT INTO tb_nexcloud_tgl (id_tbn_tgl,tgl_a,tgl_b) VALUES('3','"+yesTerDay+"','')");
        db.execSQL("INSERT INTO tb_nexcloud_tgl (id_tbn_tgl,tgl_a,tgl_b) VALUES('4','"+todayMax+"','')");
        //db.execSQL("INSERT INTO tb_nexcloud_rotation (id_rot,rotation) VALUES('1','P')");
        close();
    }

    /*public Cursor getTbNexcloudRotation(int id){
        return db.rawQuery("SELECT * FROM "+DbOpenHelper.KEY_TB_NEXCLOUD_ROTATION+" WHERE id_rot=?", new String[]{String.valueOf(id)});
    }
    public String getRotation() {
        String value="";
        open();
        Cursor cur = getTbNexcloudRotation(1);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            value = cur.getString(1);
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        close();
        return value;
    }

    public void QueryUpdateRotation(String rots) {
        db.execSQL("UPDATE "+DbOpenHelper.KEY_TB_NEXCLOUD_ROTATION+" SET "+DbOpenHelper.KEY_ROTATION+"='"+rots+"' WHERE id_rot='1' ");
    }
    public void updateRotation(String rots) {
        TbNexcloud db = new TbNexcloud(con);
        db.open();
        db.QueryUpdateRotation(rots);
        db.close();
    }*/

}
