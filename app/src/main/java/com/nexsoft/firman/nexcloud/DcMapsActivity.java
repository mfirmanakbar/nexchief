package com.nexsoft.firman.nexcloud;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nexsoft.firman.nexcloud.adapter.AdapterDCMaps;
import com.nexsoft.firman.nexcloud.data.DataDCMaps;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyTextFormat;

import java.util.ArrayList;
import java.util.List;

import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import android.view.ViewTreeObserver.OnGlobalLayoutListener;

public class DcMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private ArrayList<String> ar_0 = new ArrayList<String>();
    private ArrayList<String> ar_1 = new ArrayList<String>();
    private ArrayList<String> ar_2 = new ArrayList<String>();
    private ArrayList<String> ar_3 = new ArrayList<String>();
    private ArrayList<String> ar_4 = new ArrayList<String>();
    private ArrayList<String> ar_5 = new ArrayList<String>();
    private String TAG = "dc_maps";
    private boolean map_full = false;

    private ListView list;
    private AdapterDCMaps adapter;
    private List<DataDCMaps> itemList = new ArrayList<DataDCMaps>();

    private TextView txtDcMapsDetTotalGs, txtDcMapsDetTotalSku;

    private Marker customMarker;

    private Double totalsku=0.0, totalgs=0.0;
    private final MyTextFormat mtf = new MyTextFormat();
    private LatLng latLngValue;

    private Double ar4, ar5;
    //tutor http://www.nasc.fr/android/android-using-layout-as-custom-marker-on-google-map-api/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dc_maps);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        txtDcMapsDetTotalSku = (TextView)findViewById(R.id.txtDcMapsDetTotalSku);
        txtDcMapsDetTotalGs = (TextView)findViewById(R.id.txtDcMapsDetTotalGs);

        list    = (ListView) findViewById(R.id.ListViewDcDetMaps);
        //list.setDivider(null);
        adapter = new AdapterDCMaps(DcMapsActivity.this, itemList);
        list.setAdapter(adapter);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String lemparMaps = extras.getString("lemparMaps");
            Log.d("lemparMaps", lemparMaps);
            if(!lemparMaps.trim().equals("End")){
                saveToko(lemparMaps);
            }
        }


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(ar_0.size()>1) {
                    onLocationChanged(Double.parseDouble(ar_4.get(position)), Double.parseDouble(ar_5.get(position)));
                }
            }
        });

    }

    public void onLocationChanged(double latitudex, double longitudex)    {
        double latitude = latitudex;
        double longitude = longitudex;
        CameraPosition camPos = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(18)
                .build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        mMap.animateCamera(camUpd3);
    }

    private void saveToko(String lemparMaps) {
        //maps 4 & 5

        ar_0.clear();
        ar_1.clear();
        ar_2.clear();
        ar_3.clear();
        ar_4.clear();
        ar_5.clear();

        String[] itemInduk = lemparMaps.toString().trim().split("\\__");
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\~");
            ar_0.add(itemAnak[0]);
            ar_1.add(itemAnak[1]);
            ar_2.add(itemAnak[2]);
            ar_3.add(itemAnak[3]);
            ar_4.add(itemAnak[4]);
            ar_5.add(itemAnak[5]);
        }

        //lihatDataArray();
        setListview();

    }

    private void setListview() {
        for (int a=0; a<ar_0.size(); a++){
            DataDCMaps ita = new DataDCMaps();
            ita.setAr_0(ar_0.get(a));
            ita.setAr_1(ar_1.get(a));
            ita.setAr_2(ar_2.get(a));
            ita.setAr_3(ar_3.get(a));
            ita.setAr_4(ar_4.get(a));
            ita.setAr_5(ar_5.get(a));
            itemList.add(ita);
        }
    }

    private void lihatDataArray() {
        for (int i=0; i<ar_0.size(); i++){
            Log.d(TAG, "ArrayDCMapping" +
                    String.valueOf(ar_0.get(i)) + "_"+
                    String.valueOf(ar_1.get(i)) + "_"+
                    String.valueOf(ar_2.get(i)) + "_"+
                    String.valueOf(ar_3.get(i)) + "_"+
                    String.valueOf(ar_4.get(i)) + "_"+
                    String.valueOf(ar_5.get(i))
            );
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(-6.175110, 106.865039) , 1.0f) );

        /*LatLng latLngValue = new LatLng(-6.2460705, 106.6463888);
        mMap.addMarker(new MarkerOptions().position(latLngValue).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngValue));
        mMap.animateCamera( CameraUpdateFactory.zoomTo(14.0f));*/

        for (int i=0; i<ar_0.size(); i++) {
            String ar0 = ar_0.get(i);
            String ar1 = ar_1.get(i);
            Double ar2 = Double.valueOf(ar_2.get(i));
            Double ar3 = Double.valueOf(ar_3.get(i));

            totalsku = totalsku + ar2;
            totalgs = totalgs + ar3;

            if (!ar_4.get(i).equals("null") || !ar_5.get(i).equals("null")) {
                ar4 = Double.valueOf(ar_4.get(i));
                ar5 = Double.valueOf(ar_5.get(i));
                latLngValue = new LatLng(ar4, ar5);

                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.icon_for_maps, null);
                TextView numTxt = (TextView) marker.findViewById(R.id.txtForMaps);
                numTxt.setText(String.valueOf(i + 1));

                customMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLngValue)
                        .title(ar0)
                        .snippet(ar1)
                        .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));

                /*mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(ar4, ar5)));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));*/

                /*final View mapView = getSupportFragmentManager().findFragmentById(R.id.map).getView();
                if (mapView.getViewTreeObserver().isAlive()) {
                    mapView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                        //@SuppressLint("NewApi")
                        @Override
                        public void onGlobalLayout() {
                            LatLngBounds bounds = new LatLngBounds.Builder().include(latLngValue).build();
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            } else {
                                mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 10));
                            *//*mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngValue));
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));*//*
                        }
                    });
                }*/


            }

        }

        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(ar4, ar5) , 8.0f) );

        txtDcMapsDetTotalSku.setText(mtf.format_angka(String.valueOf(Math.round(Double.valueOf(totalsku)))));
        txtDcMapsDetTotalGs.setText(mtf.format_angka(String.valueOf(Math.round(Double.valueOf(totalgs)))));

    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ActionBar.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


}
