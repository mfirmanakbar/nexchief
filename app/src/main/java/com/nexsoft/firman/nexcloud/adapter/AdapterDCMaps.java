package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDCMaps;
import com.nexsoft.firman.nexcloud.data.DataDCdetail;
import com.nexsoft.firman.nexcloud.util.MyTextFormat;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by firmanmac on 5/2/17.
 */

public class AdapterDCMaps extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDCMaps> items_dc_maps;
    private final MyTextFormat mtf = new MyTextFormat();

    public AdapterDCMaps(Activity activity, List<DataDCMaps> items) {
        this.activity = activity;
        this.items_dc_maps = items;
    }

    @Override
    public int getCount() {
        return items_dc_maps.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dc_maps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dcmaps,null);
        }

        final TextView txtDcDetMapNamaToko = (TextView)convertView.findViewById(R.id.txtDcDetMapNamaToko);
        final TextView txtDcDetMapAlamatToko = (TextView)convertView.findViewById(R.id.txtDcDetMapAlamatToko);
        final TextView txtDcDetMapSku = (TextView)convertView.findViewById(R.id.txtDcDetMapSku);
        final TextView txtDcDetMapGS = (TextView)convertView.findViewById(R.id.txtDcDetMapGS);


        DataDCMaps datax = items_dc_maps.get(position);

        txtDcDetMapNamaToko.setText(String.valueOf(position+1)+". "+datax.getAr_0());
        txtDcDetMapAlamatToko.setText(datax.getAr_1()+"\n"+datax.getAr_4()+", "+datax.getAr_5());
        txtDcDetMapSku.setText(datax.getAr_2());
        txtDcDetMapGS.setText(mtf.format_angka(String.valueOf(Math.round(Double.valueOf(datax.getAr_3())))));

        return convertView;
    }



}
