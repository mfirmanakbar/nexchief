package com.nexsoft.firman.nexcloud;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nexsoft.firman.nexcloud.adapter.AdapterSpt;
import com.nexsoft.firman.nexcloud.data.DataSpt;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MySession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SPTActivity extends AppCompatActivity {

    public static String typeChartSpt = "ratio";
    public static ArrayList<String> spinnerB1B2Spt = new ArrayList<String>();
    private LinearLayout atasSpt;
    private TextView infoHistoriesSpt, txtSptPesan;
    //private ProgressDialog dialogprogress = null;
    private MySession sesi;
    private String TAG = "firman_Spt", dataTxt="", txtInfoLokasi="", posisi_namaNow = "Home", sesiLoginUsername="",sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="", sesiLoginUserLevel="";
    private MaterialSpinner spinnerSptB1, spinnerSptB2, spinnerSptThn, spinnerSptLevel, spinnerSptType, spinnerSptData;
    private ArrayList<String> ar_spinnerSptB1 = new ArrayList<String>();
    private ArrayList<String> ar_spinnerSptThn = new ArrayList<String>();
    private ArrayList<String> ar_spinnerSptLevel = new ArrayList<String>();
    private ArrayList<Integer> ar_pk = new ArrayList<Integer>();
    private ArrayList<Integer> ar_no = new ArrayList<Integer>();
    private ArrayList<Integer> ar_lokasi = new ArrayList<Integer>();
    private ArrayList<String> ar_1 = new ArrayList<String>();
    private ArrayList<String> ar_2 = new ArrayList<String>();
    private ListView list;
    private AdapterSpt adapter;
    private List<DataSpt> itemList = new ArrayList<DataSpt>();
    private Boolean status_awal=true;
    private String finalIPnya = "";
    private int levelListnya=0;
    private int pk_yg_diklik, lokasi_yg_diklik=0;
    private final int id_local_pk=10;

    private Handler handler = new Handler();
    private int hitTime=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spt);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesi = new MySession();

        /*dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);*/

        sesiLoginUsername = sesi.getUsername(getApplicationContext());
        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginListDistPetik = sesi.getListDistPetik(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());
        sesiLoginUserLevel = sesi.getUserLevel(getApplicationContext());

        atasSpt = (LinearLayout)findViewById(R.id.atasSpt);
        spinnerSptB1 = (MaterialSpinner)findViewById(R.id.spinnerSptB1);
        spinnerSptB2 = (MaterialSpinner)findViewById(R.id.spinnerSptB2);
        spinnerSptThn = (MaterialSpinner)findViewById(R.id.spinnerSptThn);
        spinnerSptLevel = (MaterialSpinner)findViewById(R.id.spinnerSptLevel);
        infoHistoriesSpt = (TextView)findViewById(R.id.infoHistoriesSpt);
        txtSptPesan = (TextView)findViewById(R.id.txtSptPesan);
        list    = (ListView) findViewById(R.id.listViewSpt);

        adapter = new AdapterSpt(SPTActivity.this, itemList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pk_yg_diklik = itemList.get(position).getAr_pk();
                Log.d("pk_yg_diklik", String.valueOf(pk_yg_diklik));
                lokasi_yg_diklik = itemList.get(position).getAr_lokasi();
                setHistoryUp(position);
                setKeListView();
            }
        });

        spinnerSptB1.setItems(setspinnerSptB1());
        spinnerSptB2.setItems(setspinnerSptB1());
        spinnerSptThn.setItems(setspinnerSptThn());
        spinnerSptLevel.setItems(setspinnerSptLevel());

        getTodayx();

        txtInfoLokasi = lokasi_yg_diklik+"";

        /*hitTime=0;
        handler.postDelayed(runnable, MyConfig.delayToLoad);*/
        LoadDataFromServer();

    }

    /*private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hitTime+=1;
            if (hitTime<2) {
                handler.postDelayed(this, MyConfig.delayToLoad);
            }else {
                handler.removeCallbacks(runnable);
                LoadDataFromServer();
            }
            Log.e("ikan"+TAG,String.valueOf(hitTime));
        }
    };*/

    private void loadingStart() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.VISIBLE);
    }

    private void loadingStop() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.GONE);
    }

    private void getTodayx() {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DATE, -1);
        DateFormat sdf = new SimpleDateFormat("MM");
        spinnerSptB2.setSelectedIndex(Integer.parseInt(String.valueOf(sdf.format(today)))-1);
    }

    private ArrayList<String> setspinnerSptB1() {
        ar_spinnerSptB1.clear();
        ar_spinnerSptB1.add("Jan");
        ar_spinnerSptB1.add("Feb");
        ar_spinnerSptB1.add("Mar");
        ar_spinnerSptB1.add("Apr");
        ar_spinnerSptB1.add("May");
        ar_spinnerSptB1.add("Jun");
        ar_spinnerSptB1.add("Jul");
        ar_spinnerSptB1.add("Aug");
        ar_spinnerSptB1.add("Sep");
        ar_spinnerSptB1.add("Oct");
        ar_spinnerSptB1.add("Nov");
        ar_spinnerSptB1.add("Dec");
        return ar_spinnerSptB1;
    }

    private ArrayList<String> setspinnerSptThn() {
        ar_spinnerSptThn.clear();
        ar_spinnerSptThn.add("2017");
        ar_spinnerSptThn.add("2018");
        return ar_spinnerSptThn;
    }

    private ArrayList<String> setspinnerSptLevel() {
        ar_spinnerSptLevel.clear();
        int usrLvl = Integer.parseInt(sesiLoginUserLevel);
        for (int k=usrLvl; k<=6; k++){
            ar_spinnerSptLevel.add("Lvl: "+String.valueOf(k)+" - 6");
        }
        return ar_spinnerSptLevel;
    }

    private String getBulan1() {
        String hasil = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date convertedDate = dateFormat.parse(spinnerSptThn.getText().toString().trim() + "-" + String.valueOf(spinnerSptB1.getSelectedIndex()+1) + "-" + "01");
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
            hasil = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hasil;
    }

    private String getBulan2() {
        String hasil = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date convertedDate = dateFormat.parse(spinnerSptThn.getText().toString().trim() + "-" + String.valueOf(spinnerSptB2.getSelectedIndex()+1) + "-" + "01");
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
            hasil = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hasil;
    }

    private String getLevel(){
        String[] split1 = spinnerSptLevel.getText().toString().trim().split("\\:");
        String[] split2 = split1[1].toString().trim().split("\\-");
        return split2[0].trim();
    }

    private void LoadDataFromServer() {
        saveBulanTerpilih();
        //Toast.makeText(getApplicationContext(),getBulan1() +" ~ "+ getBulan2() +" ~ "+ getLevel() +" ~ "+ getTypex(),Toast.LENGTH_SHORT).show();
        levelListnya = Integer.parseInt(getLevel());
        adapter.items_spt.clear();
        //dialogprogress.show();
        loadingStart();
        if (!IsConnected.CheckInternet()==true){
            LoadDataFromLocal(id_local_pk);
            if (txtSptPesan.getVisibility()!= View.VISIBLE) {
                txtSptPesan.setVisibility(View.VISIBLE);
            }
            //dialogprogress.dismiss();
            loadingStop();
            txtSptPesan.setText(getString(R.string.koneksi_internet_gagal));
        }else {

            if (txtSptPesan.getVisibility()==View.VISIBLE) {
                txtSptPesan.setVisibility(View.GONE);
            }

            String urlnya = "http://"+ finalIPnya +"/login/NexCloudAPI?MenuApp=MenuSPT&txtdatefrom="+getBulan1()+
                    "&txtdatethru="+getBulan2()+
                    "&getPrincipalID="+sesiLoginPrincipalID+
                    "&getListDistributorID="+sesiLoginDistributorID+
                    "&userLevel="+sesiLoginUserLevel+
                    "&txttoplevel="+levelListnya;

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                String content = res.getString("content");
                                String message = res.getString("message");
                                boolean success = res.getBoolean("success");
                                if(success==true){
                                    if (content.equals("")){
                                        //dialogprogress.dismiss();
                                        loadingStop();
                                        Toast.makeText(getApplicationContext(),"Tidak ada Data!",Toast.LENGTH_SHORT).show();
                                    }else {
                                        Log.d("Sptcontent", content);
                                        updateDataLocal( spinnerSptB1.getSelectedIndex() + "^" + spinnerSptB2.getSelectedIndex() + "^" + spinnerSptThn.getSelectedIndex() + "^" + spinnerSptLevel.getSelectedIndex(), content, id_local_pk);
                                        LoadDataFromLocal(id_local_pk);
                                    }
                                }else{
                                    finish();
                                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {}
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        //dialogprogress.dismiss();
                        loadingStop();
                        LoadDataFromLocal(id_local_pk);
                        if (txtSptPesan.getVisibility()!=View.VISIBLE) {
                            txtSptPesan.setVisibility(View.VISIBLE);
                        }
                        txtSptPesan.setText(getString(R.string.koneksi_server_gagal));
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }

    private void updateDataLocal(String tgl, String all_result, int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        db.updateTbNexcloud(tgl, all_result, idlocal);
        db.close();
    }

    private void LoadDataFromLocal(int idlocal) {
        loadingStop();
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        Cursor cur = db.getTbNexcloudByID(idlocal);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            Log.d(TAG, "DB_Local_"+ cur.getString(0) +"_"+ cur.getString(1)+"_"+cur.getString(2)+"_"+cur.getString(3));
                            LoadToSetData(cur.getString(3),cur.getString(1));
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        db.close();
    }

    private void saveBulanTerpilih() {
        spinnerB1B2Spt.clear();
        String x1 = spinnerSptB1.getText().toString();
        String x2 = spinnerSptB2.getText().toString();
        int awal=0, akhir=0;
        for (int a=0; a<ar_spinnerSptB1.size(); a++){
            if (x1==ar_spinnerSptB1.get(a)){
                awal=a;
            }
            if (x2==ar_spinnerSptB1.get(a)){
                akhir=a;
            }
        }
        for (int a=awal; a<=akhir; a++){
            spinnerB1B2Spt.add(ar_spinnerSptB1.get(a));
        }
        for (int a=0; a<spinnerB1B2Spt.size(); a++) {
            Log.d("spinnerB1B2Spt", spinnerB1B2Spt.get(a));
        }
    }

    private void LoadToSetData(String valData, String tgl) {
        if (!tgl.equals("")) {
            String[] valDataInduk = tgl.toString().trim().split("\\^");
            if (valDataInduk.length >4){
                spinnerSptB1.setSelectedIndex(Integer.parseInt(valDataInduk[0]));
                spinnerSptB2.setSelectedIndex(Integer.parseInt(valDataInduk[1]));
                spinnerSptThn.setSelectedIndex(Integer.parseInt(valDataInduk[2]));
                spinnerSptLevel.setSelectedIndex(Integer.parseInt(valDataInduk[3]));
                spinnerSptType.setSelectedIndex(Integer.parseInt(valDataInduk[4]));
            }
        }
        if (!valData.equals("")) {
            Log.d(TAG, valData + " & " + tgl);
            dataTxt = valData;
            simpanKeArray();
            lihatDataArray();
            setKeListView();
        }else{
            Toast.makeText(getApplicationContext(),"Tidak ada data!",Toast.LENGTH_SHORT).show();
        }
        //dialogprogress.dismiss();
        loadingStop();
    }


    private void simpanKeArray() {
        setArrayNull();

        //Simpan PK
        String ar2 = "";
        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        for (int a = 0; a < itemInduk.length; a++) {
            ar2 = "";
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_pk.add(a + 1);//+1
            ar_no.add(Integer.valueOf(itemAnak[0]));
            ar_lokasi.add(99999);
            ar_1.add(itemAnak[1]);
            for (int b = 2; b < itemAnak.length; b++){
                ar2+=itemAnak[b]+"~";
            }
            ar_2.add(ar2);
        }
        //update Lokasi
        if (levelListnya==0){
            for (int i=0; i<ar_pk.size(); i++){
                if (ar_no.get(i)==0){
                    ar_lokasi.set(i,0);
                }else if(ar_no.get(i)==1){
                    ar_lokasi.set(i,1);
                }else if(ar_no.get(i)>1) {
                    for (int b=i; b<ar_pk.size(); b++){
                        if(ar_no.get(b)==ar_no.get(i) && ar_no.get(b)>ar_no.get(b-1)){
                            ar_lokasi.set(b,ar_pk.get(i-1));
                        }else if(ar_no.get(b)>ar_no.get(i)){
                            ar_lokasi.set(b,ar_pk.get(i));
                        }
                    }
                }
            }
        }else{
            for (int i=0; i<ar_pk.size(); i++){
                if (i==0 || ar_no.get(i)==levelListnya){
                    ar_lokasi.set(i,0);
                }else{
                    if(ar_no.get(i)>ar_no.get(i-1)){
                        ar_lokasi.set(i,ar_pk.get(i-1));
                    }else if(ar_no.get(i)==ar_no.get(i-1)){
                        ar_lokasi.set(i,ar_lokasi.get(i-1));
                    }else if (ar_no.get(i)<ar_no.get(i-1)){ //no skrg < no sebelumnya
                        int popo = cekLokasiOFnumber(ar_no.get(i),i);
                        ar_lokasi.set(i,popo);
                    }
                }
            }
        }

    }

    private void setArrayNull() {
        ar_pk.clear();
        ar_lokasi.clear();
        ar_no.clear();
        ar_1.clear();
        ar_2.clear();
    }

    private int cekLokasiOFnumber(int val, int i) {
        int hasil=0;
        for (int x=i-1; x>=0; x--){
            if (ar_no.get(x)==val){
                hasil = ar_lokasi.get(x);
                break;
            }
        }
        return hasil;
    }

    private void lihatDataArray() {
        for (int i=0; i<ar_no.size(); i++){
            Log.d(TAG, "Lihat Array " +
                    String.valueOf(ar_pk.get(i)) + "_"+
                    String.valueOf(ar_lokasi.get(i)) + "_"+
                    String.valueOf(ar_no.get(i)) + "_"+
                    String.valueOf(ar_1.get(i)) + "_"+
                    String.valueOf(ar_2.get(i))
            );
        }
    }

    private void setKeListView() {

        if (status_awal==true){
            //muncul pertama
            adapter.items_spt.clear();
            for (int a=0; a<ar_pk.size(); a++){
                if(ar_lokasi.get(a)==0){
                    DataSpt ita = new DataSpt();
                    ita.setAr_pk(ar_pk.get(a));
                    ita.setAr_lokasi(ar_lokasi.get(a));
                    ita.setAr_no(ar_no.get(a));
                    ita.setAr_1(ar_1.get(a));
                    ita.setAr_2(ar_2.get(a));
                    itemList.add(ita);
                    adapter.notifyDataSetChanged();
                }
            }
            status_awal=false;
        }else {
            if (cekDataPkLokasi()==true){
                //muncul selanjutnya
                txtInfoLokasi = txtInfoLokasi+"^"+pk_yg_diklik;
                adapter.items_spt.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    Log.d("TES",a+"");
                    if(ar_lokasi.get(a)==pk_yg_diklik){
                        Log.d("TES_TRUE_IF",ar_lokasi.get(a)+"");
                        DataSpt ita = new DataSpt();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_1(ar_1.get(a));
                        ita.setAr_2(ar_2.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
            }else {
                if (txtSptPesan.getVisibility()!= View.VISIBLE) {
                    txtSptPesan.setVisibility(View.VISIBLE);
                }
                txtSptPesan.setText("Maaf, tidak ada data lagi !");
            }
        }
        //dialogprogress.dismiss();
        loadingStop();
    }

    private Boolean cekDataPkLokasi() {
        Boolean bol = false;
        for (int a=0; a<ar_pk.size(); a++){
            if(ar_lokasi.get(a)==pk_yg_diklik){
                bol=true;
                break;
            }
        }
        return bol;
    }

    private void setHistoryUp(int position) {
        if(cekDataPkLokasi()==true){
            infoHistoriesSpt.setVisibility(View.VISIBLE);
            posisi_namaNow = posisi_namaNow+" > "+itemList.get(position).getAr_1();
            infoHistoriesSpt.setText(posisi_namaNow);
        }
    }

    private void kembaliKeHeader() {
        String lokasiUriLast="NULL";
        String lokasiUri = txtInfoLokasi;
        String lastLocationToTxt="";
        Log.d("KKH_lokasiUri",lokasiUri);
        String[] lokasiUriCut = lokasiUri.toString().trim().split("\\^");
        for (int a=0; a<lokasiUriCut.length; a++){
            if (a==lokasiUriCut.length-2){
                lokasiUriLast = String.valueOf(lokasiUriCut[a]);
            }
            if (a<lokasiUriCut.length-1){
                if (a==lokasiUriCut.length-2){
                    lastLocationToTxt += lokasiUriCut[a];
                }else {
                    lastLocationToTxt += lokasiUriCut[a] + "^";
                }
            }
        }
        Log.d("KKH_lastLocationToTxt",lastLocationToTxt);
        txtInfoLokasi = lastLocationToTxt;//.setText(lastLocationToTxt);
        Log.d("KKH_lokasiUriLast",lokasiUriLast);
        if (!lokasiUriLast.equals("NULL")){
            Log.d("KKH_lokasiUriLast","OKEH_SHOW_LOKASI_"+lokasiUriLast);
            if (lokasiUriLast.equals("")){
                lokasiUriLast="0";
            }
            if(Integer.parseInt(lokasiUriLast)>0){
                //munculin data WHERE lokasi_di_array == PK_nya
                Log.d("KKH_JADI","munculin data WHERE lokasi_di_array == PK_nya "+lokasiUriLast);
                adapter.items_spt.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    Log.d("KKH_JADI_FOR",""+ar_lokasi.get(a));
                    if(String.valueOf(ar_lokasi.get(a)).equals(lokasiUriLast)){
                        Log.d("KKH_JADI_FOR_",""+ar_lokasi.get(a));
                        DataSpt ita = new DataSpt();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_1(ar_1.get(a));
                        ita.setAr_2(ar_2.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
                //SET HISTORY BACK
                setHistoryDown();
                //update_text
            }else{
                //munculin data WHERE no_di_array == 0
                Log.d("KKH_JADI","munculin data WHERE no_di_array == 0");
                txtInfoLokasi = "0";
                posisi_namaNow = "Home";
                infoHistoriesSpt.setVisibility(View.GONE);
                //muncul pertama
                adapter.items_spt.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    if(ar_no.get(a)==levelListnya){
                        DataSpt ita = new DataSpt();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_1(ar_1.get(a));
                        ita.setAr_2(ar_2.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
            }
        }
    }

    private void setHistoryDown() {
        String posisi_endNama = "";
        String[] hd = posisi_namaNow.toString().trim().split("\\ > ");
        for (int a=1; a<hd.length-1; a++){
            Log.d("HDHD1", hd[a]);
            posisi_endNama+= " > "+hd[a];
        }
        Log.d("HDHD2", posisi_endNama);
        posisi_namaNow = "Home "+posisi_endNama;
        infoHistoriesSpt.setText(posisi_namaNow);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5 && keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (txtSptPesan.getVisibility()==View.VISIBLE) {
                txtSptPesan.setVisibility(View.GONE);
            }
            if (!txtInfoLokasi.equals("0") && infoHistoriesSpt.getVisibility()==View.VISIBLE){
                kembaliKeHeader();
            }else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onBackPressed() {}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main3, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_sync:
                oneMoreLoad();
                LoadDataFromServer();
                return true;
            case R.id.action_tc:
                gantiChartType();
                return true;
            case R.id.action_sh:
                if (atasSpt.getVisibility()==View.VISIBLE) {
                    atasSpt.setVisibility(View.GONE);
                }else if (atasSpt.getVisibility()==View.GONE){
                    atasSpt.setVisibility(View.VISIBLE);
                }
                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_sr:
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation== Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void gantiChartType() {
        final CharSequence[] items = { "Route Coverage & Eff Call Rate", "EC-DMS Vs EC-SFA", "Actual Value "};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Chart Type:");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item==0){
                    typeChartSpt="ratio";
                }else if (item==1){
                    typeChartSpt="dmssfa";
                }else if (item==2){
                    typeChartSpt="value";
                }
                //Toast.makeText(getApplicationContext(),typeChartSpt,Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(),item+"~"+items[item],Toast.LENGTH_SHORT).show();
                oneMoreLoad();
                //LoadDataFromServer();
                LoadDataFromLocal(id_local_pk);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void oneMoreLoad() {
        status_awal=true;
        infoHistoriesSpt.setText("");
        txtInfoLokasi = "";
        posisi_namaNow = "Home";
        setArrayNull();
        if (infoHistoriesSpt.getVisibility()== View.VISIBLE){
            infoHistoriesSpt.setVisibility(View.GONE);
        }
    }

}
