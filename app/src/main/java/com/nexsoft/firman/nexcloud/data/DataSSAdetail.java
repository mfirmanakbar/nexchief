package com.nexsoft.firman.nexcloud.data;

/**
 * Created by PATRA on 07-Apr-17.
 */

public class DataSSAdetail {

    String ar_dist, ar_dist_name, ar_city, ar_cp, ar_sku;

    public DataSSAdetail() {
    }

    public DataSSAdetail(String ar_dist, String ar_dist_name, String ar_city, String ar_cp, String ar_sku) {
        this.ar_dist = ar_dist;
        this.ar_dist_name = ar_dist_name;
        this.ar_city = ar_city;
        this.ar_cp = ar_cp;
        this.ar_sku = ar_sku;
    }

    public String getAr_dist() {
        return ar_dist;
    }

    public void setAr_dist(String ar_dist) {
        this.ar_dist = ar_dist;
    }

    public String getAr_dist_name() {
        return ar_dist_name;
    }

    public void setAr_dist_name(String ar_dist_name) {
        this.ar_dist_name = ar_dist_name;
    }

    public String getAr_city() {
        return ar_city;
    }

    public void setAr_city(String ar_city) {
        this.ar_city = ar_city;
    }

    public String getAr_cp() {
        return ar_cp;
    }

    public void setAr_cp(String ar_cp) {
        this.ar_cp = ar_cp;
    }

    public String getAr_sku() {
        return ar_sku;
    }

    public void setAr_sku(String ar_sku) {
        this.ar_sku = ar_sku;
    }
}
