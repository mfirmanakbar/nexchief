package com.nexsoft.firman.nexcloud;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nexsoft.firman.nexcloud.adapter.AdapterSSA;
import com.nexsoft.firman.nexcloud.data.DataSSA;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudTgl;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MySession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SkusoldActivity extends AppCompatActivity {

    private TextView txtSSADateFrom, txtSSADateTo, txtSSAPesan;
    private EditText txtRangeA, txtRangeB;
    private int mYear, mMonth, mDay, pk_yg_diklik;
    //private ProgressDialog dialogprogress = null;
    private String TAG = "firman_SSA", dataTxt="", sesiLoginUsername="",sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="";
    private MySession sesi;

    private ArrayList<Integer> ar_pk = new ArrayList<Integer>();
    private ArrayList<Integer> ar_no = new ArrayList<Integer>();
    private ArrayList<Integer> ar_lokasi = new ArrayList<Integer>();
    private ArrayList<String> ar_dist = new ArrayList<String>();
    private ArrayList<String> ar_dist_name = new ArrayList<String>();
    private ArrayList<String> ar_city = new ArrayList<String>();
    private ArrayList<String> ar_cp = new ArrayList<String>();
    private ArrayList<String> ar_sku = new ArrayList<String>();

    private ListView list;
    private AdapterSSA adapter;
    private List<DataSSA> itemList = new ArrayList<DataSSA>();

    private String lemparDetail="";
    private String finalIPnya = "";
    private final int id_local_pk=4;
    private Handler handler = new Handler();
    private int hitTime=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skusold);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesi = new MySession();

        sesiLoginUsername = sesi.getUsername(getApplicationContext());
        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginListDistPetik = sesi.getListDistPetik(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());;

        /*dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);*/

        txtSSAPesan = (TextView)findViewById(R.id.txtSSAPesanXML);
        txtSSADateFrom = (TextView)findViewById(R.id.txtSSADateFromXML);
        txtSSADateTo = (TextView)findViewById(R.id.txtSSADateToXML);
        txtRangeA = (EditText)findViewById(R.id.idRangeA);
        txtRangeB = (EditText)findViewById(R.id.idRangeB);

        list    = (ListView) findViewById(R.id.listViewSSAx);

        adapter = new AdapterSSA(SkusoldActivity.this, itemList);
        list.setAdapter(adapter);

        //hidden keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setDateNow();
        setRangeNow();


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (ar_pk.size() > 1) {
                    lemparDetail = "";
                    pk_yg_diklik = Integer.parseInt(itemList.get(position).getAr_pk());
                    for (int i = 0; i < ar_pk.size(); i++) {
                        if (ar_lokasi.get(i) == pk_yg_diklik) {
                            if (i == ar_pk.size()-1) {
                                lemparDetail += ar_dist.get(i) + "^" + ar_dist_name.get(i) + "^" + ar_city.get(i) + "^" + ar_cp.get(i) + "^" + ar_sku.get(i);
                            } else {
                                lemparDetail += ar_dist.get(i) + "^" + ar_dist_name.get(i) + "^" + ar_city.get(i) + "^" + ar_cp.get(i) + "^" + ar_sku.get(i) + "~";
                            }
                            Log.d("lemparDetail1", lemparDetail);
                        }
                    }
                    Log.d("lemparDetail2", lemparDetail);
                    Intent aa = new Intent(SkusoldActivity.this, SkusoldActivityDetailActivity.class);
                    aa.putExtra("lemparDetail", lemparDetail);
                    startActivity(aa);
                    overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
                }else{
                    Toast.makeText(getApplicationContext(),"Maaf, tidak ada data lagi!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        LoadDataFromServer();
        /*hitTime=0;
        handler.postDelayed(runnable, MyConfig.delayToLoad);*/

    }

    /*private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hitTime+=1;
            if (hitTime<2) {
                handler.postDelayed(this, MyConfig.delayToLoad);
            }else {
                handler.removeCallbacks(runnable);
                LoadDataFromServer();
            }
            Log.e("ikan"+TAG,String.valueOf(hitTime));
        }
    };*/

    private void loadingStart() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.VISIBLE);
    }

    private void loadingStop() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.GONE);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main4, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
            }
            /*case R.id.action_sr:{
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation== Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setRangeNow() {
        txtRangeA.setText("0");
        txtRangeB.setText("4");
    }

    private void setDateNow() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = sdf.format(date);
        TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
        txtSSADateFrom.setText(pos.getNowTglB());
        txtSSADateTo.setText(dateString);
    }

    public void onClickDateFromSSA(View view){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear+1;
                        int tgl = dayOfMonth;
                        String xBulan = "";
                        String xTgl = "";
                        if (bulan<10){
                            xBulan = "0"+String.valueOf(bulan);
                        }else{
                            xBulan = String.valueOf(bulan);
                        }
                        if (tgl<10){
                            xTgl = "0"+String.valueOf(tgl);
                        }else{
                            xTgl = String.valueOf(tgl);
                        }
                        txtSSADateFrom.setText(year+"-"+xBulan+"-"+xTgl);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();

    }

    public void onClickDateToSSA(View view){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear+1;
                        int tgl = dayOfMonth;
                        String xBulan = "";
                        String xTgl = "";
                        if (bulan<10){
                            xBulan = "0"+String.valueOf(bulan);
                        }else{
                            xBulan = String.valueOf(bulan);
                        }
                        if (tgl<10){
                            xTgl = "0"+String.valueOf(tgl);
                        }else{
                            xTgl = String.valueOf(tgl);
                        }
                        txtSSADateTo.setText(year+"-"+xBulan+"-"+xTgl);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    public void onClickSubmitSSA(View view){
        //View view1 = this.getCurrentFocus();
        if (view != null) {
            //hidden keyboard
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (txtRangeA.length()<1 || txtRangeB.length()<1){
            setRangeNow();
        }
        LoadDataFromServer();
    }

    private void LoadDataFromServer() {
        //dialogprogress.show();
        loadingStart();
        if (!IsConnected.CheckInternet()==true){
            /*if (txtSSAPesan.getVisibility()==View.GONE) {
                txtSSAPesan.setVisibility(View.VISIBLE);
            }
            txtSSAPesan.setText(getString(R.string.koneksi_internet_gagal));*/
            Toast.makeText(getApplicationContext(), getString(R.string.koneksi_internet_gagal), Toast.LENGTH_SHORT).show();
            LoadDataFromLocal(id_local_pk);
            //dialogprogress.dismiss();
            loadingStop();
        }else {

            TbNexcloudTgl pos = new TbNexcloudTgl(getApplicationContext());
            pos.updateTbNexcloudTgl(txtSSADateFrom.getText().toString(),"",2);

            if (txtSSAPesan.getVisibility()==View.VISIBLE) {
                txtSSAPesan.setVisibility(View.GONE);
            }

            String postDateFrom = txtSSADateFrom.getText().toString();
            String postDateTo = txtSSADateTo.getText().toString();
            String rA = txtRangeA.getText().toString();
            String rB = txtRangeB.getText().toString();

            String urlnya = "http://"+finalIPnya+"/login/NexCloudAPI?MenuApp=MenuSSA&txtdatefrom="+postDateFrom+
                    "&txtdatethru="+postDateTo+
                    "&txtskufrom="+rA+
                    "&txtskuthru="+rB+
                    "&getPrincipalID="+sesiLoginPrincipalID+
                    "&getListDistributorID="+sesiLoginDistributorID;

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                String content = res.getString("content");
                                Log.d(TAG, content);
                                Log.d("POS_SSA", content);
                                if (content.equals("")){
                                    //dialogprogress.dismiss();
                                    loadingStop();
                                }else {
                                    updateDataLocal(txtSSADateFrom.getText().toString() + "^" + txtSSADateTo.getText().toString() + "^" + txtRangeA.getText().toString() + "^" + txtRangeB.getText().toString(), content, id_local_pk);
                                    LoadDataFromLocal(id_local_pk);
                                }
                            } catch (JSONException e) {}
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        //dialogprogress.dismiss();
                        loadingStop();
                        LoadDataFromLocal(id_local_pk);
                        if (txtSSAPesan.getVisibility()==View.GONE) {
                            txtSSAPesan.setVisibility(View.VISIBLE);
                        }
                        txtSSAPesan.setText(getString(R.string.koneksi_server_gagal));
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }

    private void updateDataLocal(String tgl, String all_result, int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        db.updateTbNexcloud(tgl, all_result, idlocal);
        db.close();
    }

    private void LoadDataFromLocal(int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        Cursor cur = db.getTbNexcloudByID(idlocal);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            Log.d(TAG, "DB_Local_"+ cur.getString(0) +"_"+ cur.getString(1)+"_"+cur.getString(2)+"_"+cur.getString(3));
                            LoadToSetData(cur.getString(3),cur.getString(1));
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        db.close();
    }

    private void LoadToSetData(String valData, String tgl) {
        if (!tgl.equals("")) {
            String[] valDataInduk = tgl.toString().trim().split("\\^");
            txtSSADateFrom.setText(valDataInduk[0]);
            txtSSADateTo.setText(valDataInduk[1]);
            txtRangeA.setText(valDataInduk[2]);
            txtRangeB.setText(valDataInduk[3]);
        }
        if (!valData.equals("")) {
            dataTxt = valData;
            simpanKeArray();
            lihatDataArray();
            setKeListView();
        }else{
            Toast.makeText(getApplicationContext(),"Tidak ada data!",Toast.LENGTH_SHORT).show();
        }
    }

    private void simpanKeArray() {
        //kosongin array
        ar_pk.clear();
        ar_no.clear();
        ar_lokasi.clear();
        ar_dist.clear();
        ar_dist_name.clear();
        ar_city.clear();
        ar_cp.clear();
        ar_sku.clear();

        //Simpan PK
        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_pk.add(a+1);
            ar_no.add(Integer.valueOf(itemAnak[0]));
            ar_lokasi.add(0);
            ar_dist.add(itemAnak[1]);
            ar_dist_name.add(itemAnak[2]);
            ar_city.add(itemAnak[3]);
            ar_cp.add(itemAnak[4]);
            ar_sku.add(itemAnak[5]);
        }
        //update Lokasi type 2
        int lokasiNow=0;
        for (int i=0; i<ar_pk.size(); i++){
            if (ar_no.get(i)==0){
                ar_lokasi.set(i,0);
            }else if(ar_no.get(i)==1 && ar_no.get(i-1)==0){
                lokasiNow = ar_pk.get(i-1);
                ar_lokasi.set(i, lokasiNow);
            }else if(ar_no.get(i)==1 && ar_no.get(i-1)==1){
                ar_lokasi.set(i, lokasiNow);
            }
        }

        //lihat
        lihatDataArray();
    }

    private void lihatDataArray() {
        for (int i=0; i<ar_no.size(); i++){
            Log.d(TAG, "Lihat SSA " +
                    String.valueOf(ar_pk.get(i)) + "_"+
                    String.valueOf(ar_lokasi.get(i)) + "_"+
                    String.valueOf(ar_no.get(i)) + "_"+
                    String.valueOf(ar_dist.get(i)) + "_"+
                    String.valueOf(ar_dist_name.get(i)) + "_"+
                    String.valueOf(ar_city.get(i)) + "_" +
                    String.valueOf(ar_cp.get(i)) + "_"+
                    String.valueOf(ar_sku.get(i))
            );
        }
    }

    private void setKeListView() {
        adapter.items_saa.clear();
        for (int a=0; a<ar_pk.size(); a++){
            if(ar_no.get(a)==0){
                DataSSA ita = new DataSSA();
                ita.setAr_pk(String.valueOf(ar_pk.get(a)));
                ita.setAr_lokasi(String.valueOf(ar_lokasi.get(a)));
                ita.setAr_no(String.valueOf(ar_no.get(a)));
                ita.setAr_dist(ar_dist.get(a));
                ita.setAr_dist_name(ar_dist_name.get(a));
                ita.setAr_city(ar_city.get(a));
                ita.setAr_cp(ar_cp.get(a));
                ita.setAr_sku(ar_sku.get(a));
                itemList.add(ita);
                adapter.notifyDataSetChanged();
            }
        }
        //dialogprogress.dismiss();
        loadingStop();
    }

}
