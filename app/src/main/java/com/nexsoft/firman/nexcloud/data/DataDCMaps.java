package com.nexsoft.firman.nexcloud.data;

/**
 * Created by PATRA on 15-May-17.
 */

public class DataDCMaps {

    String ar_0, ar_1, ar_2, ar_3, ar_4, ar_5;

    public String getAr_0() {
        return ar_0;
    }

    public void setAr_0(String ar_0) {
        this.ar_0 = ar_0;
    }

    public String getAr_1() {
        return ar_1;
    }

    public void setAr_1(String ar_1) {
        this.ar_1 = ar_1;
    }

    public String getAr_2() {
        return ar_2;
    }

    public void setAr_2(String ar_2) {
        this.ar_2 = ar_2;
    }

    public String getAr_3() {
        return ar_3;
    }

    public void setAr_3(String ar_3) {
        this.ar_3 = ar_3;
    }

    public String getAr_4() {
        return ar_4;
    }

    public void setAr_4(String ar_4) {
        this.ar_4 = ar_4;
    }

    public String getAr_5() {
        return ar_5;
    }

    public void setAr_5(String ar_5) {
        this.ar_5 = ar_5;
    }
}
