package com.nexsoft.firman.nexcloud;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nexsoft.firman.nexcloud.adapter.AdapterDasheffd;
import com.nexsoft.firman.nexcloud.adapter.AdapterDasheffsd;
import com.nexsoft.firman.nexcloud.data.DataDasheff;
import com.nexsoft.firman.nexcloud.data.DataDasheffsd;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloud;
import com.nexsoft.firman.nexcloud.dblocal.TbNexcloudIp;
import com.nexsoft.firman.nexcloud.util.ExceptionHandler;
import com.nexsoft.firman.nexcloud.util.IsConnected;
import com.nexsoft.firman.nexcloud.util.ModeCoding;
import com.nexsoft.firman.nexcloud.util.MyConfig;
import com.nexsoft.firman.nexcloud.util.MyRainbow;
import com.nexsoft.firman.nexcloud.util.MySession;
import com.nexsoft.firman.nexcloud.util.MyTextFormat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DasheffsdActivity extends AppCompatActivity {

    private ListView list;
    private AdapterDasheffsd adapter;
    private List<DataDasheffsd> itemList = new ArrayList<DataDasheffsd>();

    //private ProgressDialog dialogprogress = null;
    private String TAG = "firman_Dasheffsd", dataTxt="", sesiLoginUsername="",sesiLoginPrincipalID="",sesiLoginDistributorID="", sesiLoginListDistPetik="", sesiLoginListDistLength="";
    private final MySession sesi = new MySession();
    private String finalIPnya = "";
    private TextView txtTotalDasheffsd1, txtTotalDasheffsd2, txtTotalDasheffsd3;
    private Double ao=0.0, ro=0.0;
    private final MyRainbow mrb = new MyRainbow();
    private final int id_local_pk = 20;
    private String rotasinya = "";
    private Handler handler = new Handler();
    private int hitTime=0;
    private String txtDashDateFrom="",txtDashDateTo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dasheffsd);

        ModeCoding mcx = new ModeCoding();
        if (mcx.mode_coding==false) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TbNexcloudIp pos = new TbNexcloudIp(getApplicationContext());
        finalIPnya = pos.getIPAdrs();

        sesiLoginPrincipalID = sesi.getPrincipalID(getApplicationContext());
        sesiLoginDistributorID = sesi.getDistributorID(getApplicationContext());

        /*dialogprogress = new ProgressDialog(this);
        dialogprogress.setMessage("Please wait ...");
        dialogprogress.setCancelable(false);*/

        txtTotalDasheffsd1 = (TextView)findViewById(R.id.txtTotalDasheffsd1);
        txtTotalDasheffsd2 = (TextView)findViewById(R.id.txtTotalDasheffsd2);
        txtTotalDasheffsd3 = (TextView)findViewById(R.id.txtTotalDasheffsd3);

        list    = (ListView) findViewById(R.id.listviewDasheffsdx);
        list.setDivider(null);
        adapter = new AdapterDasheffsd(DasheffsdActivity.this, itemList);
        list.setAdapter(adapter);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            txtDashDateFrom = extras.getString("txtDashDateFrom");
            txtDashDateTo = extras.getString("txtDashDateTo");
            LoadDataFromServer(txtDashDateFrom, txtDashDateTo);
            /*hitTime=0;
            handler.postDelayed(runnable, MyConfig.delayToLoad);*/
        }

    }

    /*private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hitTime+=1;
            if (hitTime<2) {
                handler.postDelayed(this, MyConfig.delayToLoad);
            }else {
                handler.removeCallbacks(runnable);
                LoadDataFromServer(txtDashDateFrom, txtDashDateTo);
            }
            Log.e("ikan"+TAG,String.valueOf(hitTime));
        }
    };*/

    private void loadingStart() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.VISIBLE);
    }

    private void loadingStop() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.GONE);
    }

    private void LoadDataFromServer(String txtDashDateFrom, String txtDashDateTo) {
        //dialogprogress.show();
        loadingStart();
        if (!IsConnected.CheckInternet()==true){
            //dialogprogress.dismiss();
            loadingStop();
            Toast.makeText(getApplicationContext(),getString(R.string.koneksi_internet_gagal),Toast.LENGTH_SHORT).show();
            LoadDataFromLocal(id_local_pk);
        }else {

            String urlnya = "http://"+finalIPnya+"/login/NexCloudAPI?MenuApp=MDgetxDataEffectiveSFAtoDMS&datefrom="+txtDashDateFrom+
                    "&datethru="+txtDashDateTo+
                    "&getPrincipalID="+sesiLoginPrincipalID+
                    "&getListDistributorID="+sesiLoginDistributorID;

            JSONObject jsonObject = new JSONObject();
            try{
                /*jsonObject.put("", null);*/
            }catch (Exception e){
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, jsonObject,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject res = new JSONObject(response.toString());
                                String content = res.getString("content");
                                Log.d(TAG, content);
                                Log.d("POS_SSA", content);
                                if (content.equals("")){
                                    Toast.makeText(getApplicationContext(),"Sorry, No Data Available!",Toast.LENGTH_SHORT).show();
                                    //dialogprogress.dismiss()    ;
                                    loadingStop();
                                }else {
                                    updateDataLocal("", content, id_local_pk);
                                    LoadDataFromLocal(id_local_pk);
                                }
                            } catch (JSONException e) {}
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage(), TAG);
                    Log.d(TAG, "FAIL!");
                    try {
                        //dialogprogress.dismiss();
                        loadingStop();
                        LoadDataFromLocal(id_local_pk);
                        Toast.makeText(getApplicationContext(),getString(R.string.koneksi_server_gagal),Toast.LENGTH_SHORT).show();
                    }catch (Exception a){}
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(MyConfig.volleytime,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

        }
    }

    private void updateDataLocal(String tgl, String all_result, int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        db.updateTbNexcloud(tgl, all_result, idlocal);
        db.close();
    }

    private void LoadDataFromLocal(int idlocal) {
        TbNexcloud db = new TbNexcloud(getApplicationContext());
        db.open();
        Cursor cur = db.getTbNexcloudByID(idlocal);
        if(cur.getCount()!=0){
            if (cur != null) {
                if (cur.moveToFirst()) {
                    if (cur.getCount() != 0) {
                        do {
                            Log.d(TAG, "DB_Local_"+ cur.getString(0) +"_"+ cur.getString(1)+"_"+cur.getString(2)+"_"+cur.getString(3));
                            LoadToSetData(cur.getString(3),cur.getString(1));
                        } while (cur.moveToNext());
                    }
                }
            }
        }else {
            Log.d("firman_test","SQLite Data -> NULL");
        }
        db.close();
    }

    private void LoadToSetData(String valData, String tgl) {
        if (!valData.equals("")) {
            dataTxt = valData;
            setKeListView();
        }
    }

    private void setKeListView() {

        MyTextFormat mtf = new MyTextFormat();
        adapter.items_dasheffsd.clear();
        String[] row = dataTxt.toString().trim().split("\\^");
        for (int i = 0; i < row.length; i++) {
            if (!row[i].equals("")) {
                String[] val = row[i].toString().trim().split("\\|");
                DataDasheffsd ita = new DataDasheffsd();
                ita.setAr_1(val[0]);
                ita.setAr_2(val[1]);
                ita.setAr_3(val[2]);
                ita.setAr_4(val[3]);
                ita.setAr_5(mtf.format_angka(val[4]));
                ita.setAr_6(mtf.format_angka(val[5]));
                ao+=Double.parseDouble(val[4]);
                ro+=Double.parseDouble(val[5]);
                Double val7=0.0;
                if (Double.parseDouble(val[5])>0){
                    val7 = (Double.parseDouble(val[4]) / Double.parseDouble(val[5])) * 100;
                }
                ita.setAr_7(String.valueOf(mtf.format_desimal_2(val7)));
                itemList.add(ita);
                adapter.notifyDataSetChanged();
            }
        }
        Double TotalDashou3=0.0;
        if (ro>0){
            TotalDashou3 = (ao / ro) * 100;
        }
        txtTotalDasheffsd1.setText(mtf.format_angka(String.valueOf(ao)));
        txtTotalDasheffsd2.setText(mtf.format_angka(String.valueOf(ro)));
        mrb.setColor(DasheffsdActivity.this,TotalDashou3,txtTotalDasheffsd3);
        txtTotalDasheffsd3.setText(mtf.format_angka(String.valueOf(mtf.format_desimal_2(TotalDashou3)))+"%");
        //dialogprogress.dismiss();
        loadingStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main4, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
            }
            case R.id.action_sr:{
                int orientation=this.getResources().getConfiguration().orientation;
                if(orientation== Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }else{
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
