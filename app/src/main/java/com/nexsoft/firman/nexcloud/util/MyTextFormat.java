package com.nexsoft.firman.nexcloud.util;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by PATRA on 24-May-17.
 */

public class MyTextFormat {

    public static String format_angka(String angka){
        return NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(angka)));
    }

    public static String format_desimal_s(int angka){
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(angka);
    }

    public static String format_desimal_1(int angka){
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(angka);
    }

    public static String format_desimal_1(float angka){
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(angka);
    }

    public static String format_desimal_1(long angka){
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(angka);
    }

    public static String format_desimal_1(double angka){
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(angka);
    }

    public static String format_desimal_2(int angka){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(angka);
    }

    public static String format_desimal_2(float angka){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(angka);
    }

    public static String format_desimal_2(long angka){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(angka);
    }

    public static String format_desimal_2(double angka){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(angka);
    }

    public static Spanned format_html(String teks){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(teks, Html.FROM_HTML_MODE_COMPACT);
        }else {
            return Html.fromHtml(teks);
        }
    }

}
