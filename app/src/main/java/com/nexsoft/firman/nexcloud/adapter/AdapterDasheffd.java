package com.nexsoft.firman.nexcloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexsoft.firman.nexcloud.R;
import com.nexsoft.firman.nexcloud.data.DataDasheff;
import com.nexsoft.firman.nexcloud.data.DataDasheffd;
import com.nexsoft.firman.nexcloud.util.MyRainbow;

import java.util.List;

/**
 * Created by PATRA on 23-May-17.
 */

public class AdapterDasheffd extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<DataDasheffd> items_dasheffd;
    private final MyRainbow mrb = new MyRainbow();

    public AdapterDasheffd(Activity activity, List<DataDasheffd> items) {
        this.activity = activity;
        this.items_dasheffd = items;
    }

    @Override
    public int getCount() {
        return items_dasheffd.size();
    }

    @Override
    public Object getItem(int position) {
        return items_dasheffd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_dasheffd, null);
        }

        final TextView txtDasheffdAr1 = (TextView) convertView.findViewById(R.id.txtDasheffdAr1);
        final TextView txtDasheffdAr2 = (TextView) convertView.findViewById(R.id.txtDasheffdAr2);
        final TextView txtDasheffdAr3 = (TextView) convertView.findViewById(R.id.txtDasheffdAr3);
        final TextView txtDasheffdAr4 = (TextView) convertView.findViewById(R.id.txtDasheffdAr4);
        final TextView txtDasheffdAr5 = (TextView) convertView.findViewById(R.id.txtDasheffdAr5);
        final TextView txtDasheffdAr6 = (TextView) convertView.findViewById(R.id.txtDasheffdAr6);
        final TextView txtDasheffdAr7 = (TextView) convertView.findViewById(R.id.txtDasheffdAr7);
        final LinearLayout layout_dasheffd = (LinearLayout) convertView.findViewById(R.id.layout_dasheffd);

        final MyRainbow mrb = new MyRainbow();
        mrb.setColorLayoutTb(activity,position,layout_dasheffd);

        DataDasheffd datax = items_dasheffd.get(position);
        txtDasheffdAr1.setText(datax.getAr_1());
        txtDasheffdAr2.setText(datax.getAr_2());
        txtDasheffdAr3.setText(datax.getAr_3());
        txtDasheffdAr4.setText(datax.getAr_4());
        txtDasheffdAr5.setText(datax.getAr_5());
        txtDasheffdAr6.setText(datax.getAr_6());
        mrb.setColor(activity,Double.parseDouble(datax.getAr_7()),txtDasheffdAr7);
        txtDasheffdAr7.setText(datax.getAr_7()+"%");

        return convertView;
    }
}
